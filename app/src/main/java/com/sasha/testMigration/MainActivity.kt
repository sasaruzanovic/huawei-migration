package com.sasha.testMigration

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import org.xms.g.location.FusedLocationProviderClient
import org.xms.g.location.LocationServices
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import org.xms.f.analytics.ExtensionAnalytics
import org.xms.f.messaging.ExtensionMessagingService

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val mFirebaseAnalytics: ExtensionAnalytics = ExtensionAnalytics.getInstance(this)

        val bundle = Bundle()
        bundle.putString(ExtensionAnalytics.Param.getITEM_ID(), "1")
        bundle.putString(ExtensionAnalytics.Param.getITEM_NAME(), "tes")
        bundle.putString(ExtensionAnalytics.Param.getCONTENT_TYPE(), "image")
        mFirebaseAnalytics.logEvent(ExtensionAnalytics.Event.getSELECT_CONTENT(), bundle)


        val mFusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}