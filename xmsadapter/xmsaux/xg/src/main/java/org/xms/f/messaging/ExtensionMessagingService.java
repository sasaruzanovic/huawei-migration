package org.xms.f.messaging;

import org.xms.g.utils.XBox;

public class ExtensionMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    public void onMessageReceived(com.google.firebase.messaging.RemoteMessage remoteMessage) {
        this.onMessageReceived(new org.xms.f.messaging.RemoteMessage(new XBox(remoteMessage, null)));
    }

    public void onMessageReceived(org.xms.f.messaging.RemoteMessage remoteMessage) {

    }

    public static org.xms.f.messaging.ExtensionMessagingService dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.ExtensionMessagingService) param0);
    }

}
