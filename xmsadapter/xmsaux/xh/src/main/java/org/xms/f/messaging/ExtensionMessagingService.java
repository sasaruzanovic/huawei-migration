package org.xms.f.messaging;

import org.xms.g.utils.XBox;

public class ExtensionMessagingService extends com.huawei.hms.push.HmsMessageService {

    public void onMessageReceived(com.huawei.hms.push.RemoteMessage remoteMessage) {
        this.onMessageReceived(new org.xms.f.messaging.RemoteMessage(new XBox(null, remoteMessage)));
    }

    public void onMessageReceived(org.xms.f.messaging.RemoteMessage remoteMessage) {

    }

    public static org.xms.f.messaging.ExtensionMessagingService dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.ExtensionMessagingService) param0);
    }

//    public static boolean isInstance(java.lang.Object param0) {
//        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
//            return false;
//        }
//        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.HmsMessageService;
//    }
}
