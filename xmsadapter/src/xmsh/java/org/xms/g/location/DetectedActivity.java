package org.xms.g.location;

/**
 * The detected activity of the device with an an associated confidence. See ActivityRecognitionApi for details on how to obtain a DetectedActivity..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityIdentificationData, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityIdentificationData: Identified activity type.</br>
 */
public class DetectedActivity extends org.xms.g.utils.XObject implements android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.ActivityIdentificationData.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityidentificationdata-0000001050706134-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityidentificationdata-0000001050706134-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.DetectedActivity createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.ActivityIdentificationData hReturn = com.huawei.hms.location.ActivityIdentificationData.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.DetectedActivity(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.DetectedActivity[] newArray(int param0) {
            return new org.xms.g.location.DetectedActivity[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.DetectedActivity.DetectedActivity(org.xms.g.utils.XBox) constructor of DetectedActivity with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public DetectedActivity(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.DetectedActivity(int,int) Constructs a DetectedActivity.<br/>
     * com.huawei.hms.location.ActivityIdentificationData(int,int): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section483016191826">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section483016191826</a><br/>
     * 
     * @param  param0 The activity that was detected
     * @param  param1 Value from 0 to 100 indicating how likely it is that the user is performing this activity
     */
    public DetectedActivity(int param0, int param1) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(param0, param1));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getIN_VEHICLE() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.VEHICLE: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section920118334120">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section920118334120</a><br/>
     * 
     * @return The constant value.device is in a vehicle, such as a car
     */
    public static int getIN_VEHICLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.VEHICLE");
        return com.huawei.hms.location.ActivityIdentificationData.VEHICLE;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getON_BICYCLE() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.BIKE: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section150818206412">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section150818206412</a><br/>
     * 
     * @return The constant value.The device is on a bicycle
     */
    public static int getON_BICYCLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.BIKE");
        return com.huawei.hms.location.ActivityIdentificationData.BIKE;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getON_FOOT() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.FOOT: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section5817139114111">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section5817139114111</a><br/>
     * 
     * @return The constant value.The device is on a user who is walking or running
     */
    public static int getON_FOOT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.FOOT");
        return com.huawei.hms.location.ActivityIdentificationData.FOOT;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getRUNNING() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.RUNNING: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section141409458439">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section141409458439</a><br/>
     * 
     * @return The constant value.The device is on a user who is running. This is a sub-activity of ON_FOOT
     */
    public static int getRUNNING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.RUNNING");
        return com.huawei.hms.location.ActivityIdentificationData.RUNNING;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getSTILL() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.STILL: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section7209416174213">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section7209416174213</a><br/>
     * 
     * @return The constant value.The device is still (not moving)
     */
    public static int getSTILL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.STILL");
        return com.huawei.hms.location.ActivityIdentificationData.STILL;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getTILTING() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.TILTING: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section1320793144318">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section1320793144318</a><br/>
     * 
     * @return The constant value.The device angle relative to gravity changed significantly
     */
    public static int getTILTING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.TILTING");
        return com.huawei.hms.location.ActivityIdentificationData.TILTING;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getUNKNOWN() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.OTHERS: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section42028371426">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section42028371426</a><br/>
     * 
     * @return The constant value.Unable to detect the current activity
     */
    public static int getUNKNOWN() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.OTHERS");
        return com.huawei.hms.location.ActivityIdentificationData.OTHERS;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getWALKING() Return the constant value.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.WALKING: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section8832102418431">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section8832102418431</a><br/>
     * 
     * @return The constant value.The device is on a user who is walking
     */
    public static int getWALKING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationData.WALKING");
        return com.huawei.hms.location.ActivityIdentificationData.WALKING;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getConfidence() Returns a value from 0 to 100 indicating the likelihood that the user is performing this activity.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.getPossibility(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section9254532817">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section9254532817</a><br/>
     * 
     * @return A value from 0 to 100 indicating the likelihood that the user is performing this activity
     */
    public int getConfidence() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).getPossibility()");
            return ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).getPossibility();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).getPossibilityCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).getPossibilityCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getType() Returns the type of activity that was detected.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.getIdentificationActivity(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section63878407100">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationdata-0000001050706134#EN-US_TOPIC_0000001050706134__section63878407100</a><br/>
     * 
     * @return Activity type
     */
    public int getType() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).getIdentificationActivity()");
            return ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).getIdentificationActivity();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).getIdentificationActivityCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).getIdentificationActivityCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.DetectedActivity.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.toString()
     * @return A character string after being converted
     */
    public java.lang.String toString() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).toString()");
            return ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).toString();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).toStringCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).toStringCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.DetectedActivity.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.ActivityIdentificationData.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).writeToParcel(param0, param1)");
            ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).writeToParcelCallSuper(param0, param1)");
            ((HImpl) ((com.huawei.hms.location.ActivityIdentificationData) this.getHInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.DetectedActivity.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.DetectedActivity.<br/>
     * 
     * @param  param0 The input object
     * @return Casted DetectedActivity object
     */
    public static org.xms.g.location.DetectedActivity dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.DetectedActivity) param0);
    }
    
    /**
     * org.xms.g.location.DetectedActivity.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityIdentificationData;
    }
    
    private class HImpl extends com.huawei.hms.location.ActivityIdentificationData {
        
        public int getPossibility() {
            return org.xms.g.location.DetectedActivity.this.getConfidence();
        }
        
        public int getIdentificationActivity() {
            return org.xms.g.location.DetectedActivity.this.getType();
        }
        
        public java.lang.String toString() {
            return org.xms.g.location.DetectedActivity.this.toString();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.DetectedActivity.this.writeToParcel(param0, param1);
        }
        
        public int getPossibilityCallSuper() {
            return super.getPossibility();
        }
        
        public int getIdentificationActivityCallSuper() {
            return super.getIdentificationActivity();
        }
        
        public java.lang.String toStringCallSuper() {
            return super.toString();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public HImpl(int param0, int param1) {
            super(param0, param1);
        }
    }
}