package org.xms.g.location;

/**
 * The main entry point for activity recognition integration..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityIdentification, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityIdentification: Entry for calling activity identification, which is used to obtain the ActivityIdentificationService instance.</br>
 */
public class ActivityRecognition extends org.xms.g.utils.XObject {
    
    /**
     * org.xms.g.location.ActivityRecognition.ActivityRecognition(org.xms.g.utils.XBox) Constructor of ActivityRecognition with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityRecognition(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getCLIENT_NAME() Return the value of CLIENT_NAME.<br/>
     * 
     * @return Constant Value:"activity_recognition"
     */
    public static java.lang.String getCLIENT_NAME() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getAPI() Return the token that pass to addApi(Api) to enable ContextServices.<br/>
     * 
     * @return Token to pass to addApi(Api) to enable ContextServices
     */
    public static org.xms.g.common.api.Api getAPI() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getActivityRecognitionApi() Return the entry point to the activity recognition APIs.<br/>
     * 
     * @return Entry point to the activity recognition APIs
     */
    public static org.xms.g.location.ActivityRecognitionApi getActivityRecognitionApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getClient(android.app.Activity) Create a new instance of Client for use in an Activity.<br/>
     * com.huawei.hms.location.ActivityIdentification.getService(android.app.Activity): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentification-0000001050746167#EN-US_TOPIC_0000001050746167__section1120013264461">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentification-0000001050746167#EN-US_TOPIC_0000001050746167__section1120013264461</a><br/>
     * 
     * @param  param0 A specific Activity
     * @return ActivityRecognitionClient instance obtained based on Activity
     */
    public static org.xms.g.location.ActivityRecognitionClient getClient(android.app.Activity param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentification.getService(param0)");
        com.huawei.hms.location.ActivityIdentificationService hReturn = com.huawei.hms.location.ActivityIdentification.getService(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.ActivityRecognitionClient(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getClient(android.content.Context) Create a new instance of ActivityRecognitionClient for use in a non-activity Context.<br/>
     * com.huawei.hms.location.ActivityIdentification.getService(android.content.Context): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentification-0000001050746167#EN-US_TOPIC_0000001050746167__section146052044204717">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentification-0000001050746167#EN-US_TOPIC_0000001050746167__section146052044204717</a><br/>
     * 
     * @param  param0 Context instance
     * @return ActivityRecognitionClient instance obtained based on Context
     */
    public static org.xms.g.location.ActivityRecognitionClient getClient(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentification.getService(param0)");
        com.huawei.hms.location.ActivityIdentificationService hReturn = com.huawei.hms.location.ActivityIdentification.getService(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.ActivityRecognitionClient(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityRecognition.<br/>
     * 
     * @param  param0 The input object
     * @return casted ActivityRecognition object
     */
    public static org.xms.g.location.ActivityRecognition dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityRecognition) param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityIdentification;
    }
}