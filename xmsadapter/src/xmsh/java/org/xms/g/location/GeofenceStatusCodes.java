package org.xms.g.location;

/**
 * Geofence specific status codes, for use in getStatusCode()..<br/>
 * Wrapper class for com.huawei.hms.location.GeofenceErrorCodes, but only the HMS API are provided.</br>
 * com.huawei.hms.location.GeofenceErrorCodes: Geofence result codes.</br>
 */
public final class GeofenceStatusCodes extends org.xms.g.common.api.CommonStatusCodes {
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.GeofenceStatusCodes(org.xms.g.utils.XBox) Constructor of GeofenceStatusCodes with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public GeofenceStatusCodes(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.getGEOFENCE_NOT_AVAILABLE() Return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_UNAVAILABLE: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1254213477129">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1254213477129</a><br/>
     * 
     * @return Constant Value. Geofence service is not available now. Typically this is because the user turned off location access in settings > location access
     */
    public static int getGEOFENCE_NOT_AVAILABLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_UNAVAILABLE");
        return com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_UNAVAILABLE;
    }
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.getGEOFENCE_TOO_MANY_GEOFENCES() Return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_NUMBER_OVER_LIMIT: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section104412013581">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section104412013581</a><br/>
     * 
     * @return Constant Value.Your app has registered more than 100 geofences. Remove unused ones before adding new geofences
     */
    public static int getGEOFENCE_TOO_MANY_GEOFENCES() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_NUMBER_OVER_LIMIT");
        return com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_NUMBER_OVER_LIMIT;
    }
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.getGEOFENCE_TOO_MANY_PENDING_INTENTS() Return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_PENDINGINTENT_OVER_LIMIT: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1312133196">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1312133196</a><br/>
     * 
     * @return Constant Value.You have provided more than 5 different PendingIntents to the addGeofences(GoogleApiClient, GeofencingRequest, PendingIntent) call
     */
    public static int getGEOFENCE_TOO_MANY_PENDING_INTENTS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_PENDINGINTENT_OVER_LIMIT");
        return com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_PENDINGINTENT_OVER_LIMIT;
    }
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.getStatusCodeString(int) Returns an untranslated debug (not user-friendly!) string based on the current status code.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.getErrorMessage(int): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section9254532817">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section9254532817</a><br/>
     * 
     * @param  param0 Result code
     * @return Result description
     */
    public static java.lang.String getStatusCodeString(int param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.getErrorMessage(param0)");
        return com.huawei.hms.location.GeofenceErrorCodes.getErrorMessage(param0);
    }
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.GeofenceStatusCodes.<br/>
     * 
     * @param  param0 The input object
     * @return Casted GeofenceStatusCodes object
     */
    public static org.xms.g.location.GeofenceStatusCodes dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.GeofenceStatusCodes) param0);
    }
    
    /**
     * org.xms.g.location.GeofenceStatusCodes.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.GeofenceErrorCodes;
    }
}