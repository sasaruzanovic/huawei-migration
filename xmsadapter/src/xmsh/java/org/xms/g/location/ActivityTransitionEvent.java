package org.xms.g.location;

/**
 * Represents an activity transition event, for example start to walk, stop running etc..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityConversionData, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityConversionData: Activity conversion event.</br>
 */
public class ActivityTransitionEvent extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.ActivityConversionData.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversiondata-0000001051066104-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversiondata-0000001051066104-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.ActivityTransitionEvent createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.ActivityConversionData hReturn = com.huawei.hms.location.ActivityConversionData.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.ActivityTransitionEvent(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.ActivityTransitionEvent[] newArray(int param0) {
            return new org.xms.g.location.ActivityTransitionEvent[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.ActivityTransitionEvent(org.xms.g.utils.XBox) Constructor of ActivityTransitionEvent with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityTransitionEvent(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.ActivityTransitionEvent(int,int,long) Creates an activity transition event.<br/>
     * com.huawei.hms.location.ActivityConversionData(int,int,long): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section114762417137">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section114762417137</a><br/>
     * 
     * @param  param0 The type of the activity of this transition
     * @param  param1 The type of transition
     * @param  param2 The elapsed realtime when this transition happened
     */
    public ActivityTransitionEvent(int param0, int param1, long param2) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(param0, param1, param2));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.huawei.hms.location.ActivityConversionData.equals(java.lang.Object)
     * @param  param0 The other instance
     * @return True if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).equals(param0)");
            return ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).equals(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).equalsCallSuper(param0)");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).equalsCallSuper(param0);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.getActivityType() Gets the type of the activity of this transition.<br/>
     * com.huawei.hms.location.ActivityConversionData.getActivityType(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section9254532817">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section9254532817</a><br/>
     * 
     * @return The type of the activity
     */
    public int getActivityType() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).getActivityType()");
            return ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).getActivityType();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).getActivityTypeCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).getActivityTypeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.getElapsedRealTimeNanos() Gets the elapsed realtime when this transition happened.<br/>
     * com.huawei.hms.location.ActivityConversionData.getElapsedTimeFromReboot(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section17367589218">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section17367589218</a><br/>
     * 
     * @return The number of milliseconds elapsed since the device boot to the activity conversion time
     */
    public long getElapsedRealTimeNanos() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).getElapsedTimeFromReboot()");
            return ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).getElapsedTimeFromReboot();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).getElapsedTimeFromRebootCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).getElapsedTimeFromRebootCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.getTransitionType() Gets the type of the transition.<br/>
     * com.huawei.hms.location.ActivityConversionData.getConversionType(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section8583114215229">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversiondata-0000001051066104#EN-US_TOPIC_0000001051066104__section8583114215229</a><br/>
     * 
     * @return The type
     */
    public int getTransitionType() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).getConversionType()");
            return ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).getConversionType();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).getConversionTypeCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).getConversionTypeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.huawei.hms.location.ActivityConversionData.hashCode()
     * @return A hash code value
     */
    public int hashCode() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).hashCode()");
            return ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).hashCode();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).hashCodeCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).hashCodeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.ActivityConversionData.toString()
     * @return A character string after being converted
     */
    public java.lang.String toString() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).toString()");
            return ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).toString();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).toStringCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).toStringCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.ActivityConversionData.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).writeToParcel(param0, param1)");
            ((com.huawei.hms.location.ActivityConversionData) this.getHInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).writeToParcelCallSuper(param0, param1)");
            ((HImpl) ((com.huawei.hms.location.ActivityConversionData) this.getHInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityTransitionEvent.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityTransitionEvent object
     */
    public static org.xms.g.location.ActivityTransitionEvent dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityTransitionEvent) param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransitionEvent.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityConversionData;
    }
    
    private class HImpl extends com.huawei.hms.location.ActivityConversionData {
        
        public boolean equals(java.lang.Object param0) {
            return org.xms.g.location.ActivityTransitionEvent.this.equals(param0);
        }
        
        public int getActivityType() {
            return org.xms.g.location.ActivityTransitionEvent.this.getActivityType();
        }
        
        public long getElapsedTimeFromReboot() {
            return org.xms.g.location.ActivityTransitionEvent.this.getElapsedRealTimeNanos();
        }
        
        public int getConversionType() {
            return org.xms.g.location.ActivityTransitionEvent.this.getTransitionType();
        }
        
        public int hashCode() {
            return org.xms.g.location.ActivityTransitionEvent.this.hashCode();
        }
        
        public java.lang.String toString() {
            return org.xms.g.location.ActivityTransitionEvent.this.toString();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.ActivityTransitionEvent.this.writeToParcel(param0, param1);
        }
        
        public boolean equalsCallSuper(java.lang.Object param0) {
            return super.equals(param0);
        }
        
        public int getActivityTypeCallSuper() {
            return super.getActivityType();
        }
        
        public long getElapsedTimeFromRebootCallSuper() {
            return super.getElapsedTimeFromReboot();
        }
        
        public int getConversionTypeCallSuper() {
            return super.getConversionType();
        }
        
        public int hashCodeCallSuper() {
            return super.hashCode();
        }
        
        public java.lang.String toStringCallSuper() {
            return super.toString();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public HImpl() {
            super();
        }
        
        public HImpl(int param0, int param1, long param2) {
            super(param0, param1, param2);
        }
    }
}