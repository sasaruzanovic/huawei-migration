package org.xms.g.location;

/**
 * A data class representing a geographic location result from the fused location provider..<br/>
 * Wrapper class for com.huawei.hms.location.LocationResult, but only the HMS API are provided.</br>
 * com.huawei.hms.location.LocationResult: Location data information class.</br>
 */
public final class LocationResult extends org.xms.g.utils.XObject implements android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.LocationResult.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/locationresult-0000001051066114-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/locationresult-0000001051066114-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationResult createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.LocationResult hReturn = com.huawei.hms.location.LocationResult.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationResult(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.LocationResult[] newArray(int param0) {
            return new org.xms.g.location.LocationResult[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationResult.LocationResult(org.xms.g.utils.XBox) constructor of LocationResult with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationResult.create(java.util.List<android.location.Location>) Creates a LocationResult for the given locations.<br/>
     * com.huawei.hms.location.LocationResult.create(java.util.List<android.location.Location>): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section1653772245611">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section1653772245611</a><br/>
     * 
     * @param  param0 the given locations
     * @return LocationResult instance
     */
    public static org.xms.g.location.LocationResult create(java.util.List<android.location.Location> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.LocationResult.create(org.xms.g.utils.Utils.mapList2GH(param0, true))");
        com.huawei.hms.location.LocationResult hReturn = com.huawei.hms.location.LocationResult.create(org.xms.g.utils.Utils.mapList2GH(param0, true));
        return ((hReturn) == null ? null : (new org.xms.g.location.LocationResult(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.LocationResult.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.huawei.hms.location.LocationResult.equals(java.lang.Object)
     * @param  param0 the other instance
     * @return true if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationResult) this.getHInstance()).equals(param0)");
        return ((com.huawei.hms.location.LocationResult) this.getHInstance()).equals(param0);
    }
    
    /**
     * org.xms.g.location.LocationResult.extractResult(android.content.Intent) Extracts the LocationResult from an Intent.<br/>
     * com.huawei.hms.location.LocationResult.extractResult(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section191671312526">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section191671312526</a><br/>
     * 
     * @param  param0 intent instance
     * @return a LocationResult, or null if the Intent doesn't contain a result
     */
    public static org.xms.g.location.LocationResult extractResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.LocationResult.extractResult(param0)");
        com.huawei.hms.location.LocationResult hReturn = com.huawei.hms.location.LocationResult.extractResult(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.LocationResult(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.LocationResult.getLastLocation() Returns the most recent location available in this result, or null if no locations are available.<br/>
     * com.huawei.hms.location.LocationResult.getLastLocation(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section17829111811544">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section17829111811544</a><br/>
     * 
     * @return the available location of the last request
     */
    public final android.location.Location getLastLocation() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationResult) this.getHInstance()).getLastLocation()");
        return ((com.huawei.hms.location.LocationResult) this.getHInstance()).getLastLocation();
    }
    
    /**
     * org.xms.g.location.LocationResult.getLocations() Returns locations computed, ordered from oldest to newest.<br/>
     * com.huawei.hms.location.LocationResult.getLocations(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section0637438308">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section0637438308</a><br/>
     * 
     * @return a set of available locations
     */
    public final java.util.List<android.location.Location> getLocations() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationResult) this.getHInstance()).getLocations()");
        return ((com.huawei.hms.location.LocationResult) this.getHInstance()).getLocations();
    }
    
    /**
     * org.xms.g.location.LocationResult.hasResult(android.content.Intent) Returns true if an Intent contains a LocationResult.<br/>
     * com.huawei.hms.location.LocationResult.hasResult(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section122471915428">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationresult-0000001051066114-V5#EN-US_TOPIC_0000001051066114__section122471915428</a><br/>
     * 
     * @param  param0 intent instance
     * @return true if the intent contains a LocationResult, false otherwise
     */
    public static boolean hasResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.LocationResult.hasResult(param0)");
        return com.huawei.hms.location.LocationResult.hasResult(param0);
    }
    
    /**
     * org.xms.g.location.LocationResult.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.huawei.hms.location.LocationResult.hashCode()
     * @return a hash code value
     */
    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationResult) this.getHInstance()).hashCode()");
        return ((com.huawei.hms.location.LocationResult) this.getHInstance()).hashCode();
    }
    
    /**
     * org.xms.g.location.LocationResult.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.LocationResult.toString()
     * @return A character string after being converted.
     */
    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationResult) this.getHInstance()).toString()");
        return ((com.huawei.hms.location.LocationResult) this.getHInstance()).toString();
    }
    
    /**
     * org.xms.g.location.LocationResult.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.LocationResult.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationResult) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.location.LocationResult) this.getHInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationResult.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationResult.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationResult object
     */
    public static org.xms.g.location.LocationResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationResult) param0);
    }
    
    /**
     * org.xms.g.location.LocationResult.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.LocationResult;
    }
}