package org.xms.g.location;

/**
 * Specifies the list of geofences to be monitored and how the geofence notifications should be reported..<br/>
 * Wrapper class for com.huawei.hms.location.GeofenceRequest, but only the HMS API are provided.</br>
 * com.huawei.hms.location.GeofenceRequest: Geofence request class.</br>
 */
public class GeofencingRequest extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.GeofenceRequest.CREATOR: <a href=""></a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.GeofencingRequest createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.GeofenceRequest hReturn = com.huawei.hms.location.GeofenceRequest.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.GeofencingRequest(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.GeofencingRequest[] newArray(int param0) {
            return new org.xms.g.location.GeofencingRequest[param0];
        }
    };
    
    /**
     * org.xms.g.location.GeofencingRequest.GeofencingRequest(org.xms.g.utils.XBox) constructor of GeofencingRequest with XBox<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public GeofencingRequest(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getINITIAL_TRIGGER_DWELL() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceRequest.DWELL_INIT_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section18651015308">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section18651015308</a><br/>
     * 
     * @return Constant Value.A flag indicating that geofencing service should trigger GEOFENCE_TRANSITION_DWELL notification at the moment when the geofence is added and if the device is already inside that geofence for some time.
     */
    public static int getINITIAL_TRIGGER_DWELL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceRequest.DWELL_INIT_CONVERSION");
        return com.huawei.hms.location.GeofenceRequest.DWELL_INIT_CONVERSION;
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getINITIAL_TRIGGER_ENTER() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceRequest.ENTER_INIT_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section1254213477129">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section1254213477129</a><br/>
     * 
     * @return Constant Value.A flag indicating that geofencing service should trigger GEOFENCE_TRANSITION_ENTER notification at the moment when the geofence is added and if the device is already inside that geofence
     */
    public static int getINITIAL_TRIGGER_ENTER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceRequest.ENTER_INIT_CONVERSION");
        return com.huawei.hms.location.GeofenceRequest.ENTER_INIT_CONVERSION;
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getINITIAL_TRIGGER_EXIT() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceRequest.EXIT_INIT_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section63171927192919">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section63171927192919</a><br/>
     * 
     * @return Constant Value.A flag indicating that geofencing service should trigger GEOFENCE_TRANSITION_EXIT notification at the moment when the geofence is added and if the device is already outside that geofence
     */
    public static int getINITIAL_TRIGGER_EXIT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceRequest.EXIT_INIT_CONVERSION");
        return com.huawei.hms.location.GeofenceRequest.EXIT_INIT_CONVERSION;
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getGeofences() Gets the list of geofences to be monitored.<br/>
     * com.huawei.hms.location.GeofenceRequest.getGeofences(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section9254532817">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section9254532817</a><br/>
     * 
     * @return the list of geofences to be monitored
     */
    public java.util.List<org.xms.g.location.Geofence> getGeofences() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).getGeofences()");
        java.util.List hReturn = ((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).getGeofences();
        return ((java.util.List) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<com.huawei.hms.location.Geofence, org.xms.g.location.Geofence>() {
            
            public org.xms.g.location.Geofence apply(com.huawei.hms.location.Geofence param0) {
                return new org.xms.g.location.Geofence.XImpl(new org.xms.g.utils.XBox(param0));
            }
        }));
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getInitialTrigger() Gets the triggering behavior at the moment when the geofences are added.<br/>
     * com.huawei.hms.location.GeofenceRequest.getInitConversions(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section1842912113331">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencerequest-0000001050746171-V5#EN-US_TOPIC_0000001050746171__section1842912113331</a><br/>
     * 
     * @return the triggering behavior at the moment when the geofences are added. Its a bit-wise of INITIAL_TRIGGER_ENTER and INITIAL_TRIGGER_EXIT
     */
    public int getInitialTrigger() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).getInitConversions()");
        return ((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).getInitConversions();
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.GeofenceRequest.toString()
     * @return A character string after being converted.
     */
    public java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).toString()");
        return ((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).toString();
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.GeofenceRequest.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.location.GeofenceRequest) this.getHInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.GeofencingRequest.<br/>
     * 
     * @param  param0 the input object
     * @return casted GeofencingRequest object
     */
    public static org.xms.g.location.GeofencingRequest dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.GeofencingRequest) param0);
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.GeofenceRequest;
    }
    
/**
 * .<br/>
 * Wrapper class for , but only the HMS API are provided.</br>
 * : </br>
 */
    public static final class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
            this.setHInstance(new com.huawei.hms.location.GeofenceRequest.Builder());
        }
        
        public org.xms.g.location.GeofencingRequest.Builder addGeofence(org.xms.g.location.Geofence param0) throws java.lang.NullPointerException, java.lang.IllegalArgumentException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).createGeofence(((param0) == null ? null : (param0.getHInstanceGeofence())))");
            com.huawei.hms.location.GeofenceRequest.Builder hReturn = ((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).createGeofence(((param0) == null ? null : (param0.getHInstanceGeofence())));
            return ((hReturn) == null ? null : (new org.xms.g.location.GeofencingRequest.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.GeofencingRequest.Builder addGeofences(java.util.List<org.xms.g.location.Geofence> param0) throws java.lang.IllegalArgumentException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).createGeofenceList(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, true)))");
            com.huawei.hms.location.GeofenceRequest.Builder hReturn = ((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).createGeofenceList(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, true)));
            return ((hReturn) == null ? null : (new org.xms.g.location.GeofencingRequest.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.GeofencingRequest build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).build()");
            com.huawei.hms.location.GeofenceRequest hReturn = ((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).build();
            return ((hReturn) == null ? null : (new org.xms.g.location.GeofencingRequest(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.GeofencingRequest.Builder setInitialTrigger(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).setInitConversions(param0)");
            com.huawei.hms.location.GeofenceRequest.Builder hReturn = ((com.huawei.hms.location.GeofenceRequest.Builder) this.getHInstance()).setInitConversions(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.GeofencingRequest.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public static org.xms.g.location.GeofencingRequest.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.location.GeofencingRequest.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.GeofenceRequest.Builder;
        }
    }
}