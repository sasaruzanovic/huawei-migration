package org.xms.g.location;

/**
 * The main entry point for interacting with the fused location provider..<br/>
 * Wrapper class for , but only the HMS API are provided.</br>
 * : </br>
 */
public interface FusedLocationProviderApi extends org.xms.g.utils.XInterface {
    
    /**
     * org.xms.g.location.FusedLocationProviderApi.getKEY_LOCATION_CHANGED() Return the constant value.<br/>
     * 
     * @return Constant Value.Key used for a Bundle extra holding a Location value when a location change is broadcast using a PendingIntent
     */
    public static java.lang.String getKEY_LOCATION_CHANGED() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.FusedLocationProviderApi.getKEY_MOCK_LOCATION() Return the constant value.<br/>
     * 
     * @return Constant Value.Key used for the Bundle extra in Location object holding a boolean indicating whether the location was set using setMockLocation(GoogleApiClient, Location). If the value is false this extra is not set
     */
    public static java.lang.String getKEY_MOCK_LOCATION() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> flushLocations(org.xms.g.common.api.ExtensionApiClient param0);
    
    /**
     * XMS does not provide this api.
     */
    public android.location.Location getLastLocation(org.xms.g.common.api.ExtensionApiClient param0);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.location.LocationAvailability getLocationAvailability(org.xms.g.common.api.ExtensionApiClient param0);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> removeLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, android.app.PendingIntent param1);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> removeLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationListener param1);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> removeLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationCallback param1);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, org.xms.g.location.LocationListener param2) throws java.lang.IllegalStateException;
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, org.xms.g.location.LocationCallback param2, android.os.Looper param3);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, org.xms.g.location.LocationListener param2, android.os.Looper param3);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, android.app.PendingIntent param2);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> setMockLocation(org.xms.g.common.api.ExtensionApiClient param0, android.location.Location param1);
    
    /**
     * XMS does not provide this api.
     */
    public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> setMockMode(org.xms.g.common.api.ExtensionApiClient param0, boolean param1);
    
    default java.lang.Object getZInstanceFusedLocationProviderApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    default java.lang.Object getHInstanceFusedLocationProviderApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.FusedLocationProviderApi.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.FusedLocationProviderApi.<br/>
     * 
     * @param  param0 The input object
     * @return Casted FusedLocationProviderApi object
     */
    public static org.xms.g.location.FusedLocationProviderApi dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.FusedLocationProviderApi.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.location.FusedLocationProviderApi {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> flushLocations(org.xms.g.common.api.ExtensionApiClient param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public android.location.Location getLastLocation(org.xms.g.common.api.ExtensionApiClient param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.location.LocationAvailability getLocationAvailability(org.xms.g.common.api.ExtensionApiClient param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> removeLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, android.app.PendingIntent param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> removeLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationListener param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> removeLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationCallback param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, org.xms.g.location.LocationListener param2) throws java.lang.IllegalStateException {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, org.xms.g.location.LocationCallback param2, android.os.Looper param3) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, org.xms.g.location.LocationListener param2, android.os.Looper param3) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> requestLocationUpdates(org.xms.g.common.api.ExtensionApiClient param0, org.xms.g.location.LocationRequest param1, android.app.PendingIntent param2) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> setMockLocation(org.xms.g.common.api.ExtensionApiClient param0, android.location.Location param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> setMockMode(org.xms.g.common.api.ExtensionApiClient param0, boolean param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}