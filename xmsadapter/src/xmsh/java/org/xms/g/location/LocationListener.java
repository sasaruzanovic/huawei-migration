package org.xms.g.location;

/**
 * Used for receiving notifications from the FusedLocationProviderApi when the location has changed..<br/>
 * Wrapper class for , but only the HMS API are provided.</br>
 * : </br>
 */
public interface LocationListener extends org.xms.g.utils.XInterface {
    
    /**
     * XMS does not provide this api.
     */
    public void onLocationChanged(android.location.Location param0);
    
    default java.lang.Object getZInstanceLocationListener() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    default java.lang.Object getHInstanceLocationListener() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationListener.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationListener.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationListener object
     */
    public static org.xms.g.location.LocationListener dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationListener.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.location.LocationListener {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public void onLocationChanged(android.location.Location param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}