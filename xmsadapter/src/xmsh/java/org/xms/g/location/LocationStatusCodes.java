package org.xms.g.location;

/**
 * Status codes that can be returned to listeners to indicate the success or failure of an operation..<br/>
 * Wrapper class for com.huawei.hms.location.GeofenceErrorCodes, but only the HMS API are provided.</br>
 * com.huawei.hms.location.GeofenceErrorCodes: Geofence result codes.</br>
 */
public final class LocationStatusCodes extends org.xms.g.utils.XObject {
    
    /**
     * org.xms.g.location.LocationStatusCodes.LocationStatusCodes(org.xms.g.utils.XBox) constructor of LocationStatusCodes with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationStatusCodes(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getERROR() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.ERROR
     * @return Constant Value.An unspecified error occurred; no more specific information is available. The device logs may provide additional data
     */
    public static int getERROR() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.ERROR");
        return com.huawei.hms.location.GeofenceErrorCodes.ERROR;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getGEOFENCE_NOT_AVAILABLE() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_UNAVAILABLE: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1254213477129">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1254213477129</a><br/>
     * 
     * @return Constant Value.Geofence service is not available now. Typically this is because the user turned off location access in settings > location access
     */
    public static int getGEOFENCE_NOT_AVAILABLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_UNAVAILABLE");
        return com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_UNAVAILABLE;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getGEOFENCE_TOO_MANY_GEOFENCES() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_NUMBER_OVER_LIMIT: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section104412013581">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section104412013581</a><br/>
     * 
     * @return Constant Value.Your app has registered more than 100 geofences. Remove unused ones before adding new geofences
     */
    public static int getGEOFENCE_TOO_MANY_GEOFENCES() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_NUMBER_OVER_LIMIT");
        return com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_NUMBER_OVER_LIMIT;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getGEOFENCE_TOO_MANY_PENDING_INTENTS() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_PENDINGINTENT_OVER_LIMIT: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1312133196">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceerrorcodes-0000001051066110-V5#EN-US_TOPIC_0000001051066110__section1312133196</a><br/>
     * 
     * @return Constant Value.You have provided more than 5 different PendingIntents to the addGeofences(GoogleApiClient, GeofencingRequest, PendingIntent) call
     */
    public static int getGEOFENCE_TOO_MANY_PENDING_INTENTS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_PENDINGINTENT_OVER_LIMIT");
        return com.huawei.hms.location.GeofenceErrorCodes.GEOFENCE_PENDINGINTENT_OVER_LIMIT;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getSUCCESS() return the constant value.<br/>
     * com.huawei.hms.location.GeofenceErrorCodes.SUCCESS
     * @return Constant Value. The operation was successful
     */
    public static int getSUCCESS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceErrorCodes.SUCCESS");
        return com.huawei.hms.location.GeofenceErrorCodes.SUCCESS;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationStatusCodes.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationStatusCodes object
     */
    public static org.xms.g.location.LocationStatusCodes dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationStatusCodes) param0);
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.GeofenceErrorCodes;
    }
}