package org.xms.g.location;

/**
 * Result of an activity recognition..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityIdentificationResponse, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityIdentificationResponse: Activity identification result.</br>
 */
public class ActivityRecognitionResult extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.ActivityIdentificationResponse.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityidentificationresponse-0000001051066108-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityidentificationresponse-0000001051066108-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.ActivityRecognitionResult createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.ActivityIdentificationResponse hReturn = com.huawei.hms.location.ActivityIdentificationResponse.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.ActivityRecognitionResult(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.ActivityRecognitionResult[] newArray(int param0) {
            return new org.xms.g.location.ActivityRecognitionResult[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.ActivityRecognitionResult(org.xms.g.utils.XBox) Constructor of ActivityRecognitionResult with XBox<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityRecognitionResult(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.ActivityRecognitionResult(java.util.List<org.xms.g.location.DetectedActivity>,long,long) Constructs an ActivityRecognitionResult.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse(List<com.huawei.hms.locationActivityIdentificationData>,long,long): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section1346604418442">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section1346604418442</a><br/>
     * 
     * @param  param0 The activities that were detected, sorted by confidence (most probable first)
     * @param  param1 The UTC time of this detection, in milliseconds since January 1, 1970
     * @param  param2 Milliseconds since boot
     */
    public ActivityRecognitionResult(java.util.List<org.xms.g.location.DetectedActivity> param0, long param1, long param2) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, true)), param1, param2));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.ActivityRecognitionResult(org.xms.g.location.DetectedActivity,long,long) Constructs an ActivityRecognitionResult from a single activity.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse(com.huawei.hms.location.ActivityIdentificationData,long,long): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section48131849114415">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section48131849114415</a><br/>
     * 
     * @param  param0 The most probable activity of the device
     * @param  param1 The UTC time of this detection, in milliseconds since January 1, 1970
     * @param  param2 Milliseconds since boot
     */
    public ActivityRecognitionResult(org.xms.g.location.DetectedActivity param0, long param1, long param2) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(((com.huawei.hms.location.ActivityIdentificationData) ((param0) == null ? null : (param0.getHInstance()))), param1, param2));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.extractResult(android.content.Intent) Extracts the ActivityRecognitionResult from an Intent.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.getDataFromIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section9254532817">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section9254532817</a><br/>
     * 
     * @param  param0 Intent instance
     * @return an ActivityRecognitionResult, or null if the intent doesn't contain an ActivityRecognitionResult
     */
    public static org.xms.g.location.ActivityRecognitionResult extractResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationResponse.getDataFromIntent(param0)");
        com.huawei.hms.location.ActivityIdentificationResponse hReturn = com.huawei.hms.location.ActivityIdentificationResponse.getDataFromIntent(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.ActivityRecognitionResult(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.getActivityConfidence(int) Returns the confidence of the given activity type.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.getActivityPossibility(int): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section45000497546">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section45000497546</a><br/>
     * 
     * @param  param0 Activity type
     * @return The confidence of the given activity type
     */
    public int getActivityConfidence(int param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getActivityPossibility(param0)");
            return ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getActivityPossibility(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getActivityPossibilityCallSuper(param0)");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getActivityPossibilityCallSuper(param0);
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.getElapsedRealtimeMillis() Returns the elapsed real time of this detection in milliseconds since boot, including time spent in sleep as obtained by SystemClock.elapsedRealtime().<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.getElapsedTimeFromReboot(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section5515317145716">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section5515317145716</a><br/>
     * 
     * @return Timestamp, in nanoseconds
     */
    public long getElapsedRealtimeMillis() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getElapsedTimeFromReboot()");
            return ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getElapsedTimeFromReboot();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getElapsedTimeFromRebootCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getElapsedTimeFromRebootCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.getMostProbableActivity() Returns the most probable activity of the user.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.getMostActivityIdentification(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section119119581881">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section119119581881</a><br/>
     * 
     * @return The most probable activity of the user
     */
    public org.xms.g.location.DetectedActivity getMostProbableActivity() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getMostActivityIdentification()");
            com.huawei.hms.location.ActivityIdentificationData hReturn = ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getMostActivityIdentification();
            return ((hReturn) == null ? null : (new org.xms.g.location.DetectedActivity(new org.xms.g.utils.XBox(hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getMostActivityIdentificationCallSuper()");
            com.huawei.hms.location.ActivityIdentificationData hReturn = ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getMostActivityIdentificationCallSuper();
            return ((hReturn) == null ? null : (new org.xms.g.location.DetectedActivity(new org.xms.g.utils.XBox(hReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.getProbableActivities() Returns the list of activities that were detected with the confidence value associated with each activity. The activities are sorted by most probable activity first.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.getActivityIdentificationDatas(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section178201311108">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section178201311108</a><br/>
     * 
     * @return The list of activities that were detected with the confidence value associated with each activity
     */
    public java.util.List<org.xms.g.location.DetectedActivity> getProbableActivities() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getActivityIdentificationDatas()");
            java.util.List hReturn = ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getActivityIdentificationDatas();
            return ((java.util.List) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<com.huawei.hms.location.ActivityIdentificationData, org.xms.g.location.DetectedActivity>() {
                
                public org.xms.g.location.DetectedActivity apply(com.huawei.hms.location.ActivityIdentificationData param0) {
                    return new org.xms.g.location.DetectedActivity(new org.xms.g.utils.XBox(param0));
                }
            }));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getActivityIdentificationDatasCallSuper()");
            java.util.List hReturn = ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getActivityIdentificationDatasCallSuper();
            return ((java.util.List) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<com.huawei.hms.location.ActivityIdentificationData, org.xms.g.location.DetectedActivity>() {
                
                public org.xms.g.location.DetectedActivity apply(com.huawei.hms.location.ActivityIdentificationData param0) {
                    return new org.xms.g.location.DetectedActivity(new org.xms.g.utils.XBox(param0));
                }
            }));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.getTime() Returns the UTC time of this detection, in milliseconds since January 1, 1970.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.getTime(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section33798408106">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section33798408106</a><br/>
     * 
     * @return Timestamp, in milliseconds
     */
    public long getTime() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getTime()");
            return ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).getTime();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getTimeCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).getTimeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.hasResult(android.content.Intent) Returns true if an Intent contains an ActivityRecognitionResult.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.containDataFromIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section1066834181110">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationresponse-0000001051066108#EN-US_TOPIC_0000001051066108__section1066834181110</a><br/>
     * 
     * @param  param0 Intent sent by the activity
     * @return True if the intent contains an ActivityRecognitionResult, false otherwise or the given intent is null
     */
    public static boolean hasResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityIdentificationResponse.containDataFromIntent(param0)");
        return com.huawei.hms.location.ActivityIdentificationResponse.containDataFromIntent(param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.toString()
     * @return A character string after being converted
     */
    public java.lang.String toString() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).toString()");
            return ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).toString();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).toStringCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).toStringCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.ActivityIdentificationResponse.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).writeToParcel(param0, param1)");
            ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).writeToParcelCallSuper(param0, param1)");
            ((HImpl) ((com.huawei.hms.location.ActivityIdentificationResponse) this.getHInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityRecognitionResult.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityRecognitionResult object
     */
    public static org.xms.g.location.ActivityRecognitionResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityRecognitionResult) param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionResult.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityIdentificationResponse;
    }
    
    private class HImpl extends com.huawei.hms.location.ActivityIdentificationResponse {
        
        public int getActivityPossibility(int param0) {
            return org.xms.g.location.ActivityRecognitionResult.this.getActivityConfidence(param0);
        }
        
        public long getElapsedTimeFromReboot() {
            return org.xms.g.location.ActivityRecognitionResult.this.getElapsedRealtimeMillis();
        }
        
        public com.huawei.hms.location.ActivityIdentificationData getMostActivityIdentification() {
            org.xms.g.location.DetectedActivity xResult = org.xms.g.location.ActivityRecognitionResult.this.getMostProbableActivity();
            return ((com.huawei.hms.location.ActivityIdentificationData) ((xResult) == null ? null : (xResult.getHInstance())));
        }
        
        public java.util.List<com.huawei.hms.location.ActivityIdentificationData> getActivityIdentificationDatas() {
            return ((java.util.List) org.xms.g.utils.Utils.mapList2GH(org.xms.g.location.ActivityRecognitionResult.this.getProbableActivities(), true));
        }
        
        public long getTime() {
            return org.xms.g.location.ActivityRecognitionResult.this.getTime();
        }
        
        public java.lang.String toString() {
            return org.xms.g.location.ActivityRecognitionResult.this.toString();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.ActivityRecognitionResult.this.writeToParcel(param0, param1);
        }
        
        public int getActivityPossibilityCallSuper(int param0) {
            return super.getActivityPossibility(param0);
        }
        
        public long getElapsedTimeFromRebootCallSuper() {
            return super.getElapsedTimeFromReboot();
        }
        
        public com.huawei.hms.location.ActivityIdentificationData getMostActivityIdentificationCallSuper() {
            return super.getMostActivityIdentification();
        }
        
        public java.util.List<com.huawei.hms.location.ActivityIdentificationData> getActivityIdentificationDatasCallSuper() {
            return super.getActivityIdentificationDatas();
        }
        
        public long getTimeCallSuper() {
            return super.getTime();
        }
        
        public java.lang.String toStringCallSuper() {
            return super.toString();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public HImpl() {
            super();
        }
        
        public HImpl(java.util.List<com.huawei.hms.location.ActivityIdentificationData> param0, long param1, long param2) {
            super(param0, param1, param2);
        }
        
        public HImpl(com.huawei.hms.location.ActivityIdentificationData param0, long param1, long param2) {
            super(param0, param1, param2);
        }
    }
}