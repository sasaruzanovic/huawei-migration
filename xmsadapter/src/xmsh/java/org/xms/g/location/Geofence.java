package org.xms.g.location;

/**
 * Represents a geographical region, also known as a geofence. Geofences can be monitored by geofencer service. And when the user crosses the boundary of a geofence, an alert will be generated..<br/>
 * Wrapper class for com.huawei.hms.location.Geofence, but only the HMS API are provided.</br>
 * com.huawei.hms.location.Geofence: Geofence entity interface.</br>
 */
public interface Geofence extends org.xms.g.utils.XInterface {
    
    /**
     * org.xms.g.location.Geofence.getGEOFENCE_TRANSITION_DWELL() Return the constant value.<br/>
     * com.huawei.hms.location.Geofence.DWELL_GEOFENCE_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section56571345113615">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section56571345113615</a><br/>
     * 
     * @return Constant Value.The transition type indicating that the user enters and dwells in geofences for a given period of time
     */
    public static int getGEOFENCE_TRANSITION_DWELL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.Geofence.DWELL_GEOFENCE_CONVERSION");
        return com.huawei.hms.location.Geofence.DWELL_GEOFENCE_CONVERSION;
    }
    
    /**
     * org.xms.g.location.Geofence.getGEOFENCE_TRANSITION_ENTER() Return the constant value.<br/>
     * com.huawei.hms.location.Geofence.ENTER_GEOFENCE_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section1254213477129">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section1254213477129</a><br/>
     * 
     * @return Constant Value.The transition type indicating that the user enters the geofence(s)
     */
    public static int getGEOFENCE_TRANSITION_ENTER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.Geofence.ENTER_GEOFENCE_CONVERSION");
        return com.huawei.hms.location.Geofence.ENTER_GEOFENCE_CONVERSION;
    }
    
    /**
     * org.xms.g.location.Geofence.getGEOFENCE_TRANSITION_EXIT() Return the constant value.<br/>
     * com.huawei.hms.location.Geofence.EXIT_GEOFENCE_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section104194423618">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section104194423618</a><br/>
     * 
     * @return Constant Value.The transition type indicating that the user exits the geofence(s)
     */
    public static int getGEOFENCE_TRANSITION_EXIT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.Geofence.EXIT_GEOFENCE_CONVERSION");
        return com.huawei.hms.location.Geofence.EXIT_GEOFENCE_CONVERSION;
    }
    
    /**
     * org.xms.g.location.Geofence.getNEVER_EXPIRE() Return the constant value.<br/>
     * com.huawei.hms.location.Geofence.GEOFENCE_NEVER_EXPIRE: <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section1786514633613">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section1786514633613</a><br/>
     * 
     * @return Constant Value.Expiration value that indicates the geofence should never expire
     */
    public static long getNEVER_EXPIRE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.Geofence.GEOFENCE_NEVER_EXPIRE");
        return com.huawei.hms.location.Geofence.GEOFENCE_NEVER_EXPIRE;
    }
    
    /**
     * org.xms.g.location.Geofence.getRequestId() Returns the request ID of this geofence.<br/>
     * com.huawei.hms.location.Geofence.getUniqueId(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section58926443498">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofence-0000001050706130-V5#EN-US_TOPIC_0000001050706130__section58926443498</a><br/>
     * 
     * @return The request ID
     */
    public java.lang.String getRequestId();
    
    default java.lang.Object getZInstanceGeofence() {
        return getHInstanceGeofence();
    }
    
    default com.huawei.hms.location.Geofence getHInstanceGeofence() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.huawei.hms.location.Geofence) ((org.xms.g.utils.XGettable) this).getHInstance());
        }
        return new com.huawei.hms.location.Geofence() {
            
            public java.lang.String getUniqueId() {
                return org.xms.g.location.Geofence.this.getRequestId();
            }
        };
    }
    
    /**
     * org.xms.g.location.Geofence.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.Geofence.<br/>
     * 
     * @param  param0 The input object
     * @return Casted Geofence object
     */
    public static org.xms.g.location.Geofence dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.Geofence) param0);
    }
    
    /**
     * org.xms.g.location.Geofence.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.Geofence;
        }
        return param0 instanceof org.xms.g.location.Geofence;
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.location.Geofence {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.String getRequestId() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence) this.getHInstance()).getUniqueId()");
            return ((com.huawei.hms.location.Geofence) this.getHInstance()).getUniqueId();
        }
    }
    
/**
 * .<br/>
 * Wrapper class for , but only the HMS API are provided.</br>
 * : </br>
 */
    public static final class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
            this.setHInstance(new com.huawei.hms.location.Geofence.Builder());
        }
        
        public final org.xms.g.location.Geofence build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).build()");
            com.huawei.hms.location.Geofence hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).build();
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setCircularRegion(double param0, double param1, float param2) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setRoundArea(param0, param1, param2)");
            com.huawei.hms.location.Geofence.Builder hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setRoundArea(param0, param1, param2);
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setExpirationDuration(long param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setValidContinueTime(param0)");
            com.huawei.hms.location.Geofence.Builder hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setValidContinueTime(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setLoiteringDelay(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setDwellDelayTime(param0)");
            com.huawei.hms.location.Geofence.Builder hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setDwellDelayTime(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setNotificationResponsiveness(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setNotificationInterval(param0)");
            com.huawei.hms.location.Geofence.Builder hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setNotificationInterval(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setRequestId(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setUniqueId(param0)");
            com.huawei.hms.location.Geofence.Builder hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setUniqueId(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setTransitionTypes(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setConversions(param0)");
            com.huawei.hms.location.Geofence.Builder hReturn = ((com.huawei.hms.location.Geofence.Builder) this.getHInstance()).setConversions(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public static org.xms.g.location.Geofence.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.location.Geofence.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.Geofence.Builder;
        }
    }
}