package org.xms.g.location;

/**
 * The main entry point for interacting with the location settings-enabler APIs..<br/>
 * Wrapper class for com.huawei.hms.location.SettingsClient, but only the HMS API are provided.</br>
 * com.huawei.hms.location.SettingsClient: Class for checking location-related settings.</br>
 */
public class SettingsClient extends org.xms.g.common.api.ExtensionApi<org.xms.g.common.api.Api.ApiOptions.NoOptions> {
    
    /**
     * org.xms.g.location.SettingsClient.SettingsClient(org.xms.g.utils.XBox) constructor of SettingsClient with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public SettingsClient(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.SettingsClient.checkLocationSettings(org.xms.g.location.LocationSettingsRequest) Checks if the relevant system settings are enabled on the device to carry out the desired location requests.<br/>
     * com.huawei.hms.location.SettingsClient.checkLocationSettings(com.huawei.hms.location.LocationSettingsRequest): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/settingsclient-0000001051066118-V5#EN-US_TOPIC_0000001051066118__section1653772245611">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/settingsclient-0000001051066118-V5#EN-US_TOPIC_0000001051066118__section1653772245611</a><br/>
     * 
     * @param  param0 an object that contains all the location requirements that the client is interested in
     * @return the task
     */
    public org.xms.g.tasks.Task<org.xms.g.location.LocationSettingsResponse> checkLocationSettings(org.xms.g.location.LocationSettingsRequest param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.SettingsClient) this.getHInstance()).checkLocationSettings(((com.huawei.hms.location.LocationSettingsRequest) ((param0) == null ? null : (param0.getHInstance()))))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.SettingsClient) this.getHInstance()).checkLocationSettings(((com.huawei.hms.location.LocationSettingsRequest) ((param0) == null ? null : (param0.getHInstance()))));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * XMS does not provide this api.
     */
    public java.lang.Object getApiKey() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.SettingsClient.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.SettingsClient.<br/>
     * 
     * @param  param0 the input object
     * @return casted SettingsClient object
     */
    public static org.xms.g.location.SettingsClient dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.location.SettingsClient) {
            return ((org.xms.g.location.SettingsClient) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.location.SettingsClient hReturn = ((com.huawei.hms.location.SettingsClient) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.location.SettingsClient(new org.xms.g.utils.XBox(hReturn));
        }
        return ((org.xms.g.location.SettingsClient) param0);
    }
    
    /**
     * org.xms.g.location.SettingsClient.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.SettingsClient;
    }
}