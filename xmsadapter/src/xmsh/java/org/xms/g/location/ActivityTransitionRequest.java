package org.xms.g.location;

/**
 * The request object for apps to get notified when user's activity changes..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityConversionRequest, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityConversionRequest: Activity conversion request body.</br>
 */
public class ActivityTransitionRequest extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.ActivityConversionRequest.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversionrequest-0000001050986181-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversionrequest-0000001050986181-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.ActivityTransitionRequest createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.ActivityConversionRequest hReturn = com.huawei.hms.location.ActivityConversionRequest.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.ActivityTransitionRequest(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.ActivityTransitionRequest[] newArray(int param0) {
            return new org.xms.g.location.ActivityTransitionRequest[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.ActivityTransitionRequest(org.xms.g.utils.XBox) Constructor of ActivityTransitionRequest with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityTransitionRequest(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.ActivityTransitionRequest(java.util.List<org.xms.g.location.ActivityTransition>) Creates an ActivityTransitionRequest object by specifying a list of interested activity transitions.<br/>
     * com.huawei.hms.location.ActivityConversionRequest(java.util.List<com.huawei.hms.location.ActivityConversionInfo>): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionrequest-0000001050986181#EN-US_TOPIC_0000001050986181__section114762417137">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionrequest-0000001050986181#EN-US_TOPIC_0000001050986181__section114762417137</a><br/>
     * 
     * @param  param0 A list of interested activity transitions
     */
    public ActivityTransitionRequest(java.util.List<org.xms.g.location.ActivityTransition> param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, true))));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.getIS_SAME_TRANSITION() Return the Comparator.<br/>
     * com.huawei.hms.location.ActivityConversionRequest.IS_EQUAL_CONVERSION
     * @return The comparator used to determine if two transitions are the same
     */
    public static java.util.Comparator getIS_SAME_TRANSITION() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityConversionRequest.IS_EQUAL_CONVERSION");
        return com.huawei.hms.location.ActivityConversionRequest.IS_EQUAL_CONVERSION;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.huawei.hms.location.ActivityConversionRequest.equals(java.lang.Object)
     * @param  param0 The other instance
     * @return True if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).equals(param0)");
            return ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).equals(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).equalsCallSuper(param0)");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).equalsCallSuper(param0);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.huawei.hms.location.ActivityConversionRequest.hashCode()
     * @return A hash code value
     */
    public int hashCode() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).hashCode()");
            return ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).hashCode();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).hashCodeCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).hashCodeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.serializeToIntentExtra(android.content.Intent) Serializes this request to the given intent.<br/>
     * com.huawei.hms.location.ActivityConversionRequest.setDataToIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionrequest-0000001050986181#EN-US_TOPIC_0000001050986181__section9254532817">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionrequest-0000001050986181#EN-US_TOPIC_0000001050986181__section9254532817</a><br/>
     * 
     * @param  param0 The intent to serailize this object to
     */
    public void serializeToIntentExtra(android.content.Intent param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).setDataToIntent(param0)");
            ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).setDataToIntent(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).setDataToIntentCallSuper(param0)");
            ((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).setDataToIntentCallSuper(param0);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.ActivityConversionRequest.toString()
     * @return A character string after being converted
     */
    public java.lang.String toString() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).toString()");
            return ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).toString();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).toStringCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).toStringCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.ActivityConversionRequest.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).writeToParcel(param0, param1)");
            ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).writeToParcelCallSuper(param0, param1)");
            ((HImpl) ((com.huawei.hms.location.ActivityConversionRequest) this.getHInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityTransitionRequest.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityTransitionRequest object
     */
    public static org.xms.g.location.ActivityTransitionRequest dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityTransitionRequest) param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransitionRequest.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityConversionRequest;
    }
    
    private class HImpl extends com.huawei.hms.location.ActivityConversionRequest {
        
        public boolean equals(java.lang.Object param0) {
            return org.xms.g.location.ActivityTransitionRequest.this.equals(param0);
        }
        
        public int hashCode() {
            return org.xms.g.location.ActivityTransitionRequest.this.hashCode();
        }
        
        public void setDataToIntent(android.content.Intent param0) {
            org.xms.g.location.ActivityTransitionRequest.this.serializeToIntentExtra(param0);
        }
        
        public java.lang.String toString() {
            return org.xms.g.location.ActivityTransitionRequest.this.toString();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.ActivityTransitionRequest.this.writeToParcel(param0, param1);
        }
        
        public boolean equalsCallSuper(java.lang.Object param0) {
            return super.equals(param0);
        }
        
        public int hashCodeCallSuper() {
            return super.hashCode();
        }
        
        public void setDataToIntentCallSuper(android.content.Intent param0) {
            super.setDataToIntent(param0);
        }
        
        public java.lang.String toStringCallSuper() {
            return super.toString();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public HImpl() {
            super();
        }
        
        protected HImpl(android.os.Parcel param0) {
            super(param0);
        }
        
        public HImpl(java.util.List<com.huawei.hms.location.ActivityConversionInfo> param0) {
            super(param0);
        }
    }
}