package org.xms.g.location;

/**
 * Used for receiving notifications with the location information..<br/>
 * Wrapper class for com.huawei.hms.location.LocationCallback, but only the HMS API are provided.</br>
 * com.huawei.hms.location.LocationCallback: Callback class for reporting location information.</br>
 */
public class LocationCallback extends org.xms.g.utils.XObject {
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.LocationCallback.LocationCallback(org.xms.g.utils.XBox) constructor of LocationCallback with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationCallback(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.LocationCallback.LocationCallback() constructor of LocationCallback.<br/>
     * com.huawei.hms.location.LocationCallback(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationcallback-0000001050706140-V5">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationcallback-0000001050706140-V5</a><br/>
     * 
     */
    public LocationCallback() {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl());
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.LocationCallback.onLocationAvailability(org.xms.g.location.LocationAvailability) Called when there is a change in the availability of location data.<br/>
     * com.huawei.hms.location.LocationCallback.onLocationAvailability(com.huawei.hms.location.LocationAvailability): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationcallback-0000001050706140-V5#EN-US_TOPIC_0000001050706140__section1653772245611">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationcallback-0000001050706140-V5#EN-US_TOPIC_0000001050706140__section1653772245611</a><br/>
     * 
     * @param  param0 The current status of location availability
     */
    public void onLocationAvailability(org.xms.g.location.LocationAvailability param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationCallback) this.getHInstance()).onLocationAvailability(((com.huawei.hms.location.LocationAvailability) ((param0) == null ? null : (param0.getHInstance()))))");
            ((com.huawei.hms.location.LocationCallback) this.getHInstance()).onLocationAvailability(((com.huawei.hms.location.LocationAvailability) ((param0) == null ? null : (param0.getHInstance()))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.LocationCallback) this.getHInstance())).onLocationAvailabilityCallSuper(((com.huawei.hms.location.LocationAvailability) ((param0) == null ? null : (param0.getHInstance()))))");
            ((HImpl) ((com.huawei.hms.location.LocationCallback) this.getHInstance())).onLocationAvailabilityCallSuper(((com.huawei.hms.location.LocationAvailability) ((param0) == null ? null : (param0.getHInstance()))));
        }
    }
    
    /**
     * org.xms.g.location.LocationCallback.onLocationResult(org.xms.g.location.LocationResult) Called when device location information is available.<br/>
     * com.huawei.hms.location.LocationCallback.onLocationResult(com.huawei.hms.location.LocationResult): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationcallback-0000001050706140-V5#EN-US_TOPIC_0000001050706140__section18517246182511">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationcallback-0000001050706140-V5#EN-US_TOPIC_0000001050706140__section18517246182511</a><br/>
     * 
     * @param  param0 The latest location result available
     */
    public void onLocationResult(org.xms.g.location.LocationResult param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationCallback) this.getHInstance()).onLocationResult(((com.huawei.hms.location.LocationResult) ((param0) == null ? null : (param0.getHInstance()))))");
            ((com.huawei.hms.location.LocationCallback) this.getHInstance()).onLocationResult(((com.huawei.hms.location.LocationResult) ((param0) == null ? null : (param0.getHInstance()))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.LocationCallback) this.getHInstance())).onLocationResultCallSuper(((com.huawei.hms.location.LocationResult) ((param0) == null ? null : (param0.getHInstance()))))");
            ((HImpl) ((com.huawei.hms.location.LocationCallback) this.getHInstance())).onLocationResultCallSuper(((com.huawei.hms.location.LocationResult) ((param0) == null ? null : (param0.getHInstance()))));
        }
    }
    
    /**
     * org.xms.g.location.LocationCallback.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationCallback.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationCallback object
     */
    public static org.xms.g.location.LocationCallback dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationCallback) param0);
    }
    
    /**
     * org.xms.g.location.LocationCallback.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.LocationCallback;
    }
    
    private class HImpl extends com.huawei.hms.location.LocationCallback {
        
        public void onLocationAvailability(com.huawei.hms.location.LocationAvailability param0) {
            org.xms.g.location.LocationCallback.this.onLocationAvailability(((param0) == null ? null : (new org.xms.g.location.LocationAvailability(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void onLocationResult(com.huawei.hms.location.LocationResult param0) {
            org.xms.g.location.LocationCallback.this.onLocationResult(((param0) == null ? null : (new org.xms.g.location.LocationResult(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void onLocationAvailabilityCallSuper(com.huawei.hms.location.LocationAvailability param0) {
            super.onLocationAvailability(param0);
        }
        
        public void onLocationResultCallSuper(com.huawei.hms.location.LocationResult param0) {
            super.onLocationResult(param0);
        }
        
        public HImpl() {
            super();
        }
    }
}