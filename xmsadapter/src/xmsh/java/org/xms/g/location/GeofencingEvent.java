package org.xms.g.location;

/**
 * Represents an event from the GeofencingApi API..<br/>
 * Wrapper class for com.huawei.hms.location.GeofenceData, but only the HMS API are provided.</br>
 * com.huawei.hms.location.GeofenceData: Geofence event.</br>
 */
public class GeofencingEvent extends org.xms.g.utils.XObject {
    
    /**
     * org.xms.g.location.GeofencingEvent.GeofencingEvent(org.xms.g.utils.XBox) Constructor of GeofencingEvent with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public GeofencingEvent(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.fromIntent(android.content.Intent) Creates a GeofencingEvent object from the given intent.<br/>
     * com.huawei.hms.location.GeofenceData.getDataFromIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section9254532817">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section9254532817</a><br/>
     * 
     * @param  param0 The intent to extract the geofencing event data from
     * @return A GeofencingEvent object or null if the given intent is null
     */
    public static org.xms.g.location.GeofencingEvent fromIntent(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.GeofenceData.getDataFromIntent(param0)");
        com.huawei.hms.location.GeofenceData hReturn = com.huawei.hms.location.GeofenceData.getDataFromIntent(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.GeofencingEvent(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.getErrorCode() Returns the error code that explains the error that triggered the intent specified in fromIntent(Intent).<br/>
     * com.huawei.hms.location.GeofenceData.getErrorCode(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section85449184712">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section85449184712</a><br/>
     * 
     * @return the error code specified in GeofenceStatusCodes or -1 if hasError() returns false
     */
    public int getErrorCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceData) this.getHInstance()).getErrorCode()");
        return ((com.huawei.hms.location.GeofenceData) this.getHInstance()).getErrorCode();
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.getGeofenceTransition() Returns the transition type of the geofence transition alert.<br/>
     * com.huawei.hms.location.GeofenceData.getConversion(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section11578123265110">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section11578123265110</a><br/>
     * 
     * @return -1 if the intent specified in fromIntent(Intent) is not generated for a transition alert; Otherwise returns the GEOFENCE_TRANSITION_ flags value defined in Geofence
     */
    public int getGeofenceTransition() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceData) this.getHInstance()).getConversion()");
        return ((com.huawei.hms.location.GeofenceData) this.getHInstance()).getConversion();
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.getTriggeringGeofences() Returns a list of geofences that triggered this geofence transition alert.<br/>
     * com.huawei.hms.location.GeofenceData.getConvertingGeofenceList(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section13616111912545">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section13616111912545</a><br/>
     * 
     * @return A list of geofences that triggered this geofence transition alert or null if the intent specified in fromIntent(Intent) is not generated for a geofence transition alert
     */
    public java.util.List<org.xms.g.location.Geofence> getTriggeringGeofences() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceData) this.getHInstance()).getConvertingGeofenceList()");
        java.util.List hReturn = ((com.huawei.hms.location.GeofenceData) this.getHInstance()).getConvertingGeofenceList();
        return ((java.util.List) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<com.huawei.hms.location.Geofence, org.xms.g.location.Geofence>() {
            
            public org.xms.g.location.Geofence apply(com.huawei.hms.location.Geofence param0) {
                return new org.xms.g.location.Geofence.XImpl(new org.xms.g.utils.XBox(param0));
            }
        }));
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.getTriggeringLocation() Gets the location that triggered the geofence transition.<br/>
     * com.huawei.hms.location.GeofenceData.getConvertingLocation(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section43512495554">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofencedata-0000001050986185-V5#EN-US_TOPIC_0000001050986185__section43512495554</a><br/>
     * 
     * @return the location that triggered this geofence alert or null if it's not included in the intent specified in fromIntent(Intent)
     */
    public android.location.Location getTriggeringLocation() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceData) this.getHInstance()).getConvertingLocation()");
        return ((com.huawei.hms.location.GeofenceData) this.getHInstance()).getConvertingLocation();
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.hasError() Whether an error triggered this intent.<br/>
     * com.huawei.hms.location.GeofenceData.isFailure()
     * @return true if an error triggered the intent specified in fromIntent(Intent), otherwise false
     */
    public boolean hasError() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceData) this.getHInstance()).isFailure()");
        return ((com.huawei.hms.location.GeofenceData) this.getHInstance()).isFailure();
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.GeofencingEvent.<br/>
     * 
     * @param  param0 the input object
     * @return casted GeofencingEvent object
     */
    public static org.xms.g.location.GeofencingEvent dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.GeofencingEvent) param0);
    }
    
    /**
     * org.xms.g.location.GeofencingEvent.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.GeofenceData;
    }
}