package org.xms.g.location;

/**
 * Result of checking settings, indicates whether a dialog should be shown to ask the user's consent to change their settings..<br/>
 * Wrapper class for com.huawei.hms.location.LocationSettingsResult, but only the HMS API are provided.</br>
 * com.huawei.hms.location.LocationSettingsResult: Location setting result.</br>
 */
public final class LocationSettingsResult extends org.xms.g.utils.XObject implements org.xms.g.common.api.Result, android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.LocationSettingsResult.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/locationsettingsresult-0000001050746177-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/locationsettingsresult-0000001050746177-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationSettingsResult createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.LocationSettingsResult hReturn = com.huawei.hms.location.LocationSettingsResult.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationSettingsResult(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.LocationSettingsResult[] newArray(int param0) {
            return new org.xms.g.location.LocationSettingsResult[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationSettingsResult.LocationSettingsResult(org.xms.g.utils.XBox) constructor of LocationSettingsResult with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.getLocationSettingsStates() Retrieves the location settings states.<br/>
     * com.huawei.hms.location.LocationSettingsResult.getLocationSettingsStates(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsresult-0000001050746177-V5#EN-US_TOPIC_0000001050746177__section31921512113417">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsresult-0000001050746177-V5#EN-US_TOPIC_0000001050746177__section31921512113417</a><br/>
     * 
     * @return the location settings states
     */
    public final org.xms.g.location.LocationSettingsStates getLocationSettingsStates() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsResult) this.getHInstance()).getLocationSettingsStates()");
        com.huawei.hms.location.LocationSettingsStates hReturn = ((com.huawei.hms.location.LocationSettingsResult) this.getHInstance()).getLocationSettingsStates();
        return ((hReturn) == null ? null : (new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.getStatus() Returns the status of this result.<br/>
     * com.huawei.hms.location.LocationSettingsResult.getStatus(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsresult-0000001050746177-V5#EN-US_TOPIC_0000001050746177__section1653772245611">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsresult-0000001050746177-V5#EN-US_TOPIC_0000001050746177__section1653772245611</a><br/>
     * 
     * @return the status of this result
     */
    public final org.xms.g.common.api.Status getStatus() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsResult) this.getHInstance()).getStatus()");
        com.huawei.hms.support.api.client.Status hReturn = ((com.huawei.hms.location.LocationSettingsResult) this.getHInstance()).getStatus();
        return ((hReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.LocationSettingsResult.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsResult) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.location.LocationSettingsResult) this.getHInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsResult.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsResult object
     */
    public static org.xms.g.location.LocationSettingsResult dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.location.LocationSettingsResult) {
            return ((org.xms.g.location.LocationSettingsResult) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.location.LocationSettingsResult hReturn = ((com.huawei.hms.location.LocationSettingsResult) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.location.LocationSettingsResult(new org.xms.g.utils.XBox(hReturn));
        }
        return ((org.xms.g.location.LocationSettingsResult) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.LocationSettingsResult;
    }
}