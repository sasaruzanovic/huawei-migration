package org.xms.g.location;

/**
 * The main entry point for interacting with activity recognition..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityIdentificationService, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityIdentificationService: Interaction access point of activity identification.</br>
 */
public class ActivityRecognitionClient extends org.xms.g.common.api.ExtensionApi<org.xms.g.common.api.Api.ApiOptions.NoOptions> {
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.ActivityRecognitionClient(org.xms.g.utils.XBox) Constructor of ActivityRecognitionClient with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityRecognitionClient(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.ActivityRecognitionClient(android.app.Activity) Wrapper constructor of ActivityIdentificationService and ActivityRecognitionClient.<br/>
     * com.huawei.hms.location.ActivityIdentificationService(android.app.Activity): <a href="https://developer.huawei.com/consumer/en/doc/activityidentificationservice">https://developer.huawei.com/consumer/en/doc/activityidentificationservice</a><br/>
     * 
     * @param  param0 Activity of android app
     */
    public ActivityRecognitionClient(android.app.Activity param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(param0));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.ActivityRecognitionClient(android.content.Context) Create a new instance of Client for use in an Context.<br/>
     * com.huawei.hms.location.ActivityIdentificationService(android.content.Context): <a href="https://developer.huawei.com/consumer/en/doc/activityidentificationservice">https://developer.huawei.com/consumer/en/doc/activityidentificationservice</a><br/>
     * 
     * @param  param0 Context instance
     */
    public ActivityRecognitionClient(android.content.Context param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(param0));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.removeActivityTransitionUpdates(android.app.PendingIntent) Removes activity transition updates associated with the given pendingIntent.<br/>
     * com.huawei.hms.location.ActivityIdentificationService.deleteActivityConversionUpdates(android.app.PendingIntent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section17434718321">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section17434718321</a><br/>
     * 
     * @param  param0 The associated PendingIntent of the activity transition request which is to be removed
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> removeActivityTransitionUpdates(android.app.PendingIntent param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).deleteActivityConversionUpdates(param0)");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).deleteActivityConversionUpdates(param0);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).deleteActivityConversionUpdatesCallSuper(param0)");
            com.huawei.hmf.tasks.Task hReturn = ((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).deleteActivityConversionUpdatesCallSuper(param0);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.removeActivityUpdates(android.app.PendingIntent) Removes all activity updates for the specified PendingIntent.<br/>
     * com.huawei.hms.location.ActivityIdentificationService.deleteActivityIdentificationUpdates(android.app.PendingIntent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section18411141614334">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section18411141614334</a><br/>
     * 
     * @param  param0 The PendingIntent that was used in requestActivityUpdates(long, PendingIntent) or is equal as defined by equals(Object)
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> removeActivityUpdates(android.app.PendingIntent param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).deleteActivityIdentificationUpdates(param0)");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).deleteActivityIdentificationUpdates(param0);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).deleteActivityIdentificationUpdatesCallSuper(param0)");
            com.huawei.hmf.tasks.Task hReturn = ((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).deleteActivityIdentificationUpdatesCallSuper(param0);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.requestActivityTransitionUpdates(org.xms.g.location.ActivityTransitionRequest,android.app.PendingIntent) Activity Recognition Transition API provides an ability for apps to subscribe to activity transitional conditions (enter, exit).<br/>
     * com.huawei.hms.location.ActivityIdentificationService.createActivityConversionUpdates(com.huawei.hms.location.ActivityConversionRequest,android.app.PendingIntent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section461610311343">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section461610311343</a><br/>
     * 
     * @param  param0 The interested activity transitions
     * @param  param1 The PendingIntent used to generate the callback intent when one of the interested transition has happened
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> requestActivityTransitionUpdates(org.xms.g.location.ActivityTransitionRequest param0, android.app.PendingIntent param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).createActivityConversionUpdates(((com.huawei.hms.location.ActivityConversionRequest) ((param0) == null ? null : (param0.getHInstance()))), param1)");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).createActivityConversionUpdates(((com.huawei.hms.location.ActivityConversionRequest) ((param0) == null ? null : (param0.getHInstance()))), param1);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).createActivityConversionUpdatesCallSuper(((com.huawei.hms.location.ActivityConversionRequest) ((param0) == null ? null : (param0.getHInstance()))), param1)");
            com.huawei.hmf.tasks.Task hReturn = ((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).createActivityConversionUpdatesCallSuper(((com.huawei.hms.location.ActivityConversionRequest) ((param0) == null ? null : (param0.getHInstance()))), param1);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.requestActivityUpdates(long,android.app.PendingIntent) Register for activity recognition updates.<br/>
     * com.huawei.hms.location.ActivityIdentificationService.createActivityIdentificationUpdates(long,android.app.PendingIntent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section177364246397">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityidentificationservice-0000001050986183#EN-US_TOPIC_0000001050986183__section177364246397</a><br/>
     * 
     * @param  param0 The desired time between activity detections. Larger values will result in fewer activity detections while improving battery life. A value of 0 will result in activity detections at the fastest possible rate
     * @param  param1 A PendingIntent to be sent for each activity detection
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> requestActivityUpdates(long param0, android.app.PendingIntent param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).createActivityIdentificationUpdates(param0, param1)");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance()).createActivityIdentificationUpdates(param0, param1);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).createActivityIdentificationUpdatesCallSuper(param0, param1)");
            com.huawei.hmf.tasks.Task hReturn = ((HImpl) ((com.huawei.hms.location.ActivityIdentificationService) this.getHInstance())).createActivityIdentificationUpdatesCallSuper(param0, param1);
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
    }
    
    /**
     * XMS does not provide this api.
     */
    public java.lang.Object getApiKey() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityRecognitionClient.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityRecognitionClient object
     */
    public static org.xms.g.location.ActivityRecognitionClient dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.location.ActivityRecognitionClient) {
            return ((org.xms.g.location.ActivityRecognitionClient) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.location.ActivityIdentificationService hReturn = ((com.huawei.hms.location.ActivityIdentificationService) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.location.ActivityRecognitionClient(new org.xms.g.utils.XBox(hReturn));
        }
        return ((org.xms.g.location.ActivityRecognitionClient) param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityIdentificationService;
    }
    
    private class HImpl extends com.huawei.hms.location.ActivityIdentificationService {
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> deleteActivityConversionUpdates(android.app.PendingIntent param0) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.removeActivityTransitionUpdates(param0);
            return ((com.huawei.hmf.tasks.Task) ((xResult) == null ? null : (xResult.getHInstance())));
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> deleteActivityIdentificationUpdates(android.app.PendingIntent param0) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.removeActivityUpdates(param0);
            return ((com.huawei.hmf.tasks.Task) ((xResult) == null ? null : (xResult.getHInstance())));
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> createActivityConversionUpdates(com.huawei.hms.location.ActivityConversionRequest param0, android.app.PendingIntent param1) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.requestActivityTransitionUpdates(((param0) == null ? null : (new org.xms.g.location.ActivityTransitionRequest(new org.xms.g.utils.XBox(param0)))), param1);
            return ((com.huawei.hmf.tasks.Task) ((xResult) == null ? null : (xResult.getHInstance())));
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> createActivityIdentificationUpdates(long param0, android.app.PendingIntent param1) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.requestActivityUpdates(param0, param1);
            return ((com.huawei.hmf.tasks.Task) ((xResult) == null ? null : (xResult.getHInstance())));
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> deleteActivityConversionUpdatesCallSuper(android.app.PendingIntent param0) {
            return super.deleteActivityConversionUpdates(param0);
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> deleteActivityIdentificationUpdatesCallSuper(android.app.PendingIntent param0) {
            return super.deleteActivityIdentificationUpdates(param0);
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> createActivityConversionUpdatesCallSuper(com.huawei.hms.location.ActivityConversionRequest param0, android.app.PendingIntent param1) {
            return super.createActivityConversionUpdates(param0, param1);
        }
        
        public com.huawei.hmf.tasks.Task<java.lang.Void> createActivityIdentificationUpdatesCallSuper(long param0, android.app.PendingIntent param1) {
            return super.createActivityIdentificationUpdates(param0, param1);
        }
        
        public HImpl(android.content.Context param0) {
            super(param0);
        }
        
        public HImpl(android.app.Activity param0) {
            super(param0);
        }
    }
}