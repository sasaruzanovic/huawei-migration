package org.xms.g.location;

/**
 * Successful response of checking settings..<br/>
 * Wrapper class for com.huawei.hms.location.LocationSettingsResponse, but only the HMS API are provided.</br>
 * com.huawei.hms.location.LocationSettingsResponse: Response to the location check request. This class is used to obtain the status of the current location-related settings.</br>
 */
public class LocationSettingsResponse extends org.xms.g.common.api.Response<org.xms.g.location.LocationSettingsResult> {
    
    /**
     * org.xms.g.location.LocationSettingsResponse.LocationSettingsResponse(org.xms.g.utils.XBox) constructor of LocationSettingsResponse with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsResponse(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResponse.getLocationSettingsStates() Obtains the location setting status.<br/>
     * com.huawei.hms.location.LocationSettingsResponse.getLocationSettingsStates(): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsresponse-0000001050706142-V5#EN-US_TOPIC_0000001050706142__section1653772245611">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsresponse-0000001050706142-V5#EN-US_TOPIC_0000001050706142__section1653772245611</a><br/>
     * 
     * @return Location setting status
     */
    public org.xms.g.location.LocationSettingsStates getLocationSettingsStates() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsResponse) this.getHInstance()).getLocationSettingsStates()");
        com.huawei.hms.location.LocationSettingsStates hReturn = ((com.huawei.hms.location.LocationSettingsResponse) this.getHInstance()).getLocationSettingsStates();
        return ((hReturn) == null ? null : (new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsResponse.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsResponse.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsResponse object
     */
    public static org.xms.g.location.LocationSettingsResponse dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationSettingsResponse) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResponse.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.LocationSettingsResponse;
    }
}