package org.xms.g.location;

/**
 * The main entry point for interacting with the geofencing APIs..<br/>
 * Wrapper class for com.huawei.hms.location.GeofenceService, but only the HMS API are provided.</br>
 * com.huawei.hms.location.GeofenceService: Geofence location class, which is used for interaction during location.</br>
 */
public class GeofencingClient extends org.xms.g.common.api.ExtensionApi<org.xms.g.common.api.Api.ApiOptions.NoOptions> {
    
    /**
     * org.xms.g.location.GeofencingClient.GeofencingClient(org.xms.g.utils.XBox) Constructor of GeofencingClient with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public GeofencingClient(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.GeofencingClient.addGeofences(org.xms.g.location.GeofencingRequest,android.app.PendingIntent) Sets alerts to be notified when the device enters or exits one of the specified geofences.<br/>
     * com.huawei.hms.location.GeofenceService.createGeofenceList(com.huawei.hms.location.GeofenceRequest,android.app.PendingIntent): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceservice-0000001050986187-V5#EN-US_TOPIC_0000001050986187__section9254532817">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceservice-0000001050986187-V5#EN-US_TOPIC_0000001050986187__section9254532817</a><br/>
     * 
     * @param  param0 Geofencing request that include a list of geofences to be added and related triggering behavior. The request must be created using GeofencingRequest.Builder
     * @param  param1 A pending intent that will be used to generate an intent when matched geofence transition is observed
     * @return The task
     */
    public org.xms.g.tasks.Task<java.lang.Void> addGeofences(org.xms.g.location.GeofencingRequest param0, android.app.PendingIntent param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceService) this.getHInstance()).createGeofenceList(((com.huawei.hms.location.GeofenceRequest) ((param0) == null ? null : (param0.getHInstance()))), param1)");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.GeofenceService) this.getHInstance()).createGeofenceList(((com.huawei.hms.location.GeofenceRequest) ((param0) == null ? null : (param0.getHInstance()))), param1);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.GeofencingClient.removeGeofences(java.util.List<java.lang.String>) Removes geofences by their request IDs. Request ID is specified when you create a Geofence by calling setRequestId(String).<br/>
     * com.huawei.hms.location.GeofenceService.deleteGeofenceList(java.util.List): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceservice-0000001050986187-V5#EN-US_TOPIC_0000001050986187__section8918132319409">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceservice-0000001050986187-V5#EN-US_TOPIC_0000001050986187__section8918132319409</a><br/>
     * 
     * @param  param0 A list of request IDs of geofences that need to be removed
     * @return The task
     */
    public org.xms.g.tasks.Task<java.lang.Void> removeGeofences(java.util.List<java.lang.String> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceService) this.getHInstance()).deleteGeofenceList(org.xms.g.utils.Utils.mapList2GH(param0, true))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.GeofenceService) this.getHInstance()).deleteGeofenceList(org.xms.g.utils.Utils.mapList2GH(param0, true));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.GeofencingClient.removeGeofences(android.app.PendingIntent) Removes all geofences associated with the given pendingIntent.<br/>
     * com.huawei.hms.location.GeofenceService.deleteGeofenceList(android.app.PendingIntent): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceservice-0000001050986187-V5#EN-US_TOPIC_0000001050986187__section1270519314305">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/geofenceservice-0000001050986187-V5#EN-US_TOPIC_0000001050986187__section1270519314305</a><br/>
     * 
     * @param  param0 The pending intent associated with the geofences that need to be removed
     * @return The task
     */
    public org.xms.g.tasks.Task<java.lang.Void> removeGeofences(android.app.PendingIntent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.GeofenceService) this.getHInstance()).deleteGeofenceList(param0)");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.location.GeofenceService) this.getHInstance()).deleteGeofenceList(param0);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * XMS does not provide this api.
     */
    public java.lang.Object getApiKey() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.GeofencingClient.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.GeofencingClient.<br/>
     * 
     * @param  param0 The input object
     * @return Casted GeofencingClient object
     */
    public static org.xms.g.location.GeofencingClient dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.location.GeofencingClient) {
            return ((org.xms.g.location.GeofencingClient) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.location.GeofenceService hReturn = ((com.huawei.hms.location.GeofenceService) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.location.GeofencingClient(new org.xms.g.utils.XBox(hReturn));
        }
        return ((org.xms.g.location.GeofencingClient) param0);
    }
    
    /**
     * org.xms.g.location.GeofencingClient.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.GeofenceService;
    }
}