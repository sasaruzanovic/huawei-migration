package org.xms.g.location;

/**
 * Stores the current states of all location-related settings..<br/>
 * Wrapper class for com.huawei.hms.location.LocationSettingsStates, but only the HMS API are provided.</br>
 * com.huawei.hms.location.LocationSettingsStates: Current location-related setting status.</br>
 */
public final class LocationSettingsStates extends org.xms.g.utils.XObject implements android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.LocationSettingsStates.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationSettingsStates createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.LocationSettingsStates hReturn = com.huawei.hms.location.LocationSettingsStates.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.LocationSettingsStates[] newArray(int param0) {
            return new org.xms.g.location.LocationSettingsStates[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationSettingsStates.LocationSettingsStates(org.xms.g.utils.XBox) constructor of LocationSettingsStates with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsStates(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.fromIntent(android.content.Intent) Retrieves the location settings states from the intent extras.<br/>
     * com.huawei.hms.location.LocationSettingsStates.fromIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsstates-0000001050706144-V5#EN-US_TOPIC_0000001050706144__section1653772245611">https://developer.huawei.com/consumer/en/doc/HMSCore-References-V5/locationsettingsstates-0000001050706144-V5#EN-US_TOPIC_0000001050706144__section1653772245611</a><br/>
     * 
     * @param  param0 Intent instance
     * @return the location settings states
     */
    public static org.xms.g.location.LocationSettingsStates fromIntent(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.LocationSettingsStates.fromIntent(param0)");
        com.huawei.hms.location.LocationSettingsStates hReturn = com.huawei.hms.location.LocationSettingsStates.fromIntent(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isBlePresent() Whether BLE is present on the device.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isBlePresent(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if BLE is present on the device
     */
    public final boolean isBlePresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isBlePresent()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isBlePresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isBleUsable() Whether BLE is enabled and is usable by the app.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isBleUsable(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if BLE is enabled and is usable by the app
     */
    public final boolean isBleUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isBleUsable()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isBleUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isGpsPresent() Whether GPS provider is present on the device.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isGpsPresent(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if GPS provider is present on the device
     */
    public final boolean isGpsPresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isGpsPresent()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isGpsPresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isGpsUsable() Whether GPS provider is enabled and is usable by the app.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isGpsUsable(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if GPS provider is enabled and is usable by the app
     */
    public final boolean isGpsUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isGpsUsable()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isGpsUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isLocationPresent() Whether location is present on the device.This method returns true when either GPS or network location provider is present.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isLocationPresent(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if location is present on the device
     */
    public final boolean isLocationPresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isLocationPresent()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isLocationPresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isLocationUsable() Whether location is enabled and is usable by the app.This method returns true when either GPS or network location provider is usable.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isLocationUsable(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if location is enabled and is usable by the app
     */
    public final boolean isLocationUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isLocationUsable()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isLocationUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isNetworkLocationPresent() Whether network location provider is present on the device.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isNetworkLocationPresent(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if network location provider is present on the device
     */
    public final boolean isNetworkLocationPresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isNetworkLocationPresent()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isNetworkLocationPresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isNetworkLocationUsable() Whether network location provider is enabled and usable by the app.<br/>
     * com.huawei.hms.location.LocationSettingsStates.isNetworkLocationUsable(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4">https://developer.huawei.com/consumer/en/doc/development/HMS-References/locationsettingsstates-v4</a><br/>
     * 
     * @return true if network location provider is enabled and usable by the app
     */
    public final boolean isNetworkLocationUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isNetworkLocationUsable()");
        return ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).isNetworkLocationUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.LocationSettingsStates.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.location.LocationSettingsStates) this.getHInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsStates.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsStates object
     */
    public static org.xms.g.location.LocationSettingsStates dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationSettingsStates) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.LocationSettingsStates;
    }
}