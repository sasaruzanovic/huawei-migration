package org.xms.g.location;

/**
 * Represents an activity and the transition of it. For instance start to walk; stop running etc..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityConversionInfo, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityConversionInfo: Activity conversion.</br>
 */
public class ActivityTransition extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.ActivityConversionInfo.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversioninfo-0000001050746165-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversioninfo-0000001050746165-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.ActivityTransition createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.ActivityConversionInfo hReturn = com.huawei.hms.location.ActivityConversionInfo.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.ActivityTransition(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.ActivityTransition[] newArray(int param0) {
            return new org.xms.g.location.ActivityTransition[param0];
        }
    };
    
    /**
     * org.xms.g.location.ActivityTransition.ActivityTransition(org.xms.g.utils.XBox) Constructor of ActivityTransition with XBox<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityTransition(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransition.getACTIVITY_TRANSITION_ENTER() Return the Constant Value.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.ENTER_ACTIVITY_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section1254213477129">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section1254213477129</a><br/>
     * 
     * @return The constant value that user enters the given activity
     */
    public static int getACTIVITY_TRANSITION_ENTER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityConversionInfo.ENTER_ACTIVITY_CONVERSION");
        return com.huawei.hms.location.ActivityConversionInfo.ENTER_ACTIVITY_CONVERSION;
    }
    
    /**
     * org.xms.g.location.ActivityTransition.getACTIVITY_TRANSITION_EXIT() Return the Constant Value.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.EXIT_ACTIVITY_CONVERSION: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section1528611994020">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section1528611994020</a><br/>
     * 
     * @return the constant value that user exits the given activity
     */
    public static int getACTIVITY_TRANSITION_EXIT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityConversionInfo.EXIT_ACTIVITY_CONVERSION");
        return com.huawei.hms.location.ActivityConversionInfo.EXIT_ACTIVITY_CONVERSION;
    }
    
    /**
     * org.xms.g.location.ActivityTransition.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.equals(java.lang.Object)
     * @param  param0 The other instance
     * @return True if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).equals(param0)");
        return ((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).equals(param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransition.getActivityType() Gets the type of the activity to be detected.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.getActivityType(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section9194145513470">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section9194145513470</a><br/>
     * 
     * @return The type of the activity
     */
    public int getActivityType() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).getActivityType()");
        return ((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).getActivityType();
    }
    
    /**
     * org.xms.g.location.ActivityTransition.getTransitionType() Gets the interested transition type. It's one of the ACTIVITY_TRANSITION_xxx constants.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.getConversionType(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section9254532817">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversioninfo-0000001050746165#EN-US_TOPIC_0000001050746165__section9254532817</a><br/>
     * 
     * @return The interested transition type
     */
    public int getTransitionType() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).getConversionType()");
        return ((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).getConversionType();
    }
    
    /**
     * org.xms.g.location.ActivityTransition.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.hashCode()
     * @return A hash code value
     */
    public int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).hashCode()");
        return ((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).hashCode();
    }
    
    /**
     * org.xms.g.location.ActivityTransition.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.toString()
     * @return A character string after being converted
     */
    public java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).toString()");
        return ((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).toString();
    }
    
    /**
     * org.xms.g.location.ActivityTransition.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.ActivityConversionInfo.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.location.ActivityConversionInfo) this.getHInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * org.xms.g.location.ActivityTransition.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityTransition.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityTransition object
     */
    public static org.xms.g.location.ActivityTransition dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityTransition) param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransition.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityConversionInfo;
    }
    
/**
 * .<br/>
 * Wrapper class for , but only the HMS API are provided.</br>
 * : </br>
 */
    public static class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
            this.setHInstance(new com.huawei.hms.location.ActivityConversionInfo.Builder());
        }
        
        public org.xms.g.location.ActivityTransition build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo.Builder) this.getHInstance()).build()");
            com.huawei.hms.location.ActivityConversionInfo hReturn = ((com.huawei.hms.location.ActivityConversionInfo.Builder) this.getHInstance()).build();
            return ((hReturn) == null ? null : (new org.xms.g.location.ActivityTransition(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.location.ActivityTransition.Builder setActivityTransition(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo.Builder) this.getHInstance()).setConversionType(param0)");
            com.huawei.hms.location.ActivityConversionInfo.Builder hReturn = ((com.huawei.hms.location.ActivityConversionInfo.Builder) this.getHInstance()).setConversionType(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.ActivityTransition.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.location.ActivityTransition.Builder setActivityType(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionInfo.Builder) this.getHInstance()).setActivityType(param0)");
            com.huawei.hms.location.ActivityConversionInfo.Builder hReturn = ((com.huawei.hms.location.ActivityConversionInfo.Builder) this.getHInstance()).setActivityType(param0);
            return ((hReturn) == null ? null : (new org.xms.g.location.ActivityTransition.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public static org.xms.g.location.ActivityTransition.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.location.ActivityTransition.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityConversionInfo.Builder;
        }
    }
}