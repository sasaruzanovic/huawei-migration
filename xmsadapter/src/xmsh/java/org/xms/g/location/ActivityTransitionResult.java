package org.xms.g.location;

/**
 * Represents the result of activity transitions..<br/>
 * Wrapper class for com.huawei.hms.location.ActivityConversionResponse, but only the HMS API are provided.</br>
 * com.huawei.hms.location.ActivityConversionResponse: Activity conversion result.</br>
 */
public class ActivityTransitionResult extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.huawei.hms.location.ActivityConversionResponse.CREATOR: <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversionresponse-0000001051066106-V5">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References-V5/activityconversionresponse-0000001051066106-V5</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.ActivityTransitionResult createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.location.ActivityConversionResponse hReturn = com.huawei.hms.location.ActivityConversionResponse.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.ActivityTransitionResult(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.g.location.ActivityTransitionResult[] newArray(int param0) {
            return new org.xms.g.location.ActivityTransitionResult[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityTransitionResult.ActivityTransitionResult(org.xms.g.utils.XBox) Constructor of ActivityTransitionResult with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityTransitionResult(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.ActivityTransitionResult(java.util.List<org.xms.g.location.ActivityTransitionEvent>) Constructs a result by specifying a list of transition events.<br/>
     * com.huawei.hms.location.ActivityConversionResponse(java.util.List<com.huawei.hms.location.ActivityConversionData>): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section114762417137">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section114762417137</a><br/>
     * 
     * @param  param0 The transition events
     */
    public ActivityTransitionResult(java.util.List<org.xms.g.location.ActivityTransitionEvent> param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, true))));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.huawei.hms.location.ActivityConversionResponse.equals(java.lang.Object)
     * @param  param0 The other instance
     * @return True if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).equals(param0)");
            return ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).equals(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).equalsCallSuper(param0)");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).equalsCallSuper(param0);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.extractResult(android.content.Intent) Extracts the ActivityTransitionResult from the given Intent.<br/>
     * com.huawei.hms.location.ActivityConversionResponse.getDataFromIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section9254532817">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section9254532817</a><br/>
     * 
     * @param  param0 The Intent to extract the result from
     * @return The ActivityTransitionResult included in the given intent or return null if no such result is found in the given intent
     */
    public static org.xms.g.location.ActivityTransitionResult extractResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityConversionResponse.getDataFromIntent(param0)");
        com.huawei.hms.location.ActivityConversionResponse hReturn = com.huawei.hms.location.ActivityConversionResponse.getDataFromIntent(param0);
        return ((hReturn) == null ? null : (new org.xms.g.location.ActivityTransitionResult(new org.xms.g.utils.XBox(hReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.getTransitionEvents() Gets all the activity transition events in this result. The events are in ascending order of time, and may include events in the past.<br/>
     * com.huawei.hms.location.ActivityConversionResponse.getActivityConversionDatas(): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section11309956806">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section11309956806</a><br/>
     * 
     * @return List of activity transition events
     */
    public java.util.List<org.xms.g.location.ActivityTransitionEvent> getTransitionEvents() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).getActivityConversionDatas()");
            java.util.List hReturn = ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).getActivityConversionDatas();
            return ((java.util.List) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<com.huawei.hms.location.ActivityConversionData, org.xms.g.location.ActivityTransitionEvent>() {
                
                public org.xms.g.location.ActivityTransitionEvent apply(com.huawei.hms.location.ActivityConversionData param0) {
                    return new org.xms.g.location.ActivityTransitionEvent(new org.xms.g.utils.XBox(param0));
                }
            }));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).getActivityConversionDatasCallSuper()");
            java.util.List hReturn = ((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).getActivityConversionDatasCallSuper();
            return ((java.util.List) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<com.huawei.hms.location.ActivityConversionData, org.xms.g.location.ActivityTransitionEvent>() {
                
                public org.xms.g.location.ActivityTransitionEvent apply(com.huawei.hms.location.ActivityConversionData param0) {
                    return new org.xms.g.location.ActivityTransitionEvent(new org.xms.g.utils.XBox(param0));
                }
            }));
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.hasResult(android.content.Intent) Checks if the intent contains an ActivityTransitionResult.<br/>
     * com.huawei.hms.location.ActivityConversionResponse.containDataFromIntent(android.content.Intent): <a href="https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section1615213590310">https://developer.huawei.com/consumer/en/doc/development/HMSCore-References/activityconversionresponse-0000001051066106#EN-US_TOPIC_0000001051066106__section1615213590310</a><br/>
     * 
     * @param  param0 Intent instance
     * @return True if the intent contains ActivityTransitionResult; false otherwise
     */
    public static boolean hasResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.location.ActivityConversionResponse.containDataFromIntent(param0)");
        return com.huawei.hms.location.ActivityConversionResponse.containDataFromIntent(param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.huawei.hms.location.ActivityConversionResponse.hashCode()
     * @return A hash code value
     */
    public int hashCode() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).hashCode()");
            return ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).hashCode();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).hashCodeCallSuper()");
            return ((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).hashCodeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.huawei.hms.location.ActivityConversionResponse.writeToParcel(android.os.Parcel,int)
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).writeToParcel(param0, param1)");
            ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).writeToParcelCallSuper(param0, param1)");
            ((HImpl) ((com.huawei.hms.location.ActivityConversionResponse) this.getHInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityTransitionResult.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityTransitionResult object
     */
    public static org.xms.g.location.ActivityTransitionResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityTransitionResult) param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.location.ActivityConversionResponse;
    }
    
    private class HImpl extends com.huawei.hms.location.ActivityConversionResponse {
        
        public boolean equals(java.lang.Object param0) {
            return org.xms.g.location.ActivityTransitionResult.this.equals(param0);
        }
        
        public java.util.List<com.huawei.hms.location.ActivityConversionData> getActivityConversionDatas() {
            return ((java.util.List) org.xms.g.utils.Utils.mapList2GH(org.xms.g.location.ActivityTransitionResult.this.getTransitionEvents(), true));
        }
        
        public int hashCode() {
            return org.xms.g.location.ActivityTransitionResult.this.hashCode();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.ActivityTransitionResult.this.writeToParcel(param0, param1);
        }
        
        public boolean equalsCallSuper(java.lang.Object param0) {
            return super.equals(param0);
        }
        
        public java.util.List<com.huawei.hms.location.ActivityConversionData> getActivityConversionDatasCallSuper() {
            return super.getActivityConversionDatas();
        }
        
        public int hashCodeCallSuper() {
            return super.hashCode();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public HImpl() {
            super();
        }
        
        public HImpl(java.util.List<com.huawei.hms.location.ActivityConversionData> param0) {
            super(param0);
        }
    }
}