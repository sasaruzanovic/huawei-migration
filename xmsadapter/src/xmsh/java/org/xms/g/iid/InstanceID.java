package org.xms.g.iid;

public class InstanceID extends org.xms.g.utils.XObject {
    
    public InstanceID(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static java.lang.String getERROR_MAIN_THREAD() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getERROR_MISSING_INSTANCEID_SERVICE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getERROR_SERVICE_NOT_AVAILABLE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getERROR_TIMEOUT() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void deleteInstanceID() throws java.io.IOException {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void deleteToken(java.lang.String param0, java.lang.String param1) throws java.io.IOException {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public long getCreationTime() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getId() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.iid.InstanceID getInstance(android.content.Context param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getToken(java.lang.String param0, java.lang.String param1) throws java.io.IOException {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getToken(java.lang.String param0, java.lang.String param1, android.os.Bundle param2) throws java.io.IOException {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.iid.InstanceID dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}