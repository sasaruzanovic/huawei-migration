package org.xms.g.iid;

public class InstanceIDListenerService extends android.app.Service implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    
    public InstanceIDListenerService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
    }
    
    public InstanceIDListenerService() {
    }
    
    public void onTokenRefresh() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public android.os.IBinder onBind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.iid.InstanceIDListenerService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}