package org.xms.g.common.data;

public interface Freezable<XT> extends org.xms.g.utils.XInterface {
    
    public XT freeze();
    
    public boolean isDataValid();
    
    default java.lang.Object getZInstanceFreezable() {
        return getHInstanceFreezable();
    }
    
    default <T> com.huawei.hms.common.data.Freezable<T> getHInstanceFreezable() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.huawei.hms.common.data.Freezable<T>) ((org.xms.g.utils.XGettable) this).getHInstance());
        }
        return new com.huawei.hms.common.data.Freezable<T>() {
            
            public T freeze() {
                XT xResult = ((XT) org.xms.g.common.data.Freezable.this.freeze());
                return ((T) org.xms.g.utils.Utils.getInstanceInInterface(xResult, true));
            }
            
            public boolean isDataValid() {
                return org.xms.g.common.data.Freezable.this.isDataValid();
            }
        };
    }
    
    public static org.xms.g.common.data.Freezable dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.data.Freezable) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.common.data.Freezable;
        }
        return param0 instanceof org.xms.g.common.data.Freezable;
    }
    
    public static class XImpl<XT> extends org.xms.g.utils.XObject implements org.xms.g.common.data.Freezable<XT> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public XT freeze() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.Freezable) this.getHInstance()).freeze()");
            java.lang.Object hmsObj = ((com.huawei.hms.common.data.Freezable) this.getHInstance()).freeze();
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
        }
        
        public boolean isDataValid() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.Freezable) this.getHInstance()).isDataValid()");
            return ((com.huawei.hms.common.data.Freezable) this.getHInstance()).isDataValid();
        }
    }
}