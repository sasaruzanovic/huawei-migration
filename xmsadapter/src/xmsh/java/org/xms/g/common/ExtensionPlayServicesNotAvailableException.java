package org.xms.g.common;

public final class ExtensionPlayServicesNotAvailableException extends java.lang.Exception implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    
    public ExtensionPlayServicesNotAvailableException(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
    }
    
    public ExtensionPlayServicesNotAvailableException(int param0) {
        this.setHInstance(new com.huawei.hms.api.HuaweiServicesNotAvailableException(param0));
    }
    
    public int getErrorCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.api.HuaweiServicesNotAvailableException) this.getHInstance()).errorCode");
        return ((com.huawei.hms.api.HuaweiServicesNotAvailableException) this.getHInstance()).errorCode;
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.common.ExtensionPlayServicesNotAvailableException dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.ExtensionPlayServicesNotAvailableException) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.api.HuaweiServicesNotAvailableException;
    }
}