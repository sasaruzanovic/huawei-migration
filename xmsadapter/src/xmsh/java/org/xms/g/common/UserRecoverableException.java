package org.xms.g.common;

public class UserRecoverableException extends java.lang.Exception implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    private boolean wrapper = true;
    
    public UserRecoverableException(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
        wrapper = true;
    }
    
    public UserRecoverableException(java.lang.String param0, android.content.Intent param1) {
        this.setHInstance(new HImpl(param0, param1));
        wrapper = false;
    }
    
    public android.content.Intent getIntent() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.api.UserRecoverableException) this.getHInstance()).getIntent()");
            return ((com.huawei.hms.api.UserRecoverableException) this.getHInstance()).getIntent();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.api.UserRecoverableException) this.getHInstance())).getIntentCallSuper()");
            return ((HImpl) ((com.huawei.hms.api.UserRecoverableException) this.getHInstance())).getIntentCallSuper();
        }
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.common.UserRecoverableException dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.UserRecoverableException) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.api.UserRecoverableException;
    }
    
    private class HImpl extends com.huawei.hms.api.UserRecoverableException {
        
        public android.content.Intent getIntent() {
            return org.xms.g.common.UserRecoverableException.this.getIntent();
        }
        
        public android.content.Intent getIntentCallSuper() {
            return super.getIntent();
        }
        
        public HImpl(java.lang.String param0, android.content.Intent param1) {
            super(param0, param1);
        }
    }
}