package org.xms.g.common.api;

public abstract class PendingResult<XR extends org.xms.g.common.api.Result> extends org.xms.g.utils.XObject {
    
    public PendingResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public abstract XR await();
    
    public abstract XR await(long param0, java.util.concurrent.TimeUnit param1);
    
    public abstract void cancel();
    
    public abstract boolean isCanceled();
    
    public abstract void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0);
    
    public abstract void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0, long param1, java.util.concurrent.TimeUnit param2);
    
    public <XS extends org.xms.g.common.api.Result> org.xms.g.common.api.TransformedResult<XS> then(org.xms.g.common.api.ResultTransform<? super XR, ? extends XS> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).convertResult(((com.huawei.hms.support.api.client.ResultConvert) ((param0) == null ? null : (param0.getHInstance()))))");
        com.huawei.hms.support.api.client.ConvertedResult hReturn = ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).convertResult(((com.huawei.hms.support.api.client.ResultConvert) ((param0) == null ? null : (param0.getHInstance()))));
        return ((hReturn) == null ? null : (new org.xms.g.common.api.TransformedResult.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.common.api.PendingResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.PendingResult) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.support.api.client.PendingResult;
    }
    
    public static class XImpl<XR extends org.xms.g.common.api.Result> extends org.xms.g.common.api.PendingResult<XR> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public XR await() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).await()");
            java.lang.Object hmsObj = ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).await();
            return ((XR) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
        }
        
        public XR await(long param0, java.util.concurrent.TimeUnit param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).await(param0, param1)");
            java.lang.Object hmsObj = ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).await(param0, param1);
            return ((XR) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
        }
        
        public void cancel() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).cancel()");
            ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).cancel();
        }
        
        public boolean isCanceled() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).isCanceled()");
            return ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).isCanceled();
        }
        
        public void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).setResultCallback(((param0) == null ? null : (param0.getHInstanceResultCallback())))");
            ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).setResultCallback(((param0) == null ? null : (param0.getHInstanceResultCallback())));
        }
        
        public void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0, long param1, java.util.concurrent.TimeUnit param2) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).setResultCallback(((param0) == null ? null : (param0.getHInstanceResultCallback())), param1, param2)");
            ((com.huawei.hms.support.api.client.PendingResult) this.getHInstance()).setResultCallback(((param0) == null ? null : (param0.getHInstanceResultCallback())), param1, param2);
        }
    }
}