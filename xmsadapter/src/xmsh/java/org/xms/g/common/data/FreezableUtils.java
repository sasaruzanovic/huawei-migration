package org.xms.g.common.data;

public final class FreezableUtils extends org.xms.g.utils.XObject {
    
    public FreezableUtils(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public FreezableUtils() {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public static <XT, XE extends org.xms.g.common.data.Freezable<XT>> java.util.ArrayList<XT> freeze(XE[] param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.common.data.FreezableUtils.freeze(org.xms.g.utils.Utils.mapArray2GH(param0, com.huawei.hms.common.data.Freezable.class, true))");
        java.util.ArrayList hReturn = com.huawei.hms.common.data.FreezableUtils.freeze(org.xms.g.utils.Utils.mapArray2GH(param0, com.huawei.hms.common.data.Freezable.class, true));
        return ((java.util.ArrayList) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
            }
        }));
    }
    
    public static <XT, XE extends org.xms.g.common.data.Freezable<XT>> java.util.ArrayList<XT> freeze(java.util.ArrayList<XE> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.common.data.FreezableUtils.freeze(((java.util.ArrayList) org.xms.g.utils.Utils.mapList2GH(param0, true)))");
        java.util.ArrayList hReturn = com.huawei.hms.common.data.FreezableUtils.freeze(((java.util.ArrayList) org.xms.g.utils.Utils.mapList2GH(param0, true)));
        return ((java.util.ArrayList) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
            }
        }));
    }
    
    public static <XT, XE extends org.xms.g.common.data.Freezable<XT>> java.util.ArrayList<XT> freezeIterable(java.lang.Iterable<XE> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.common.data.FreezableUtils.freezeIterable(org.xms.g.utils.Utils.transformIterable(param0, e -> org.xms.g.utils.Utils.getInstanceInInterface(e, true)))");
        java.util.ArrayList hReturn = com.huawei.hms.common.data.FreezableUtils.freezeIterable(org.xms.g.utils.Utils.transformIterable(param0, e -> org.xms.g.utils.Utils.getInstanceInInterface(e, true)));
        return ((java.util.ArrayList) org.xms.g.utils.Utils.mapCollection(hReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
            }
        }));
    }
    
    public static org.xms.g.common.data.FreezableUtils dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.data.FreezableUtils) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.common.data.FreezableUtils;
    }
}