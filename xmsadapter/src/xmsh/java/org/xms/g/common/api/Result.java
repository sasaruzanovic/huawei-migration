package org.xms.g.common.api;

public interface Result extends org.xms.g.utils.XInterface {
    
    public org.xms.g.common.api.Status getStatus();
    
    default java.lang.Object getZInstanceResult() {
        return getHInstanceResult();
    }
    
    default com.huawei.hms.support.api.client.Result getHInstanceResult() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.huawei.hms.support.api.client.Result) ((org.xms.g.utils.XGettable) this).getHInstance());
        }
        return new com.huawei.hms.support.api.client.Result() {
            
            public com.huawei.hms.support.api.client.Status getStatus() {
                org.xms.g.common.api.Status xResult = org.xms.g.common.api.Result.this.getStatus();
                return ((com.huawei.hms.support.api.client.Status) ((xResult) == null ? null : (xResult.getHInstance())));
            }
        };
    }
    
    public static org.xms.g.common.api.Result dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.Result) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.support.api.client.Result;
        }
        return param0 instanceof org.xms.g.common.api.Result;
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.common.api.Result {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.common.api.Status getStatus() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Result) this.getHInstance()).getStatus()");
            com.huawei.hms.support.api.client.Status hReturn = ((com.huawei.hms.support.api.client.Result) this.getHInstance()).getStatus();
            return ((hReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn))));
        }
    }
}