package org.xms.g.common.images;

public final class Size extends org.xms.g.utils.XObject {
    
    public Size(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public Size(int param0, int param1) {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new com.huawei.hms.common.size.Size(param0, param1));
    }
    
    public final boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.size.Size) this.getHInstance()).equals(param0)");
        return ((com.huawei.hms.common.size.Size) this.getHInstance()).equals(param0);
    }
    
    public final int getHeight() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.size.Size) this.getHInstance()).getHeight()");
        return ((com.huawei.hms.common.size.Size) this.getHInstance()).getHeight();
    }
    
    public final int getWidth() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.size.Size) this.getHInstance()).getWidth()");
        return ((com.huawei.hms.common.size.Size) this.getHInstance()).getWidth();
    }
    
    public final int hashCode() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final org.xms.g.common.images.Size parseSize(java.lang.String param0) throws java.lang.NumberFormatException, java.lang.NullPointerException {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.size.Size) this.getHInstance()).toString()");
        return ((com.huawei.hms.common.size.Size) this.getHInstance()).toString();
    }
    
    public static org.xms.g.common.images.Size dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.images.Size) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.common.size.Size;
    }
}