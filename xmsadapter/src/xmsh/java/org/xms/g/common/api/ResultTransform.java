package org.xms.g.common.api;




public abstract class ResultTransform<XR extends org.xms.g.common.api.Result, XS extends org.xms.g.common.api.Result> extends org.xms.g.utils.XObject {
    private boolean wrapper = true;
    
    
    
    public ResultTransform(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    public ResultTransform() {
        super(((org.xms.g.utils.XBox) null));
        this.setHInstance(new HImpl());
        wrapper = false;
    }
    
    public final org.xms.g.common.api.PendingResult<XS> createFailedResult(org.xms.g.common.api.Status param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance()).newFailedPendingResult(((com.huawei.hms.support.api.client.Status) ((param0) == null ? null : (param0.getHInstance()))))");
        com.huawei.hms.support.api.client.PendingResult hReturn = ((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance()).newFailedPendingResult(((com.huawei.hms.support.api.client.Status) ((param0) == null ? null : (param0.getHInstance()))));
        return ((hReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public org.xms.g.common.api.Status onFailure(org.xms.g.common.api.Status param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance()).onFailed(((com.huawei.hms.support.api.client.Status) ((param0) == null ? null : (param0.getHInstance()))))");
            com.huawei.hms.support.api.client.Status hReturn = ((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance()).onFailed(((com.huawei.hms.support.api.client.Status) ((param0) == null ? null : (param0.getHInstance()))));
            return ((hReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((HImpl) ((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance())).onFailedCallSuper(((com.huawei.hms.support.api.client.Status) ((param0) == null ? null : (param0.getHInstance()))))");
            com.huawei.hms.support.api.client.Status hReturn = ((HImpl) ((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance())).onFailedCallSuper(((com.huawei.hms.support.api.client.Status) ((param0) == null ? null : (param0.getHInstance()))));
            return ((hReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn))));
        }
    }
    
    public abstract org.xms.g.common.api.PendingResult<XS> onSuccess(XR param0);
    
    public static org.xms.g.common.api.ResultTransform dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.ResultTransform) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.support.api.client.ResultConvert;
    }
    
    private class HImpl<R extends com.huawei.hms.support.api.client.Result, S extends com.huawei.hms.support.api.client.Result> extends com.huawei.hms.support.api.client.ResultConvert<R, S> {
        
        
        
        public com.huawei.hms.support.api.client.Status onFailed(com.huawei.hms.support.api.client.Status param0) {
            org.xms.g.common.api.Status xResult = org.xms.g.common.api.ResultTransform.this.onFailure(((param0) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(param0)))));
            return ((com.huawei.hms.support.api.client.Status) ((xResult) == null ? null : (xResult.getHInstance())));
        }
        
        public com.huawei.hms.support.api.client.Status onFailedCallSuper(com.huawei.hms.support.api.client.Status param0) {
            return super.onFailed(param0);
        }
        
        public com.huawei.hms.support.api.client.PendingResult onSuccess(com.huawei.hms.support.api.client.Result param0) {
            throw new java.lang.RuntimeException("Stub");
        }
        
        public HImpl() {
            super();
        }
    }
    
    public static class XImpl<XR extends org.xms.g.common.api.Result, XS extends org.xms.g.common.api.Result> extends org.xms.g.common.api.ResultTransform<XR, XS> {
        
        
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.common.api.PendingResult<XS> onSuccess(XR param0) {
            
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance()).onSuccess(((com.huawei.hms.support.api.client.Result) hObj0))");
            java.lang.Object hObj0 = org.xms.g.utils.Utils.getInstanceInInterface(param0, true);
            com.huawei.hms.support.api.client.PendingResult hReturn = null;
            hReturn = ((com.huawei.hms.support.api.client.ResultConvert) this.getHInstance()).onSuccess(((com.huawei.hms.support.api.client.Result) hObj0));
            if (hReturn == null)
                return null;
            return new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(hReturn));
        }
    }
}