package org.xms.g.common.api;

public final class Status extends org.xms.g.utils.XObject implements org.xms.g.common.api.Result, android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {

        public org.xms.g.common.api.Status createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.support.api.client.Status hReturn = com.huawei.hms.support.api.client.Status.CREATOR.createFromParcel(param0);
            return new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn));
        }

        public org.xms.g.common.api.Status[] newArray(int param0) {
            return new org.xms.g.common.api.Status[param0];
        }
    };

    public Status(org.xms.g.utils.XBox param0) {
        super(param0);
    }

    public Status(int param0) {
        super((org.xms.g.utils.XBox) null);
        this.setHInstance(new com.huawei.hms.support.api.client.Status(param0));
    }

    public Status(int param0, java.lang.String param1) {
        super((org.xms.g.utils.XBox) null);
        this.setHInstance(new com.huawei.hms.support.api.client.Status(param0, param1));
    }

    public Status(int param0, java.lang.String param1, android.app.PendingIntent param2) {
        super((org.xms.g.utils.XBox) null);
        this.setHInstance(new com.huawei.hms.support.api.client.Status(param0, param1, param2));
    }

    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).equals(param0)");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).equals(param0);
    }

    public final android.app.PendingIntent getResolution() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).getResolution()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).getResolution();
    }

    public final org.xms.g.common.api.Status getStatus() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).getStatus()");
        com.huawei.hms.support.api.client.Status hReturn = ((com.huawei.hms.support.api.client.Status) this.getHInstance()).getStatus();
        return ((hReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn))));
    }

    public final int getStatusCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).getStatusCode()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).getStatusCode();
    }

    public final java.lang.String getStatusMessage() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).getStatusMessage()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).getStatusMessage();
    }

    public final boolean hasResolution() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).hasResolution()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).hasResolution();
    }

    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).hashCode()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).hashCode();
    }

    public final boolean isCanceled() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).isCanceled()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).isCanceled();
    }

    public final boolean isInterrupted() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).isInterrupted()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).isInterrupted();
    }

    public final boolean isSuccess() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).isSuccess()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).isSuccess();
    }

    public void startResolutionForResult(android.app.Activity param0, int param1) throws android.content.IntentSender.SendIntentException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).startResolutionForResult(param0, param1)");
        ((com.huawei.hms.support.api.client.Status) this.getHInstance()).startResolutionForResult(param0, param1);
    }

    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).toString()");
        return ((com.huawei.hms.support.api.client.Status) this.getHInstance()).toString();
    }

    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.support.api.client.Status) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.support.api.client.Status) this.getHInstance()).writeToParcel(param0, param1);
    }

    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public static org.xms.g.common.api.Status dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.api.Status) {
            return ((org.xms.g.common.api.Status) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.support.api.client.Status hReturn = ((com.huawei.hms.support.api.client.Status) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn));
        }
        if (param0 instanceof com.huawei.hms.support.api.client.Status) {
            return new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(param0));
        }
        return ((org.xms.g.common.api.Status) param0);
    }

    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.support.api.client.Status;
    }
}