package org.xms.g.common.api;

public class BooleanResult extends org.xms.g.utils.XObject {
    
    public BooleanResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).equals(param0)");
        return ((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).equals(param0);
    }
    
    public org.xms.g.common.api.Status getStatus() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).getStatus()");
        com.huawei.hms.support.api.client.Status hReturn = ((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).getStatus();
        return ((hReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public boolean getValue() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).getValue()");
        return ((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).getValue();
    }
    
    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).hashCode()");
        return ((com.huawei.hms.common.api.BooleanResult) this.getHInstance()).hashCode();
    }
    
    public static org.xms.g.common.api.BooleanResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.BooleanResult) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.common.api.BooleanResult;
    }
}