package org.xms.g.common.data;

public interface DataBuffer<XT> extends org.xms.g.utils.XInterface, org.xms.g.common.api.Releasable, java.io.Closeable, java.lang.Iterable<XT> {
    
    public void close();
    
    public XT get(int param0);
    
    public int getCount();
    
    public boolean isClosed();
    
    public java.util.Iterator<XT> iterator();
    
    public void release();
    
    public java.util.Iterator<XT> singleRefIterator();
    
    default java.lang.Object getZInstanceDataBuffer() {
        return getHInstanceDataBuffer();
    }
    
    default <T> com.huawei.hms.common.data.DataBuffer<T> getHInstanceDataBuffer() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.huawei.hms.common.data.DataBuffer<T>) ((org.xms.g.utils.XGettable) this).getHInstance());
        }
        throw new java.lang.RuntimeException("Not for inheriting");
    }
    
    public static org.xms.g.common.data.DataBuffer dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.data.DataBuffer) {
            return ((org.xms.g.common.data.DataBuffer) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.common.data.DataBuffer hReturn = ((com.huawei.hms.common.data.DataBuffer) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.common.data.DataBuffer.XImpl(new org.xms.g.utils.XBox(hReturn));
        }
        return ((org.xms.g.common.data.DataBuffer) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.common.data.DataBuffer;
        }
        return param0 instanceof org.xms.g.common.data.DataBuffer;
    }
    
    public static class XImpl<XT> extends org.xms.g.utils.XObject implements org.xms.g.common.data.DataBuffer<XT> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public void close() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).close()");
            ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).close();
        }
        
        public XT get(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).get(param0)");
            java.lang.Object hmsObj = ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).get(param0);
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
        }
        
        public int getCount() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).getCount()");
            return ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).getCount();
        }
        
        public boolean isClosed() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).isClosed()");
            return ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).isClosed();
        }
        
        public java.util.Iterator<XT> iterator() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).iterator()");
            java.util.Iterator hReturn = ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).iterator();
            return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(hReturn, new org.xms.g.utils.Function<Object, XT>() {
                
                public XT apply(java.lang.Object param0) {
                    return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
                }
            }));
        }
        
        public void release() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).release()");
            ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).release();
        }
        
        public java.util.Iterator<XT> singleRefIterator() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).singleRefIterator()");
            java.util.Iterator hReturn = ((com.huawei.hms.common.data.DataBuffer) this.getHInstance()).singleRefIterator();
            return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(hReturn, new org.xms.g.utils.Function<Object, XT>() {
                
                public XT apply(java.lang.Object param0) {
                    return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
                }
            }));
        }
    }
}