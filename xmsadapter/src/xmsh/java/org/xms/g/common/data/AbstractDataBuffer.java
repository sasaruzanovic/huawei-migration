package org.xms.g.common.data;

public abstract class AbstractDataBuffer<XT> extends org.xms.g.utils.XObject implements org.xms.g.common.data.DataBuffer<XT> {
    
    public AbstractDataBuffer(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public void close() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).close()");
        ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).close();
    }
    
    public abstract XT get(int param0);
    
    public int getCount() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).getCount()");
        return ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).getCount();
    }
    
    public boolean isClosed() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).isClosed()");
        return ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).isClosed();
    }
    
    public java.util.Iterator<XT> iterator() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).iterator()");
        java.util.Iterator hReturn = ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).iterator();
        return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(hReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
            }
        }));
    }
    
    public void release() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).release()");
        ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).release();
    }
    
    public java.util.Iterator<XT> singleRefIterator() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).singleRefIterator()");
        java.util.Iterator hReturn = ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).singleRefIterator();
        return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(hReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(param0));
            }
        }));
    }
    
    public static org.xms.g.common.data.AbstractDataBuffer dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.data.AbstractDataBuffer) {
            return ((org.xms.g.common.data.AbstractDataBuffer) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.huawei.hms.common.data.AbstractDataBuffer hReturn = ((com.huawei.hms.common.data.AbstractDataBuffer) ((org.xms.g.utils.XGettable) param0).getHInstance());
            return new org.xms.g.common.data.AbstractDataBuffer.XImpl(new org.xms.g.utils.XBox(hReturn));
        }
        return ((org.xms.g.common.data.AbstractDataBuffer) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.common.data.AbstractDataBuffer;
    }
    
    public static class XImpl<XT> extends org.xms.g.common.data.AbstractDataBuffer<XT> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public XT get(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).get(param0)");
            java.lang.Object hmsObj = ((com.huawei.hms.common.data.AbstractDataBuffer) this.getHInstance()).get(param0);
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
        }
    }
}