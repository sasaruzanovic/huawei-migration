package org.xms.g.tasks;




public class TaskCompletionSource<XTResult> extends org.xms.g.utils.XObject {
    
    
    
    public TaskCompletionSource(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public org.xms.g.tasks.Task<XTResult> getTask() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).getTask()");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).getTask();
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public void setException(java.lang.Exception param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setException(param0)");
        ((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setException(param0);
    }
    
    public void setResult(XTResult param0) {
        java.lang.Object hObj0 = ((java.lang.Object) org.xms.g.utils.Utils.getInstanceInInterface(param0, true));
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setResult(hObj0)");
        ((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setResult(hObj0);
    }
    
    public boolean trySetException(java.lang.Exception param0) {
        
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setException(param0)");
        ((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setException(param0);
        return true;
    }
    
    public boolean trySetResult(XTResult param0) {
        
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setResult(param0)");
        ((com.huawei.hmf.tasks.TaskCompletionSource) this.getHInstance()).setResult(param0);
        return true;
    }
    
    public static org.xms.g.tasks.TaskCompletionSource dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.tasks.TaskCompletionSource) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hmf.tasks.TaskCompletionSource;
    }
}