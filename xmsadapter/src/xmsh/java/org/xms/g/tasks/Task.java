package org.xms.g.tasks;

import org.xms.g.utils.Utils;

public abstract class Task<XTResult> extends org.xms.g.utils.XObject {
    
    public Task(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public org.xms.g.tasks.Task<XTResult> addOnCanceledListener(org.xms.g.tasks.OnCanceledListener param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCanceledListener(((param0) == null ? null : (param0.getHInstanceOnCanceledListener())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCanceledListener(((param0) == null ? null : (param0.getHInstanceOnCanceledListener())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public org.xms.g.tasks.Task<XTResult> addOnCanceledListener(java.util.concurrent.Executor param0, org.xms.g.tasks.OnCanceledListener param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCanceledListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCanceledListener())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCanceledListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCanceledListener())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public org.xms.g.tasks.Task<XTResult> addOnCanceledListener(android.app.Activity param0, org.xms.g.tasks.OnCanceledListener param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCanceledListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCanceledListener())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCanceledListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCanceledListener())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public org.xms.g.tasks.Task<XTResult> addOnCompleteListener(org.xms.g.tasks.OnCompleteListener<XTResult> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCompleteListener(((param0) == null ? null : (param0.getHInstanceOnCompleteListener())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCompleteListener(((param0) == null ? null : (param0.getHInstanceOnCompleteListener())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public org.xms.g.tasks.Task<XTResult> addOnCompleteListener(android.app.Activity param0, org.xms.g.tasks.OnCompleteListener<XTResult> param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCompleteListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCompleteListener())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCompleteListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCompleteListener())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public org.xms.g.tasks.Task<XTResult> addOnCompleteListener(java.util.concurrent.Executor param0, org.xms.g.tasks.OnCompleteListener<XTResult> param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCompleteListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCompleteListener())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnCompleteListener(param0, ((param1) == null ? null : (param1.getHInstanceOnCompleteListener())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public abstract org.xms.g.tasks.Task<XTResult> addOnFailureListener(android.app.Activity param0, org.xms.g.tasks.OnFailureListener param1);
    
    public abstract org.xms.g.tasks.Task<XTResult> addOnFailureListener(org.xms.g.tasks.OnFailureListener param0);
    
    public abstract org.xms.g.tasks.Task<XTResult> addOnFailureListener(java.util.concurrent.Executor param0, org.xms.g.tasks.OnFailureListener param1);
    
    public abstract org.xms.g.tasks.Task<XTResult> addOnSuccessListener(java.util.concurrent.Executor param0, org.xms.g.tasks.OnSuccessListener<? super XTResult> param1);
    
    public abstract org.xms.g.tasks.Task<XTResult> addOnSuccessListener(org.xms.g.tasks.OnSuccessListener<? super XTResult> param0);
    
    public abstract org.xms.g.tasks.Task<XTResult> addOnSuccessListener(android.app.Activity param0, org.xms.g.tasks.OnSuccessListener<? super XTResult> param1);
    
    public <XTContinuationResult> org.xms.g.tasks.Task<XTContinuationResult> continueWith(org.xms.g.tasks.Continuation<XTResult, XTContinuationResult> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWith(((param0) == null ? null : (param0.getHInstanceContinuation())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWith(((param0) == null ? null : (param0.getHInstanceContinuation())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public <XTContinuationResult> org.xms.g.tasks.Task<XTContinuationResult> continueWith(java.util.concurrent.Executor param0, org.xms.g.tasks.Continuation<XTResult, XTContinuationResult> param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWith(param0, ((param1) == null ? null : (param1.getHInstanceContinuation())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWith(param0, ((param1) == null ? null : (param1.getHInstanceContinuation())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public <XTContinuationResult> org.xms.g.tasks.Task<XTContinuationResult> continueWithTask(org.xms.g.tasks.Continuation<XTResult, org.xms.g.tasks.Task<XTContinuationResult>> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWithTask(((param0) == null ? null : (param0.getHInstanceContinuation())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWithTask(((param0) == null ? null : (param0.getHInstanceContinuation())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public <XTContinuationResult> org.xms.g.tasks.Task<XTContinuationResult> continueWithTask(java.util.concurrent.Executor param0, org.xms.g.tasks.Continuation<XTResult, org.xms.g.tasks.Task<XTContinuationResult>> param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWithTask(param0, ((param1) == null ? null : (param1.getHInstanceContinuation())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).continueWithTask(param0, ((param1) == null ? null : (param1.getHInstanceContinuation())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public abstract java.lang.Exception getException();
    
    public abstract XTResult getResult();
    
    public abstract <XX extends java.lang.Throwable> XTResult getResult(java.lang.Class<XX> param0) throws XX;
    
    public abstract boolean isCanceled();
    
    public abstract boolean isComplete();
    
    public abstract boolean isSuccessful();
    
    public <XTContinuationResult> org.xms.g.tasks.Task<XTContinuationResult> onSuccessTask(java.util.concurrent.Executor param0, org.xms.g.tasks.SuccessContinuation<XTResult, XTContinuationResult> param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).onSuccessTask(param0, ((param1) == null ? null : (param1.getHInstanceSuccessContinuation())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).onSuccessTask(param0, ((param1) == null ? null : (param1.getHInstanceSuccessContinuation())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public <XTContinuationResult> org.xms.g.tasks.Task<XTContinuationResult> onSuccessTask(org.xms.g.tasks.SuccessContinuation<XTResult, XTContinuationResult> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).onSuccessTask(((param0) == null ? null : (param0.getHInstanceSuccessContinuation())))");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).onSuccessTask(((param0) == null ? null : (param0.getHInstanceSuccessContinuation())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.tasks.Task dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.tasks.Task) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hmf.tasks.Task;
    }
    
    public static class XImpl<XTResult> extends org.xms.g.tasks.Task<XTResult> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.tasks.Task<XTResult> addOnFailureListener(android.app.Activity param0, org.xms.g.tasks.OnFailureListener param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnFailureListener(param0, ((param1) == null ? null : (param1.getHInstanceOnFailureListener())))");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnFailureListener(param0, ((param1) == null ? null : (param1.getHInstanceOnFailureListener())));
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.tasks.Task<XTResult> addOnFailureListener(org.xms.g.tasks.OnFailureListener param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnFailureListener(((param0) == null ? null : (param0.getHInstanceOnFailureListener())))");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnFailureListener(((param0) == null ? null : (param0.getHInstanceOnFailureListener())));
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.tasks.Task<XTResult> addOnFailureListener(java.util.concurrent.Executor param0, org.xms.g.tasks.OnFailureListener param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnFailureListener(param0, ((param1) == null ? null : (param1.getHInstanceOnFailureListener())))");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnFailureListener(param0, ((param1) == null ? null : (param1.getHInstanceOnFailureListener())));
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.tasks.Task<XTResult> addOnSuccessListener(java.util.concurrent.Executor param0, org.xms.g.tasks.OnSuccessListener<? super XTResult> param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnSuccessListener(param0, ((param1) == null ? null : (param1.getHInstanceOnSuccessListener())))");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnSuccessListener(param0, ((param1) == null ? null : (param1.getHInstanceOnSuccessListener())));
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.tasks.Task<XTResult> addOnSuccessListener(org.xms.g.tasks.OnSuccessListener<? super XTResult> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnSuccessListener(((param0) == null ? null : (param0.getHInstanceOnSuccessListener())))");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnSuccessListener(((param0) == null ? null : (param0.getHInstanceOnSuccessListener())));
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.g.tasks.Task<XTResult> addOnSuccessListener(android.app.Activity param0, org.xms.g.tasks.OnSuccessListener<? super XTResult> param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnSuccessListener(param0, ((param1) == null ? null : (param1.getHInstanceOnSuccessListener())))");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hmf.tasks.Task) this.getHInstance()).addOnSuccessListener(param0, ((param1) == null ? null : (param1.getHInstanceOnSuccessListener())));
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public java.lang.Exception getException() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).getException()");
            return (java.lang.Exception) Utils.getXmsObjectWithHmsObject(((com.huawei.hmf.tasks.Task) this.getHInstance()).getException());
        }
        
        public XTResult getResult() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).getResult()");
            java.lang.Object hmsObj = ((com.huawei.hmf.tasks.Task) this.getHInstance()).getResult();
            return ((XTResult) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
        }
        
        public <XX extends java.lang.Throwable> XTResult getResult(java.lang.Class<XX> param0) throws XX {
            try {
                java.lang.Class hObj0 = ((java.lang.Class) org.xms.g.utils.Utils.getHmsClassWithXmsClass(param0));
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).getResultThrowException(hObj0)");
                java.lang.Object hmsObj = ((com.huawei.hmf.tasks.Task) this.getHInstance()).getResultThrowException(hObj0);
                return ((XTResult) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
            }
            catch (java.lang.Throwable e) {
                throw ((XX) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(e));
            }
        }
        
        public boolean isCanceled() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).isCanceled()");
            return ((com.huawei.hmf.tasks.Task) this.getHInstance()).isCanceled();
        }
        
        public boolean isComplete() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).isComplete()");
            return ((com.huawei.hmf.tasks.Task) this.getHInstance()).isComplete();
        }
        
        public boolean isSuccessful() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.Task) this.getHInstance()).isSuccessful()");
            return ((com.huawei.hmf.tasks.Task) this.getHInstance()).isSuccessful();
        }
    }
}