package org.xms.g.tasks;




public final class Tasks extends org.xms.g.utils.XObject {
    
    
    
    public Tasks(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static <XTResult> XTResult await(org.xms.g.tasks.Task<XTResult> param0, long param1, java.util.concurrent.TimeUnit param2) throws java.util.concurrent.ExecutionException, java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.await(((com.huawei.hmf.tasks.Task) ((param0) == null ? null : (param0.getHInstance()))), param1, param2)");
        java.lang.Object hmsObj = com.huawei.hmf.tasks.Tasks.await(((com.huawei.hmf.tasks.Task) ((param0) == null ? null : (param0.getHInstance()))), param1, param2);
        return ((XTResult) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
    }
    
    public static <XTResult> XTResult await(org.xms.g.tasks.Task<XTResult> param0) throws java.util.concurrent.ExecutionException, java.lang.InterruptedException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.await(((com.huawei.hmf.tasks.Task) ((param0) == null ? null : (param0.getHInstance()))))");
        java.lang.Object hmsObj = com.huawei.hmf.tasks.Tasks.await(((com.huawei.hmf.tasks.Task) ((param0) == null ? null : (param0.getHInstance()))));
        return ((XTResult) org.xms.g.utils.Utils.getXmsObjectWithHmsObject(hmsObj));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<XTResult> call(java.util.concurrent.Callable<XTResult> param0) {
        
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.callInBackground(com.huawei.hmf.tasks.TaskExecutors.uiThread(), param0)");
        com.huawei.hmf.tasks.Task hReturn = null;
        hReturn = com.huawei.hmf.tasks.Tasks.callInBackground(com.huawei.hmf.tasks.TaskExecutors.uiThread(), param0);
        if (hReturn == null) {
            return null;
        }
        return new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<XTResult> call(java.util.concurrent.Executor param0, java.util.concurrent.Callable<XTResult> param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.callInBackground(param0, param1)");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.callInBackground(param0, param1);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<XTResult> forCanceled() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.fromCanceled()");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.fromCanceled();
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<XTResult> forException(java.lang.Exception param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.fromException(param0)");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.fromException(param0);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<XTResult> forResult(XTResult param0) {
        XTResult hObj0 = ((XTResult) org.xms.g.utils.Utils.getInstanceInInterface(param0, true));
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.fromResult(hObj0)");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.fromResult(hObj0);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.tasks.Task<java.lang.Void> whenAll(org.xms.g.tasks.Task<?>... param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.join(((com.huawei.hmf.tasks.Task[]) org.xms.g.utils.Utils.genericArrayCopy(param0, com.huawei.hmf.tasks.Task.class, x -> (com.huawei.hmf.tasks.Task)x.getHInstance())))");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.join(((com.huawei.hmf.tasks.Task[]) org.xms.g.utils.Utils.genericArrayCopy(param0, com.huawei.hmf.tasks.Task.class, x -> (com.huawei.hmf.tasks.Task)x.getHInstance())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.tasks.Task<java.lang.Void> whenAll(java.util.Collection<? extends org.xms.g.tasks.Task<?>> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.join(org.xms.g.utils.Utils.mapCollection2GH(param0, true))");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.join(org.xms.g.utils.Utils.mapCollection2GH(param0, true));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.tasks.Task<java.util.List<org.xms.g.tasks.Task<?>>> whenAllComplete(org.xms.g.tasks.Task<?>... param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.allOf(((com.huawei.hmf.tasks.Task[]) org.xms.g.utils.Utils.genericArrayCopy(param0, com.huawei.hmf.tasks.Task.class, x -> (com.huawei.hmf.tasks.Task)x.getHInstance())))");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.allOf(((com.huawei.hmf.tasks.Task[]) org.xms.g.utils.Utils.genericArrayCopy(param0, com.huawei.hmf.tasks.Task.class, x -> (com.huawei.hmf.tasks.Task)x.getHInstance())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.tasks.Task<java.util.List<org.xms.g.tasks.Task<?>>> whenAllComplete(java.util.Collection<? extends org.xms.g.tasks.Task<?>> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.allOf(org.xms.g.utils.Utils.mapCollection2GH(param0, true))");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.allOf(org.xms.g.utils.Utils.mapCollection2GH(param0, true));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<java.util.List<XTResult>> whenAllSuccess(org.xms.g.tasks.Task<?>... param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.successOf(((com.huawei.hmf.tasks.Task[]) org.xms.g.utils.Utils.genericArrayCopy(param0, com.huawei.hmf.tasks.Task.class, x -> (com.huawei.hmf.tasks.Task)x.getHInstance())))");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.successOf(((com.huawei.hmf.tasks.Task[]) org.xms.g.utils.Utils.genericArrayCopy(param0, com.huawei.hmf.tasks.Task.class, x -> (com.huawei.hmf.tasks.Task)x.getHInstance())));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static <XTResult> org.xms.g.tasks.Task<java.util.List<XTResult>> whenAllSuccess(java.util.Collection param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.Tasks.successOf(org.xms.g.utils.Utils.mapCollection2GH(param0, true))");
        com.huawei.hmf.tasks.Task hReturn = com.huawei.hmf.tasks.Tasks.successOf(org.xms.g.utils.Utils.mapCollection2GH(param0, true));
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.g.tasks.Tasks dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.tasks.Tasks) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hmf.tasks.Tasks;
    }
}