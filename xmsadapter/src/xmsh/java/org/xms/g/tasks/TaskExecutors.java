package org.xms.g.tasks;




public final class TaskExecutors extends org.xms.g.utils.XObject {
    
    
    
    public TaskExecutors(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static java.util.concurrent.Executor getMAIN_THREAD() {
        
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hmf.tasks.TaskExecutors.uiThread()");
        return com.huawei.hmf.tasks.TaskExecutors.uiThread();
    }
    
    public static org.xms.g.tasks.TaskExecutors dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.tasks.TaskExecutors) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        
        throw new RuntimeException("HMS does not support this API.");
    }
}