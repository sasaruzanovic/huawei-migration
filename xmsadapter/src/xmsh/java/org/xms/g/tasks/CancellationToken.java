package org.xms.g.tasks;




public abstract class CancellationToken extends org.xms.g.utils.XObject {
    
    
    
    public CancellationToken(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public abstract boolean isCancellationRequested();
    
    public abstract org.xms.g.tasks.CancellationToken onCanceledRequested(org.xms.g.tasks.OnTokenCanceledListener param0);
    
    public static org.xms.g.tasks.CancellationToken dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.tasks.CancellationToken) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hmf.tasks.CancellationToken;
    }
    
    public static class XImpl extends org.xms.g.tasks.CancellationToken {
        
        
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public boolean isCancellationRequested() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.CancellationToken) this.getHInstance()).isCancellationRequested()");
            return ((com.huawei.hmf.tasks.CancellationToken) this.getHInstance()).isCancellationRequested();
        }
        
        public org.xms.g.tasks.CancellationToken onCanceledRequested(org.xms.g.tasks.OnTokenCanceledListener param0) {
            
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hmf.tasks.CancellationToken) this.getHInstance()).register(action)");
            com.huawei.hmf.tasks.CancellationToken hReturn = null;
            Runnable action = new Runnable() {
                @Override
                public void run() {
                    param0.onCanceled();
                }
            };
            hReturn = ((com.huawei.hmf.tasks.CancellationToken) this.getHInstance()).register(action);
            if (hReturn == null) {
                return null;
            }
            return new org.xms.g.tasks.CancellationToken.XImpl(new org.xms.g.utils.XBox(hReturn));
        }
    }
}