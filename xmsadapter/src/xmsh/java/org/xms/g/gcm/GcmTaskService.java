package org.xms.g.gcm;

public abstract class GcmTaskService extends android.app.Service implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    
    public GcmTaskService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
    }
    
    public static java.lang.String getSERVICE_PERMISSION() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.gcm.GcmTaskService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class XImpl extends org.xms.g.gcm.GcmTaskService {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public android.os.IBinder onBind(android.content.Intent param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}