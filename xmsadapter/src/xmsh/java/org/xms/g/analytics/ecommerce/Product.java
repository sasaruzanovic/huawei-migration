package org.xms.g.analytics.ecommerce;

public class Product extends org.xms.g.utils.XObject {
    
    public Product(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public Product() {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public org.xms.g.analytics.ecommerce.Product setBrand(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setCategory(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setCouponCode(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setCustomDimension(int param0, java.lang.String param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setCustomMetric(int param0, int param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setId(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setName(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setPosition(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setPrice(double param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setQuantity(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.Product setVariant(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String toString() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.ecommerce.Product dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}