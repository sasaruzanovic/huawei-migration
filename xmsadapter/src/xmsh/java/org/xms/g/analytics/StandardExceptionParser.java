package org.xms.g.analytics;

public class StandardExceptionParser extends org.xms.g.utils.XObject implements org.xms.g.analytics.ExceptionParser {
    
    public StandardExceptionParser(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public StandardExceptionParser(android.content.Context param0, java.util.Collection<java.lang.String> param1) {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public java.lang.String getDescription(java.lang.String param0, java.lang.Throwable param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setIncludedPackages(android.content.Context param0, java.util.Collection<java.lang.String> param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    protected java.lang.StackTraceElement getBestStackTraceElement(java.lang.Throwable param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    protected java.lang.Throwable getCause(java.lang.Throwable param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    protected java.lang.String getDescription(java.lang.Throwable param0, java.lang.StackTraceElement param1, java.lang.String param2) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.StandardExceptionParser dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}