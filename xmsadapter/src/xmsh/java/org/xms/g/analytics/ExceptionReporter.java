package org.xms.g.analytics;

public class ExceptionReporter extends org.xms.g.utils.XObject implements java.lang.Thread.UncaughtExceptionHandler {
    
    public ExceptionReporter(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public ExceptionReporter(org.xms.g.analytics.Tracker param0, java.lang.Thread.UncaughtExceptionHandler param1, android.content.Context param2) {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public org.xms.g.analytics.ExceptionParser getExceptionParser() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setExceptionParser(org.xms.g.analytics.ExceptionParser param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void uncaughtException(java.lang.Thread param0, java.lang.Throwable param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.ExceptionReporter dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}