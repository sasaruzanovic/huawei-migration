package org.xms.g.analytics;

public class AnalyticsJobService extends android.app.job.JobService implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    
    public AnalyticsJobService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
    }
    
    public final boolean callServiceStopSelfResult(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean onStartJob(android.app.job.JobParameters param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean onStopJob(android.app.job.JobParameters param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.analytics.AnalyticsJobService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}