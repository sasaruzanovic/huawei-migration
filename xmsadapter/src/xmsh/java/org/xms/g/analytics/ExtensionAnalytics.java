package org.xms.g.analytics;

public final class ExtensionAnalytics extends org.xms.g.utils.XObject {
    
    public ExtensionAnalytics(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public final void dispatchLocalHits() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void enableAutoActivityReports(android.app.Application param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean getAppOptOut() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.ExtensionAnalytics getInstance(android.content.Context param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final org.xms.g.analytics.Logger getLogger() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean isDryRunEnabled() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final org.xms.g.analytics.Tracker newTracker(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final org.xms.g.analytics.Tracker newTracker(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void reportActivityStart(android.app.Activity param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void reportActivityStop(android.app.Activity param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setAppOptOut(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setDryRun(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setLocalDispatchPeriod(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setLogger(org.xms.g.analytics.Logger param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.ExtensionAnalytics dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}