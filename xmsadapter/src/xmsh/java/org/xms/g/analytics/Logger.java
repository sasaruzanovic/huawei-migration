package org.xms.g.analytics;

public interface Logger extends org.xms.g.utils.XInterface {
    
    public void error(java.lang.Exception param0);
    
    public void error(java.lang.String param0);
    
    public int getLogLevel();
    
    public void info(java.lang.String param0);
    
    public void setLogLevel(int param0);
    
    public void verbose(java.lang.String param0);
    
    public void warn(java.lang.String param0);
    
    default java.lang.Object getZInstanceLogger() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    default java.lang.Object getHInstanceLogger() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.Logger dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.analytics.Logger {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public void error(java.lang.Exception param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public void error(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public int getLogLevel() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public void info(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public void setLogLevel(int param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public void verbose(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public void warn(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class LogLevel extends org.xms.g.utils.XObject {
        
        public LogLevel(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public LogLevel() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static int getERROR() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static int getINFO() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static int getVERBOSE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static int getWARNING() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.Logger.LogLevel dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}