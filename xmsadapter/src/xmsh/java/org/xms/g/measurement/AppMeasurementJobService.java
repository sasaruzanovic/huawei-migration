package org.xms.g.measurement;

public final class AppMeasurementJobService extends android.app.job.JobService implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    
    public AppMeasurementJobService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
    }
    
    public AppMeasurementJobService() {
    }
    
    public final void onCreate() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void onDestroy() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void onRebind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean onStartJob(android.app.job.JobParameters param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean onStopJob(android.app.job.JobParameters param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean onUnbind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.measurement.AppMeasurementJobService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}