package org.xms.g.measurement;

public final class AppMeasurementReceiver extends androidx.legacy.content.WakefulBroadcastReceiver implements org.xms.g.utils.XGettable {
    public java.lang.Object hInstance;
    
    public AppMeasurementReceiver(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setHInstance(param0.getHInstance());
    }
    
    public AppMeasurementReceiver() {
    }
    
    public final android.content.BroadcastReceiver.PendingResult doGoAsync() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void doStartService(android.content.Context param0, android.content.Intent param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void onReceive(android.content.Context param0, android.content.Intent param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.measurement.AppMeasurementReceiver dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}