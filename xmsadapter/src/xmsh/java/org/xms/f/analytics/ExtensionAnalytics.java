package org.xms.f.analytics;




public final class ExtensionAnalytics extends org.xms.g.utils.XObject {
    
    
    
    public ExtensionAnalytics(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public org.xms.g.tasks.Task<java.lang.String> getAppInstanceId() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).getAAID()");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).getAAID();
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public static org.xms.f.analytics.ExtensionAnalytics getInstance(android.content.Context param0) {
        
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.HiAnalytics.getInstance(param0)");
        com.huawei.hms.analytics.HiAnalyticsInstance hReturn = com.huawei.hms.analytics.HiAnalytics.getInstance(param0);
        return ((hReturn) == null ? null : (new org.xms.f.analytics.ExtensionAnalytics(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public final void logEvent(java.lang.String param0, android.os.Bundle param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).onEvent(param0, param1)");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).onEvent(param0, param1);
    }
    
    public final void resetAnalyticsData() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).clearCachedData()");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).clearCachedData();
    }
    
    public final void setAnalyticsCollectionEnabled(boolean param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setAnalyticsEnabled(param0)");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setAnalyticsEnabled(param0);
    }
    
    public final void setCurrentScreen(android.app.Activity param0, java.lang.String param1, java.lang.String param2) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setMinimumSessionDuration(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setMinActivitySessions(param0)");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setMinActivitySessions(param0);
    }
    
    public final void setSessionTimeoutDuration(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setSessionDuration(param0)");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setSessionDuration(param0);
    }
    
    public final void setUserId(java.lang.String param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserId(param0)");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserId(param0);
    }
    
    public final void setUserProperty(java.lang.String param0, java.lang.String param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserProfile(param0, param1)");
        ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserProfile(param0, param1);
    }
    
    public static org.xms.f.analytics.ExtensionAnalytics dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.analytics.ExtensionAnalytics) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.HiAnalyticsInstance;
    }
    
    public static class Event extends org.xms.g.utils.XObject {
        
        public Event(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected Event() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getADD_PAYMENT_INFO() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.CREATEPAYMENTINFO");
            return com.huawei.hms.analytics.type.HAEventType.CREATEPAYMENTINFO;
        }
        
        public static java.lang.String getADD_TO_CART() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2CART");
            return com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2CART;
        }
        
        public static java.lang.String getADD_TO_WISHLIST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2WISHLIST");
            return com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2WISHLIST;
        }
        
        public static java.lang.String getAPP_OPEN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTAPP");
            return com.huawei.hms.analytics.type.HAEventType.STARTAPP;
        }
        
        public static java.lang.String getBEGIN_CHECKOUT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTCHECKOUT");
            return com.huawei.hms.analytics.type.HAEventType.STARTCHECKOUT;
        }
        
        public static java.lang.String getCAMPAIGN_DETAILS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWCAMPAIGN");
            return com.huawei.hms.analytics.type.HAEventType.VIEWCAMPAIGN;
        }
        
        public static java.lang.String getCHECKOUT_PROGRESS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWCHECKOUTSTEP");
            return com.huawei.hms.analytics.type.HAEventType.VIEWCHECKOUTSTEP;
        }
        
        public static java.lang.String getEARN_VIRTUAL_CURRENCY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.WINVIRTUALCOIN");
            return com.huawei.hms.analytics.type.HAEventType.WINVIRTUALCOIN;
        }
        
        public static java.lang.String getECOMMERCE_PURCHASE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.COMPLETEPURCHASE");
            return com.huawei.hms.analytics.type.HAEventType.COMPLETEPURCHASE;
        }
        
        public static java.lang.String getGENERATE_LEAD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.OBTAINLEADS");
            return com.huawei.hms.analytics.type.HAEventType.OBTAINLEADS;
        }
        
        public static java.lang.String getJOIN_GROUP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.JOINUSERGROUP");
            return com.huawei.hms.analytics.type.HAEventType.JOINUSERGROUP;
        }
        
        public static java.lang.String getLEVEL_END() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.COMPLETELEVEL");
            return com.huawei.hms.analytics.type.HAEventType.COMPLETELEVEL;
        }
        
        public static java.lang.String getLEVEL_START() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTLEVEL");
            return com.huawei.hms.analytics.type.HAEventType.STARTLEVEL;
        }
        
        public static java.lang.String getLEVEL_UP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.UPGRADELEVEL");
            return com.huawei.hms.analytics.type.HAEventType.UPGRADELEVEL;
        }
        
        public static java.lang.String getLOGIN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SIGNIN");
            return com.huawei.hms.analytics.type.HAEventType.SIGNIN;
        }
        
        public static java.lang.String getPOST_SCORE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SUBMITSCORE");
            return com.huawei.hms.analytics.type.HAEventType.SUBMITSCORE;
        }
        
        public static java.lang.String getPRESENT_OFFER() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.CREATEORDER");
            return com.huawei.hms.analytics.type.HAEventType.CREATEORDER;
        }
        
        public static java.lang.String getPURCHASE_REFUND() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.REFUNDORDER");
            return com.huawei.hms.analytics.type.HAEventType.REFUNDORDER;
        }
        
        public static java.lang.String getREMOVE_FROM_CART() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.DELPRODUCTFROMCART");
            return com.huawei.hms.analytics.type.HAEventType.DELPRODUCTFROMCART;
        }
        
        public static java.lang.String getSEARCH() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SEARCH");
            return com.huawei.hms.analytics.type.HAEventType.SEARCH;
        }
        
        public static java.lang.String getSELECT_CONTENT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWCONTENT");
            return com.huawei.hms.analytics.type.HAEventType.VIEWCONTENT;
        }
        
        public static java.lang.String getSET_CHECKOUT_OPTION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.UPDATECHECKOUTOPTION");
            return com.huawei.hms.analytics.type.HAEventType.UPDATECHECKOUTOPTION;
        }
        
        public static java.lang.String getSHARE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SHARECONTENT");
            return com.huawei.hms.analytics.type.HAEventType.SHARECONTENT;
        }
        
        public static java.lang.String getSIGN_UP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.REGISTERACCOUNT");
            return com.huawei.hms.analytics.type.HAEventType.REGISTERACCOUNT;
        }
        
        public static java.lang.String getSPEND_VIRTUAL_CURRENCY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.CONSUMEVIRTUALCOIN");
            return com.huawei.hms.analytics.type.HAEventType.CONSUMEVIRTUALCOIN;
        }
        
        public static java.lang.String getTUTORIAL_BEGIN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTTUTORIAL");
            return com.huawei.hms.analytics.type.HAEventType.STARTTUTORIAL;
        }
        
        public static java.lang.String getTUTORIAL_COMPLETE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.COMPLETETUTORIAL");
            return com.huawei.hms.analytics.type.HAEventType.COMPLETETUTORIAL;
        }
        
        public static java.lang.String getUNLOCK_ACHIEVEMENT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.OBTAINACHIEVEMENT");
            return com.huawei.hms.analytics.type.HAEventType.OBTAINACHIEVEMENT;
        }
        
        public static java.lang.String getVIEW_ITEM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCT");
            return com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCT;
        }
        
        public static java.lang.String getVIEW_ITEM_LIST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCTLIST");
            return com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCTLIST;
        }
        
        public static java.lang.String getVIEW_SEARCH_RESULTS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWSEARCHRESULT");
            return com.huawei.hms.analytics.type.HAEventType.VIEWSEARCHRESULT;
        }
        
        public static java.lang.String getADD_SHIPPING_INFO() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPURCHASE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getREFUND() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSELECT_ITEM() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSELECT_PROMOTION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getVIEW_CART() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getVIEW_PROMOTION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.Event dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.Event) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.type.HAEventType;
        }
    }
    
    public static class Param extends org.xms.g.utils.XObject {
        
        public Param(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected Param() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getACHIEVEMENT_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ACHIEVEMENTID");
            return com.huawei.hms.analytics.type.HAParamType.ACHIEVEMENTID;
        }
        
        public static java.lang.String getACLID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CLICKID");
            return com.huawei.hms.analytics.type.HAParamType.CLICKID;
        }
        
        public static java.lang.String getAFFILIATION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.STORENAME");
            return com.huawei.hms.analytics.type.HAParamType.STORENAME;
        }
        
        public static java.lang.String getCAMPAIGN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PROMOTIONNAME");
            return com.huawei.hms.analytics.type.HAParamType.PROMOTIONNAME;
        }
        
        public static java.lang.String getCHARACTER() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ROLENAME");
            return com.huawei.hms.analytics.type.HAParamType.ROLENAME;
        }
        
        public static java.lang.String getCHECKOUT_OPTION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.OPTION");
            return com.huawei.hms.analytics.type.HAParamType.OPTION;
        }
        
        public static java.lang.String getCHECKOUT_STEP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.STEP");
            return com.huawei.hms.analytics.type.HAParamType.STEP;
        }
        
        public static java.lang.String getCONTENT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CONTENT");
            return com.huawei.hms.analytics.type.HAParamType.CONTENT;
        }
        
        public static java.lang.String getCONTENT_TYPE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CONTENTTYPE");
            return com.huawei.hms.analytics.type.HAParamType.CONTENTTYPE;
        }
        
        public static java.lang.String getCOUPON() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.VOUCHER");
            return com.huawei.hms.analytics.type.HAParamType.VOUCHER;
        }
        
        public static java.lang.String getCP1() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.EXTENDPARAM");
            return com.huawei.hms.analytics.type.HAParamType.EXTENDPARAM;
        }
        
        public static java.lang.String getCREATIVE_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.MATERIALNAME");
            return com.huawei.hms.analytics.type.HAParamType.MATERIALNAME;
        }
        
        public static java.lang.String getCREATIVE_SLOT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.MATERIALSLOT");
            return com.huawei.hms.analytics.type.HAParamType.MATERIALSLOT;
        }
        
        public static java.lang.String getCURRENCY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CURRNAME");
            return com.huawei.hms.analytics.type.HAParamType.CURRNAME;
        }
        
        public static java.lang.String getDESTINATION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.DESTINATION");
            return com.huawei.hms.analytics.type.HAParamType.DESTINATION;
        }
        
        public static java.lang.String getEND_DATE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ENDDATE");
            return com.huawei.hms.analytics.type.HAParamType.ENDDATE;
        }
        
        public static java.lang.String getEXTEND_SESSION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getFLIGHT_NUMBER() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.FLIGHTNO");
            return com.huawei.hms.analytics.type.HAParamType.FLIGHTNO;
        }
        
        public static java.lang.String getGROUP_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.USERGROUPID");
            return com.huawei.hms.analytics.type.HAParamType.USERGROUPID;
        }
        
        public static java.lang.String getINDEX() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.POSITIONID");
            return com.huawei.hms.analytics.type.HAParamType.POSITIONID;
        }
        
        public static java.lang.String getITEM_BRAND() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BRAND");
            return com.huawei.hms.analytics.type.HAParamType.BRAND;
        }
        
        public static java.lang.String getITEM_CATEGORY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CATEGORY");
            return com.huawei.hms.analytics.type.HAParamType.CATEGORY;
        }
        
        public static java.lang.String getITEM_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTID");
            return com.huawei.hms.analytics.type.HAParamType.PRODUCTID;
        }
        
        public static java.lang.String getITEM_LIST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTLIST");
            return com.huawei.hms.analytics.type.HAParamType.PRODUCTLIST;
        }
        
        public static java.lang.String getITEM_LOCATION_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PLACEID");
            return com.huawei.hms.analytics.type.HAParamType.PLACEID;
        }
        
        public static java.lang.String getITEM_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTNAME");
            return com.huawei.hms.analytics.type.HAParamType.PRODUCTNAME;
        }
        
        public static java.lang.String getITEM_VARIANT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTFEATURE");
            return com.huawei.hms.analytics.type.HAParamType.PRODUCTFEATURE;
        }
        
        public static java.lang.String getLEVEL() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.LEVELID");
            return com.huawei.hms.analytics.type.HAParamType.LEVELID;
        }
        
        public static java.lang.String getLEVEL_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.LEVELNAME");
            return com.huawei.hms.analytics.type.HAParamType.LEVELNAME;
        }
        
        public static java.lang.String getLOCATION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PLACEID");
            return com.huawei.hms.analytics.type.HAParamType.PLACEID;
        }
        
        public static java.lang.String getMEDIUM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.MEDIUM");
            return com.huawei.hms.analytics.type.HAParamType.MEDIUM;
        }
        
        public static java.lang.String getMETHOD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CHANNEL");
            return com.huawei.hms.analytics.type.HAParamType.CHANNEL;
        }
        
        public static java.lang.String getNUMBER_OF_NIGHTS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BOOKINGDAYS");
            return com.huawei.hms.analytics.type.HAParamType.BOOKINGDAYS;
        }
        
        public static java.lang.String getNUMBER_OF_PASSENGERS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PASSENGERSNUMBER");
            return com.huawei.hms.analytics.type.HAParamType.PASSENGERSNUMBER;
        }
        
        public static java.lang.String getNUMBER_OF_ROOMS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BOOKINGROOMS");
            return com.huawei.hms.analytics.type.HAParamType.BOOKINGROOMS;
        }
        
        public static java.lang.String getORIGIN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ORIGINATINGPLACE");
            return com.huawei.hms.analytics.type.HAParamType.ORIGINATINGPLACE;
        }
        
        public static java.lang.String getPRICE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRICE");
            return com.huawei.hms.analytics.type.HAParamType.PRICE;
        }
        
        public static java.lang.String getQUANTITY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.QUANTITY");
            return com.huawei.hms.analytics.type.HAParamType.QUANTITY;
        }
        
        public static java.lang.String getSCORE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.SCORE");
            return com.huawei.hms.analytics.type.HAParamType.SCORE;
        }
        
        public static java.lang.String getSEARCH_TERM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.KEYWORDS");
            return com.huawei.hms.analytics.type.HAParamType.KEYWORDS;
        }
        
        public static java.lang.String getSHIPPING() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.SHIPPING");
            return com.huawei.hms.analytics.type.HAParamType.SHIPPING;
        }
        
        public static java.lang.String getSIGN_UP_METHOD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CHANNEL");
            return com.huawei.hms.analytics.type.HAParamType.CHANNEL;
        }
        
        public static java.lang.String getSOURCE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.SOURCE");
            return com.huawei.hms.analytics.type.HAParamType.SOURCE;
        }
        
        public static java.lang.String getSTART_DATE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BEGINDATE");
            return com.huawei.hms.analytics.type.HAParamType.BEGINDATE;
        }
        
        public static java.lang.String getSUCCESS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.RESULT");
            return com.huawei.hms.analytics.type.HAParamType.RESULT;
        }
        
        public static java.lang.String getTAX() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.TAXFEE");
            return com.huawei.hms.analytics.type.HAParamType.TAXFEE;
        }
        
        public static java.lang.String getTERM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.KEYWORDS");
            return com.huawei.hms.analytics.type.HAParamType.KEYWORDS;
        }
        
        public static java.lang.String getTRANSACTION_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.TRANSACTIONID");
            return com.huawei.hms.analytics.type.HAParamType.TRANSACTIONID;
        }
        
        public static java.lang.String getTRAVEL_CLASS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CLASS");
            return com.huawei.hms.analytics.type.HAParamType.CLASS;
        }
        
        public static java.lang.String getVALUE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.REVENUE");
            return com.huawei.hms.analytics.type.HAParamType.REVENUE;
        }
        
        public static java.lang.String getVIRTUAL_CURRENCY_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.VIRTUALCURRNAME");
            return com.huawei.hms.analytics.type.HAParamType.VIRTUALCURRNAME;
        }
        
        public static java.lang.String getDISCOUNT() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY2() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY3() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY4() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY5() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_LIST_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_LIST_NAME() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEMS() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getLOCATION_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPAYMENT_TYPE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPROMOTION_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPROMOTION_NAME() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSHIPPING_TIER() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.Param dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.Param) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.type.HAParamType;
        }
    }
    
    public static class UserProperty extends org.xms.g.utils.XObject {
        
        public UserProperty(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected UserProperty() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getALLOW_AD_PERSONALIZATION_SIGNALS() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSIGN_UP_METHOD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CHANNEL");
            return com.huawei.hms.analytics.type.HAParamType.CHANNEL;
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.UserProperty dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.UserProperty) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.type.HAParamType;
        }
    }
}