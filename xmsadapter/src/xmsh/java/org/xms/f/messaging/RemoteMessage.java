package org.xms.f.messaging;

public final class RemoteMessage extends org.xms.g.utils.XObject implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.f.messaging.RemoteMessage createFromParcel(android.os.Parcel param0) {
            com.huawei.hms.push.RemoteMessage hReturn = com.huawei.hms.push.RemoteMessage.CREATOR.createFromParcel(param0);
            return new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(hReturn));
        }
        
        public org.xms.f.messaging.RemoteMessage[] newArray(int param0) {
            return new org.xms.f.messaging.RemoteMessage[param0];
        }
    };
    
    public RemoteMessage(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static int getPRIORITY_HIGH() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.RemoteMessage.PRIORITY_HIGH");
        return com.huawei.hms.push.RemoteMessage.PRIORITY_HIGH;
    }
    
    public static int getPRIORITY_NORMAL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.RemoteMessage.PRIORITY_NORMAL");
        return com.huawei.hms.push.RemoteMessage.PRIORITY_NORMAL;
    }
    
    public static int getPRIORITY_UNKNOWN() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.RemoteMessage.PRIORITY_UNKNOWN");
        return com.huawei.hms.push.RemoteMessage.PRIORITY_UNKNOWN;
    }
    
    public final java.lang.String getCollapseKey() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getCollapseKey()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getCollapseKey();
    }
    
    public final java.util.Map<java.lang.String, java.lang.String> getData() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getDataOfMap()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getDataOfMap();
    }
    
    public final java.lang.String getFrom() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getFrom()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getFrom();
    }
    
    public final java.lang.String getMessageId() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageId()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageId();
    }
    
    public final java.lang.String getMessageType() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageType()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageType();
    }
    
    public org.xms.f.messaging.RemoteMessage.Notification getNotification() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getNotification()");
        com.huawei.hms.push.RemoteMessage.Notification hReturn = ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getNotification();
        return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Notification(new org.xms.g.utils.XBox(hReturn))));
    }
    
    public int getOriginalPriority() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getOriginalUrgency()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getOriginalUrgency();
    }
    
    public int getPriority() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getUrgency()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getUrgency();
    }
    
    public long getSentTime() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getSentTime()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getSentTime();
    }
    
    public final java.lang.String getTo() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTo()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTo();
    }
    
    public int getTtl() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTtl()");
        return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTtl();
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).writeToParcel(param0, param1)");
        ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).writeToParcel(param0, param1);
    }
    
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.f.messaging.RemoteMessage dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.RemoteMessage) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.RemoteMessage;
    }
    
    public static class Builder extends org.xms.g.utils.XObject {
        
        
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder(java.lang.String param0) {
            super(((org.xms.g.utils.XBox) null));
            
            this.setHInstance(new com.huawei.hms.push.RemoteMessage.Builder("push.hcm.upstream"));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder addData(java.lang.String param0, java.lang.String param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).addData(param0, param1)");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).addData(param0, param1);
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).build()");
            com.huawei.hms.push.RemoteMessage hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).build();
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder clearData() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).clearData()");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).clearData();
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setCollapseKey(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setCollapseKey(param0)");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setCollapseKey(param0);
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setData(java.util.Map<java.lang.String, java.lang.String> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setData(param0)");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setData(param0);
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setMessageId(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageId(param0)");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageId(param0);
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setMessageType(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageType(param0)");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageType(param0);
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setTtl(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setTtl(param0)");
            com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setTtl(param0);
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(hReturn))));
        }
        
        public static org.xms.f.messaging.RemoteMessage.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.messaging.RemoteMessage.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.RemoteMessage.Builder;
        }
    }
    
    public static class Notification extends org.xms.g.utils.XObject {
        
        public Notification(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.String getBody() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBody()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBody();
        }
        
        public java.lang.String[] getBodyLocalizationArgs() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationArgs()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationArgs();
        }
        
        public java.lang.String getBodyLocalizationKey() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationKey()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationKey();
        }
        
        public java.lang.String getChannelId() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getChannelId()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getChannelId();
        }
        
        public java.lang.String getClickAction() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getClickAction()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getClickAction();
        }
        
        public java.lang.String getColor() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getColor()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getColor();
        }
        
        public boolean getDefaultLightSettings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultLight()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultLight();
        }
        
        public boolean getDefaultSound() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultSound()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultSound();
        }
        
        public boolean getDefaultVibrateSettings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultVibrate()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultVibrate();
        }
        
        public java.lang.Long getEventTime() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getWhen()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getWhen();
        }
        
        public java.lang.String getIcon() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getIcon()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getIcon();
        }
        
        public android.net.Uri getImageUrl() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImageUrl()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImageUrl();
        }
        
        public int[] getLightSettings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLightSettings()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLightSettings();
        }
        
        public android.net.Uri getLink() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLink()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLink();
        }
        
        public boolean getLocalOnly() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isLocalOnly()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isLocalOnly();
        }
        
        public java.lang.Integer getNotificationCount() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBadgeNumber()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBadgeNumber();
        }
        
        public java.lang.Integer getNotificationPriority() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImportance()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImportance();
        }
        
        public java.lang.String getSound() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getSound()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getSound();
        }
        
        public boolean getSticky() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isAutoCancel()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isAutoCancel();
        }
        
        public java.lang.String getTag() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTag()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTag();
        }
        
        public java.lang.String getTicker() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTicker()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTicker();
        }
        
        public java.lang.String getTitle() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitle()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitle();
        }
        
        public java.lang.String[] getTitleLocalizationArgs() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationArgs()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationArgs();
        }
        
        public java.lang.String getTitleLocalizationKey() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationKey()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationKey();
        }
        
        public long[] getVibrateTimings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVibrateConfig()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVibrateConfig();
        }
        
        public java.lang.Integer getVisibility() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVisibility()");
            return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVisibility();
        }
        
        public static org.xms.f.messaging.RemoteMessage.Notification dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.messaging.RemoteMessage.Notification) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.RemoteMessage.Notification;
        }
    }
}