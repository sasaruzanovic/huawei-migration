package org.xms.f.messaging;

public class ExtensionMessaging extends org.xms.g.utils.XObject {

    public ExtensionMessaging(org.xms.g.utils.XBox param0) {
        super(param0);
    }

    public static java.lang.String getINSTANCE_ID_SCOPE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.HmsMessaging.DEFAULT_TOKEN_SCOPE");
        return com.huawei.hms.push.HmsMessaging.DEFAULT_TOKEN_SCOPE;
    }

    public static synchronized org.xms.f.messaging.ExtensionMessaging getInstance(android.content.Context context) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.HmsMessaging) this.getHInstance()).getInstance(context)");
        com.huawei.hms.push.HmsMessaging hReturn = com.huawei.hms.push.HmsMessaging.getInstance(context);
        return ((hReturn) == null ? null : new org.xms.f.messaging.ExtensionMessaging(new org.xms.g.utils.XBox(hReturn)));
    }

    public static synchronized org.xms.f.messaging.ExtensionMessaging getInstance() {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public void setDeliveryMetricsExportToBigQuery(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public boolean deliveryMetricsExportToBigQueryEnabled() {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public boolean isAutoInitEnabled() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.HmsMessaging) this.getHInstance()).isAutoInitEnabled()");
        return ((com.huawei.hms.push.HmsMessaging) this.getHInstance()).isAutoInitEnabled();
    }

    public void send(org.xms.f.messaging.RemoteMessage param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.HmsMessaging) this.getHInstance()).send(((com.huawei.hms.push.RemoteMessage) ((param0) == null ? null : (param0.getHInstance()))))");
        ((com.huawei.hms.push.HmsMessaging) this.getHInstance()).send(((com.huawei.hms.push.RemoteMessage) ((param0) == null ? null : (param0.getHInstance()))));
    }

    public void setAutoInitEnabled(boolean param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.HmsMessaging) this.getHInstance()).setAutoInitEnabled(param0)");
        ((com.huawei.hms.push.HmsMessaging) this.getHInstance()).setAutoInitEnabled(param0);
    }

    public org.xms.g.tasks.Task<java.lang.Void> subscribeToTopic(java.lang.String param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.HmsMessaging) this.getHInstance()).subscribe(param0)");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.push.HmsMessaging) this.getHInstance()).subscribe(param0);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }

    public org.xms.g.tasks.Task<java.lang.Void> unsubscribeFromTopic(java.lang.String param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.HmsMessaging) this.getHInstance()).unsubscribe(param0)");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.push.HmsMessaging) this.getHInstance()).unsubscribe(param0);
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }

    public static org.xms.f.messaging.ExtensionMessaging dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.ExtensionMessaging) param0);
    }

    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.HmsMessaging;
    }
}