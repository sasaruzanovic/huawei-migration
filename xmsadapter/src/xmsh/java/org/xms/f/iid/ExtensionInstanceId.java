package org.xms.f.iid;

public class ExtensionInstanceId extends org.xms.g.utils.XObject {

    public ExtensionInstanceId(org.xms.g.utils.XBox param0) {
        super(param0);
    }

    private String getAppId() {
        android.content.Context context = null;
        try {
            java.lang.reflect.Field field = null;
            java.lang.reflect.Field[] fields = com.huawei.hms.aaid.HmsInstanceId.class.getDeclaredFields();
            for (java.lang.reflect.Field f : fields) {
                if (f.getType().getName().equals("android.content.Context")) {
                    field = f;
                    break;
                }
            }
            field.setAccessible(true);
            context = (android.content.Context) field.get(this.getHInstance());
        } catch (java.lang.IllegalAccessException e) {
            org.xms.g.utils.XmsLog.d("XMSRouter", e.toString());
        }
        if (context != null) {
            return com.huawei.hms.utils.Util.getAppId(context);
        } else {
            throw new java.lang.RuntimeException("context cannot be null");
        }
    }

    public static org.xms.f.iid.ExtensionInstanceId getInstance(android.content.Context context) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getInstance(context)");
        com.huawei.hms.aaid.HmsInstanceId hReturn = com.huawei.hms.aaid.HmsInstanceId.getInstance(context);
        return ((hReturn) == null ? null : (new org.xms.f.iid.ExtensionInstanceId(new org.xms.g.utils.XBox(hReturn))));
    }

    public static org.xms.f.iid.ExtensionInstanceId getInstance() {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public static org.xms.f.iid.ExtensionInstanceId getInstance(org.xms.f.ExtensionApp param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public org.xms.g.tasks.Task<org.xms.f.iid.InstanceIdResult> getInstanceId() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getAAID()");
        com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getAAID();
        return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(hReturn))));
    }

    public java.lang.String getId() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getId()");
        return ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getId();
    }

    public long getCreationTime() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getCreationTime()");
        return ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getCreationTime();
    }

    public void deleteInstanceId() throws java.io.IOException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).deleteAAID()");
        try {
            ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).deleteAAID();
        } catch (com.huawei.hms.common.ApiException e) {
            throw new java.io.IOException(e);
        }
    }

    public java.lang.String getToken() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getToken()");
        return ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getToken();
    }

    public java.lang.String getToken(java.lang.String param0, java.lang.String param1) throws java.io.IOException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getToken(getAppId(), param1)");
        try {
            return ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).getToken(getAppId(), param1);
        } catch (com.huawei.hms.common.ApiException e) {
            throw new java.io.IOException(e);
        }
    }

    public void deleteToken(java.lang.String param0, java.lang.String param1) throws java.io.IOException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).deleteToken(getAppId(), param1)");
        try {
            ((com.huawei.hms.aaid.HmsInstanceId) this.getHInstance()).deleteToken(getAppId(), param1);
        } catch (com.huawei.hms.common.ApiException e) {
            throw new java.io.IOException(e);
        }
    }

    public static org.xms.f.iid.ExtensionInstanceId dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.iid.ExtensionInstanceId) param0);
    }

    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.aaid.HmsInstanceId;
    }
}