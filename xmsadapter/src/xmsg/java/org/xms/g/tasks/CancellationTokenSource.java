package org.xms.g.tasks;

public class CancellationTokenSource extends org.xms.g.utils.XObject {
    
    public CancellationTokenSource(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public void cancel() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.tasks.CancellationTokenSource) this.getGInstance()).cancel()");
        ((com.google.android.gms.tasks.CancellationTokenSource) this.getGInstance()).cancel();
    }
    
    public org.xms.g.tasks.CancellationToken getToken() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.tasks.CancellationTokenSource) this.getGInstance()).getToken()");
        com.google.android.gms.tasks.CancellationToken gReturn = ((com.google.android.gms.tasks.CancellationTokenSource) this.getGInstance()).getToken();
        return ((gReturn) == null ? null : (new org.xms.g.tasks.CancellationToken.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static org.xms.g.tasks.CancellationTokenSource dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.tasks.CancellationTokenSource) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.tasks.CancellationTokenSource;
    }
}