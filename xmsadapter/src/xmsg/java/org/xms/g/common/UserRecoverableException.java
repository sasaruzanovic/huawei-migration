package org.xms.g.common;

public class UserRecoverableException extends java.lang.Exception implements org.xms.g.utils.XGettable {
    public java.lang.Object gInstance;
    private boolean wrapper = true;
    
    public UserRecoverableException(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setGInstance(param0.getGInstance());
        wrapper = true;
    }
    
    public UserRecoverableException(java.lang.String param0, android.content.Intent param1) {
        this.setGInstance(new GImpl(param0, param1));
        wrapper = false;
    }
    
    public android.content.Intent getIntent() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.UserRecoverableException) this.getGInstance()).getIntent()");
            return ((com.google.android.gms.common.UserRecoverableException) this.getGInstance()).getIntent();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.common.UserRecoverableException) this.getGInstance())).getIntentCallSuper()");
            return ((GImpl) ((com.google.android.gms.common.UserRecoverableException) this.getGInstance())).getIntentCallSuper();
        }
    }
    
    public void setGInstance(java.lang.Object param0) {
        this.gInstance = param0;
    }
    
    public java.lang.Object getGInstance() {
        return this.gInstance;
    }
    
    public static org.xms.g.common.UserRecoverableException dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.UserRecoverableException) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.UserRecoverableException;
    }
    
    private class GImpl extends com.google.android.gms.common.UserRecoverableException {
        
        public android.content.Intent getIntent() {
            return org.xms.g.common.UserRecoverableException.this.getIntent();
        }
        
        public android.content.Intent getIntentCallSuper() {
            return super.getIntent();
        }
        
        public GImpl(java.lang.String param0, android.content.Intent param1) {
            super(param0, param1);
        }
    }
}