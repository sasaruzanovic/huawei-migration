package org.xms.g.common.api;

public abstract class ExtensionApi<XO extends org.xms.g.common.api.Api.ApiOptions> extends org.xms.g.utils.XObject implements org.xms.g.common.api.HasApiKey<XO> {
    
    public ExtensionApi(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static org.xms.g.common.api.ExtensionApi dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.api.ExtensionApi) {
            return ((org.xms.g.common.api.ExtensionApi) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.common.api.GoogleApi gReturn = ((com.google.android.gms.common.api.GoogleApi) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.common.api.ExtensionApi.XImpl(new org.xms.g.utils.XBox(gReturn));
        }
        return ((org.xms.g.common.api.ExtensionApi) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.GoogleApi;
    }
    
    public static class XImpl<XO extends org.xms.g.common.api.Api.ApiOptions> extends org.xms.g.common.api.ExtensionApi<XO> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.Object getApiKey() {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}