package org.xms.g.common.api;

public abstract class ExtensionApiClient extends org.xms.g.utils.XObject {
    private boolean wrapper = true;
    
    public ExtensionApiClient(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    public ExtensionApiClient() {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl());
        wrapper = false;
    }
    
    public static int getSIGN_IN_MODE_OPTIONAL() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getSIGN_IN_MODE_REQUIRED() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public abstract org.xms.g.common.ConnectionResult blockingConnect();
    
    public abstract org.xms.g.common.ConnectionResult blockingConnect(long param0, java.util.concurrent.TimeUnit param1);
    
    public abstract org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> clearDefaultAccountAndReconnect();
    
    public void connect(int param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).connect(param0)");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).connect(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance())).connectCallSuper(param0)");
            ((GImpl) ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance())).connectCallSuper(param0);
        }
    }
    
    public abstract void connect();
    
    public abstract void disconnect();
    
    public abstract void dump(java.lang.String param0, java.io.FileDescriptor param1, java.io.PrintWriter param2, java.lang.String[] param3);
    
    public static void dumpAll(java.lang.String param0, java.io.FileDescriptor param1, java.io.PrintWriter param2, java.lang.String[] param3) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public abstract org.xms.g.common.ConnectionResult getConnectionResult(org.xms.g.common.api.Api<?> param0);
    
    public abstract boolean hasConnectedApi(org.xms.g.common.api.Api<?> param0);
    
    public abstract boolean isConnected();
    
    public abstract boolean isConnecting();
    
    public abstract boolean isConnectionCallbacksRegistered(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0);
    
    public abstract boolean isConnectionFailedListenerRegistered(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0);
    
    public abstract void reconnect();
    
    public abstract void registerConnectionCallbacks(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0);
    
    public abstract void registerConnectionFailedListener(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0);
    
    public abstract void stopAutoManage(androidx.fragment.app.FragmentActivity param0) throws java.lang.IllegalStateException;
    
    public abstract void unregisterConnectionCallbacks(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0);
    
    public abstract void unregisterConnectionFailedListener(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0);
    
    public static org.xms.g.common.api.ExtensionApiClient dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.ExtensionApiClient) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.GoogleApiClient;
    }
    
    private class GImpl extends com.google.android.gms.common.api.GoogleApiClient {
        
        public void connect(int param0) {
            org.xms.g.common.api.ExtensionApiClient.this.connect(param0);
        }
        
        public void connectCallSuper(int param0) {
            super.connect(param0);
        }
        
        public com.google.android.gms.common.ConnectionResult blockingConnect() {
            org.xms.g.common.ConnectionResult xResult = org.xms.g.common.api.ExtensionApiClient.this.blockingConnect();
            return ((com.google.android.gms.common.ConnectionResult) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.common.ConnectionResult blockingConnect(long param0, java.util.concurrent.TimeUnit param1) {
            org.xms.g.common.ConnectionResult xResult = org.xms.g.common.api.ExtensionApiClient.this.blockingConnect(param0, param1);
            return ((com.google.android.gms.common.ConnectionResult) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status> clearDefaultAccountAndReconnect() {
            org.xms.g.common.api.PendingResult xResult = org.xms.g.common.api.ExtensionApiClient.this.clearDefaultAccountAndReconnect();
            return ((com.google.android.gms.common.api.PendingResult) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public void connect() {
            org.xms.g.common.api.ExtensionApiClient.this.connect();
        }
        
        public void disconnect() {
            org.xms.g.common.api.ExtensionApiClient.this.disconnect();
        }
        
        public void dump(java.lang.String param0, java.io.FileDescriptor param1, java.io.PrintWriter param2, java.lang.String[] param3) {
            org.xms.g.common.api.ExtensionApiClient.this.dump(param0, param1, param2, param3);
        }
        
        public com.google.android.gms.common.ConnectionResult getConnectionResult(com.google.android.gms.common.api.Api<?> param0) {
            org.xms.g.common.ConnectionResult xResult = org.xms.g.common.api.ExtensionApiClient.this.getConnectionResult(((param0) == null ? null : (new org.xms.g.common.api.Api(new org.xms.g.utils.XBox(param0)))));
            return ((com.google.android.gms.common.ConnectionResult) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public boolean hasConnectedApi(com.google.android.gms.common.api.Api<?> param0) {
            return org.xms.g.common.api.ExtensionApiClient.this.hasConnectedApi(((param0) == null ? null : (new org.xms.g.common.api.Api(new org.xms.g.utils.XBox(param0)))));
        }
        
        public boolean isConnected() {
            return org.xms.g.common.api.ExtensionApiClient.this.isConnected();
        }
        
        public boolean isConnecting() {
            return org.xms.g.common.api.ExtensionApiClient.this.isConnecting();
        }
        
        public boolean isConnectionCallbacksRegistered(com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks param0) {
            return org.xms.g.common.api.ExtensionApiClient.this.isConnectionCallbacksRegistered(((param0) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks.XImpl(new org.xms.g.utils.XBox(param0)))));
        }
        
        public boolean isConnectionFailedListenerRegistered(com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener param0) {
            return org.xms.g.common.api.ExtensionApiClient.this.isConnectionFailedListenerRegistered(((param0) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener.XImpl(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void reconnect() {
            org.xms.g.common.api.ExtensionApiClient.this.reconnect();
        }
        
        public void registerConnectionCallbacks(com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks param0) {
            org.xms.g.common.api.ExtensionApiClient.this.registerConnectionCallbacks(((param0) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks.XImpl(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void registerConnectionFailedListener(com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener param0) {
            org.xms.g.common.api.ExtensionApiClient.this.registerConnectionFailedListener(((param0) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener.XImpl(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void stopAutoManage(androidx.fragment.app.FragmentActivity param0) throws java.lang.IllegalStateException {
            org.xms.g.common.api.ExtensionApiClient.this.stopAutoManage(param0);
        }
        
        public void unregisterConnectionCallbacks(com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks param0) {
            org.xms.g.common.api.ExtensionApiClient.this.unregisterConnectionCallbacks(((param0) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks.XImpl(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void unregisterConnectionFailedListener(com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener param0) {
            org.xms.g.common.api.ExtensionApiClient.this.unregisterConnectionFailedListener(((param0) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener.XImpl(new org.xms.g.utils.XBox(param0)))));
        }
        
        public GImpl() {
            super();
        }
    }
    
    public static class XImpl extends org.xms.g.common.api.ExtensionApiClient {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.common.ConnectionResult blockingConnect() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).blockingConnect()");
            com.google.android.gms.common.ConnectionResult gReturn = ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).blockingConnect();
            return ((gReturn) == null ? null : (new org.xms.g.common.ConnectionResult(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.g.common.ConnectionResult blockingConnect(long param0, java.util.concurrent.TimeUnit param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).blockingConnect(param0, param1)");
            com.google.android.gms.common.ConnectionResult gReturn = ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).blockingConnect(param0, param1);
            return ((gReturn) == null ? null : (new org.xms.g.common.ConnectionResult(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> clearDefaultAccountAndReconnect() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).clearDefaultAccountAndReconnect()");
            com.google.android.gms.common.api.PendingResult gReturn = ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).clearDefaultAccountAndReconnect();
            return ((gReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public void connect() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).connect()");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).connect();
        }
        
        public void disconnect() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).disconnect()");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).disconnect();
        }
        
        public void dump(java.lang.String param0, java.io.FileDescriptor param1, java.io.PrintWriter param2, java.lang.String[] param3) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).dump(param0, param1, param2, param3)");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).dump(param0, param1, param2, param3);
        }
        
        public org.xms.g.common.ConnectionResult getConnectionResult(org.xms.g.common.api.Api<?> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).getConnectionResult(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))))");
            com.google.android.gms.common.ConnectionResult gReturn = ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).getConnectionResult(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))));
            return ((gReturn) == null ? null : (new org.xms.g.common.ConnectionResult(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public boolean hasConnectedApi(org.xms.g.common.api.Api<?> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).hasConnectedApi(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))))");
            return ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).hasConnectedApi(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))));
        }
        
        public boolean isConnected() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnected()");
            return ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnected();
        }
        
        public boolean isConnecting() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnecting()");
            return ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnecting();
        }
        
        public boolean isConnectionCallbacksRegistered(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnectionCallbacksRegistered(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())))");
            return ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnectionCallbacksRegistered(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())));
        }
        
        public boolean isConnectionFailedListenerRegistered(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnectionFailedListenerRegistered(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())))");
            return ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).isConnectionFailedListenerRegistered(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())));
        }
        
        public void reconnect() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).reconnect()");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).reconnect();
        }
        
        public void registerConnectionCallbacks(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).registerConnectionCallbacks(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())))");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).registerConnectionCallbacks(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())));
        }
        
        public void registerConnectionFailedListener(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).registerConnectionFailedListener(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())))");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).registerConnectionFailedListener(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())));
        }
        
        public void stopAutoManage(androidx.fragment.app.FragmentActivity param0) throws java.lang.IllegalStateException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).stopAutoManage(param0)");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).stopAutoManage(param0);
        }
        
        public void unregisterConnectionCallbacks(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).unregisterConnectionCallbacks(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())))");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).unregisterConnectionCallbacks(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())));
        }
        
        public void unregisterConnectionFailedListener(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).unregisterConnectionFailedListener(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())))");
            ((com.google.android.gms.common.api.GoogleApiClient) this.getGInstance()).unregisterConnectionFailedListener(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())));
        }
    }
    
    public static final class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder(android.content.Context param0) {
            super(((org.xms.g.utils.XBox) null));
            this.setGInstance(new com.google.android.gms.common.api.GoogleApiClient.Builder(param0));
        }
        
        public Builder(android.content.Context param0, org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param1, org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param2) {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public final <XO extends org.xms.g.common.api.Api.ApiOptions.HasOptions> org.xms.g.common.api.ExtensionApiClient.Builder addApi(org.xms.g.common.api.Api<XO> param0, XO param1) {
            com.google.android.gms.common.api.Api.ApiOptions.HasOptions gObj1 = ((com.google.android.gms.common.api.Api.ApiOptions.HasOptions) org.xms.g.utils.Utils.getInstanceInInterface(param1, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApi(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))), gObj1)");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApi(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))), gObj1);
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder addApi(org.xms.g.common.api.Api<? extends org.xms.g.common.api.Api.ApiOptions.NotRequiredOptions> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApi(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApi(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final <XO extends org.xms.g.common.api.Api.ApiOptions.HasOptions> org.xms.g.common.api.ExtensionApiClient.Builder addApiIfAvailable(org.xms.g.common.api.Api<XO> param0, XO param1, org.xms.g.common.api.Scope... param2) {
            com.google.android.gms.common.api.Api.ApiOptions.HasOptions gObj1 = ((com.google.android.gms.common.api.Api.ApiOptions.HasOptions) org.xms.g.utils.Utils.getInstanceInInterface(param1, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApiIfAvailable(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))), gObj1, ((com.google.android.gms.common.api.Scope[]) org.xms.g.utils.Utils.genericArrayCopy(param2, com.google.android.gms.common.api.Scope.class, x -> (com.google.android.gms.common.api.Scope)x.getGInstance())))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApiIfAvailable(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))), gObj1, ((com.google.android.gms.common.api.Scope[]) org.xms.g.utils.Utils.genericArrayCopy(param2, com.google.android.gms.common.api.Scope.class, x -> (com.google.android.gms.common.api.Scope)x.getGInstance())));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder addApiIfAvailable(org.xms.g.common.api.Api<? extends org.xms.g.common.api.Api.ApiOptions.NotRequiredOptions> param0, org.xms.g.common.api.Scope[] param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApiIfAvailable(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))), ((com.google.android.gms.common.api.Scope[]) org.xms.g.utils.Utils.genericArrayCopy(param1, com.google.android.gms.common.api.Scope.class, x -> (com.google.android.gms.common.api.Scope)x.getGInstance())))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addApiIfAvailable(((com.google.android.gms.common.api.Api) ((param0) == null ? null : (param0.getGInstance()))), ((com.google.android.gms.common.api.Scope[]) org.xms.g.utils.Utils.genericArrayCopy(param1, com.google.android.gms.common.api.Scope.class, x -> (com.google.android.gms.common.api.Scope)x.getGInstance())));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder addConnectionCallbacks(org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addConnectionCallbacks(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addConnectionCallbacks(((param0) == null ? null : (param0.getGInstanceConnectionCallbacks())));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder addOnConnectionFailedListener(org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addOnConnectionFailedListener(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addOnConnectionFailedListener(((param0) == null ? null : (param0.getGInstanceOnConnectionFailedListener())));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder addScope(org.xms.g.common.api.Scope param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addScope(((com.google.android.gms.common.api.Scope) ((param0) == null ? null : (param0.getGInstance()))))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).addScope(((com.google.android.gms.common.api.Scope) ((param0) == null ? null : (param0.getGInstance()))));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).build()");
            com.google.android.gms.common.api.GoogleApiClient gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).build();
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder enableAutoManage(androidx.fragment.app.FragmentActivity param0, org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param1) throws java.lang.NullPointerException, java.lang.IllegalStateException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).enableAutoManage(param0, ((param1) == null ? null : (param1.getGInstanceOnConnectionFailedListener())))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).enableAutoManage(param0, ((param1) == null ? null : (param1.getGInstanceOnConnectionFailedListener())));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder enableAutoManage(androidx.fragment.app.FragmentActivity param0, int param1, org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener param2) throws java.lang.NullPointerException, java.lang.NullPointerException, java.lang.IllegalStateException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).enableAutoManage(param0, param1, ((param2) == null ? null : (param2.getGInstanceOnConnectionFailedListener())))");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).enableAutoManage(param0, param1, ((param2) == null ? null : (param2.getGInstanceOnConnectionFailedListener())));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder setAccountName(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setAccountName(param0)");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setAccountName(param0);
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder setGravityForPopups(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setGravityForPopups(param0)");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setGravityForPopups(param0);
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder setHandler(android.os.Handler param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setHandler(param0)");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setHandler(param0);
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder setViewForPopups(android.view.View param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setViewForPopups(param0)");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).setViewForPopups(param0);
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.common.api.ExtensionApiClient.Builder useDefaultAccount() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).useDefaultAccount()");
            com.google.android.gms.common.api.GoogleApiClient.Builder gReturn = ((com.google.android.gms.common.api.GoogleApiClient.Builder) this.getGInstance()).useDefaultAccount();
            return ((gReturn) == null ? null : (new org.xms.g.common.api.ExtensionApiClient.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public static org.xms.g.common.api.ExtensionApiClient.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.common.api.ExtensionApiClient.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.GoogleApiClient.Builder;
        }
    }
    
    public static interface ConnectionCallbacks extends org.xms.g.utils.XInterface {
        
        public static int getCAUSE_NETWORK_LOST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST");
            return com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST;
        }
        
        public static int getCAUSE_SERVICE_DISCONNECTED() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED");
            return com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED;
        }
        
        public void onConnected(android.os.Bundle param0);
        
        public void onConnectionSuspended(int param0);
        
        default java.lang.Object getZInstanceConnectionCallbacks() {
            return getGInstanceConnectionCallbacks();
        }
        
        default com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks getGInstanceConnectionCallbacks() {
            if (this instanceof org.xms.g.utils.XGettable) {
                return ((com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks) ((org.xms.g.utils.XGettable) this).getGInstance());
            }
            return new com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks() {
                
                public void onConnected(android.os.Bundle param0) {
                    org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks.this.onConnected(param0);
                }
                
                public void onConnectionSuspended(int param0) {
                    org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks.this.onConnectionSuspended(param0);
                }
            };
        }
        
        public static org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XInterface)) {
                return false;
            }
            if (param0 instanceof org.xms.g.utils.XGettable) {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
            }
            return param0 instanceof org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks;
        }
        
        public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.common.api.ExtensionApiClient.ConnectionCallbacks {
            
            public XImpl(org.xms.g.utils.XBox param0) {
                super(param0);
            }
            
            public void onConnected(android.os.Bundle param0) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks) this.getGInstance()).onConnected(param0)");
                ((com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks) this.getGInstance()).onConnected(param0);
            }
            
            public void onConnectionSuspended(int param0) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks) this.getGInstance()).onConnectionSuspended(param0)");
                ((com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks) this.getGInstance()).onConnectionSuspended(param0);
            }
        }
    }
    
    public static interface OnConnectionFailedListener extends org.xms.g.utils.XInterface {
        
        public void onConnectionFailed(org.xms.g.common.ConnectionResult param0);
        
        default java.lang.Object getZInstanceOnConnectionFailedListener() {
            return getGInstanceOnConnectionFailedListener();
        }
        
        default com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener getGInstanceOnConnectionFailedListener() {
            if (this instanceof org.xms.g.utils.XGettable) {
                return ((com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener) ((org.xms.g.utils.XGettable) this).getGInstance());
            }
            return new com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener() {
                
                public void onConnectionFailed(com.google.android.gms.common.ConnectionResult param0) {
                    org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener.this.onConnectionFailed(((param0) == null ? null : (new org.xms.g.common.ConnectionResult(new org.xms.g.utils.XBox(param0)))));
                }
            };
        }
        
        public static org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XInterface)) {
                return false;
            }
            if (param0 instanceof org.xms.g.utils.XGettable) {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
            }
            return param0 instanceof org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener;
        }
        
        public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.common.api.ExtensionApiClient.OnConnectionFailedListener {
            
            public XImpl(org.xms.g.utils.XBox param0) {
                super(param0);
            }
            
            public void onConnectionFailed(org.xms.g.common.ConnectionResult param0) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener) this.getGInstance()).onConnectionFailed(((com.google.android.gms.common.ConnectionResult) ((param0) == null ? null : (param0.getGInstance()))))");
                ((com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener) this.getGInstance()).onConnectionFailed(((com.google.android.gms.common.ConnectionResult) ((param0) == null ? null : (param0.getGInstance()))));
            }
        }
    }
}