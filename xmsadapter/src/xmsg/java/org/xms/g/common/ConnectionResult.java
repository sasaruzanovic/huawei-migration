package org.xms.g.common;

public final class ConnectionResult extends org.xms.g.utils.XObject implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.common.ConnectionResult createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.common.ConnectionResult gReturn = com.google.android.gms.common.ConnectionResult.CREATOR.createFromParcel(param0);
            return new org.xms.g.common.ConnectionResult(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.common.ConnectionResult[] newArray(int param0) {
            return new org.xms.g.common.ConnectionResult[param0];
        }
    };
    
    public ConnectionResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public ConnectionResult(int param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new com.google.android.gms.common.ConnectionResult(param0));
    }
    
    public ConnectionResult(int param0, android.app.PendingIntent param1) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new com.google.android.gms.common.ConnectionResult(param0, param1));
    }
    
    public ConnectionResult(int param0, android.app.PendingIntent param1, java.lang.String param2) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new com.google.android.gms.common.ConnectionResult(param0, param1, param2));
    }
    
    public static int getAPI_UNAVAILABLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.API_UNAVAILABLE");
        return com.google.android.gms.common.ConnectionResult.API_UNAVAILABLE;
    }
    
    public static int getCANCELED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.CANCELED");
        return com.google.android.gms.common.ConnectionResult.CANCELED;
    }
    
    public static int getDEVELOPER_ERROR() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.DEVELOPER_ERROR");
        return com.google.android.gms.common.ConnectionResult.DEVELOPER_ERROR;
    }
    
    public static int getDRIVE_EXTERNAL_STORAGE_REQUIRED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED");
        return com.google.android.gms.common.ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED;
    }
    
    public static int getINTERNAL_ERROR() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.INTERNAL_ERROR");
        return com.google.android.gms.common.ConnectionResult.INTERNAL_ERROR;
    }
    
    public static int getINTERRUPTED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.INTERRUPTED");
        return com.google.android.gms.common.ConnectionResult.INTERRUPTED;
    }
    
    public static int getINVALID_ACCOUNT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.INVALID_ACCOUNT");
        return com.google.android.gms.common.ConnectionResult.INVALID_ACCOUNT;
    }
    
    public static int getLICENSE_CHECK_FAILED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.LICENSE_CHECK_FAILED");
        return com.google.android.gms.common.ConnectionResult.LICENSE_CHECK_FAILED;
    }
    
    public static int getNETWORK_ERROR() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.NETWORK_ERROR");
        return com.google.android.gms.common.ConnectionResult.NETWORK_ERROR;
    }
    
    public static int getRESOLUTION_REQUIRED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.RESOLUTION_REQUIRED");
        return com.google.android.gms.common.ConnectionResult.RESOLUTION_REQUIRED;
    }
    
    public static int getRESTRICTED_PROFILE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.RESTRICTED_PROFILE");
        return com.google.android.gms.common.ConnectionResult.RESTRICTED_PROFILE;
    }
    
    public static int getSERVICE_DISABLED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SERVICE_DISABLED");
        return com.google.android.gms.common.ConnectionResult.SERVICE_DISABLED;
    }
    
    public static int getSERVICE_INVALID() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SERVICE_INVALID");
        return com.google.android.gms.common.ConnectionResult.SERVICE_INVALID;
    }
    
    public static int getSERVICE_MISSING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SERVICE_MISSING");
        return com.google.android.gms.common.ConnectionResult.SERVICE_MISSING;
    }
    
    public static int getSERVICE_MISSING_PERMISSION() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SERVICE_MISSING_PERMISSION");
        return com.google.android.gms.common.ConnectionResult.SERVICE_MISSING_PERMISSION;
    }
    
    public static int getSERVICE_UPDATING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SERVICE_UPDATING");
        return com.google.android.gms.common.ConnectionResult.SERVICE_UPDATING;
    }
    
    public static int getSERVICE_VERSION_UPDATE_REQUIRED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED");
        return com.google.android.gms.common.ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED;
    }
    
    public static int getSIGN_IN_FAILED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SIGN_IN_FAILED");
        return com.google.android.gms.common.ConnectionResult.SIGN_IN_FAILED;
    }
    
    public static int getSIGN_IN_REQUIRED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SIGN_IN_REQUIRED");
        return com.google.android.gms.common.ConnectionResult.SIGN_IN_REQUIRED;
    }
    
    public static int getSUCCESS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.SUCCESS");
        return com.google.android.gms.common.ConnectionResult.SUCCESS;
    }
    
    public static int getTIMEOUT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.ConnectionResult.TIMEOUT");
        return com.google.android.gms.common.ConnectionResult.TIMEOUT;
    }
    
    public final boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).equals(param0)");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).equals(param0);
    }
    
    public final int getErrorCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).getErrorCode()");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).getErrorCode();
    }
    
    public final java.lang.String getErrorMessage() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).getErrorMessage()");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).getErrorMessage();
    }
    
    public final android.app.PendingIntent getResolution() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).getResolution()");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).getResolution();
    }
    
    public final boolean hasResolution() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).hasResolution()");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).hasResolution();
    }
    
    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).hashCode()");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).hashCode();
    }
    
    public final boolean isSuccess() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).isSuccess()");
        return ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).isSuccess();
    }
    
    public void startResolutionForResult(android.app.Activity param0, int param1) throws android.content.IntentSender.SendIntentException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).startResolutionForResult(param0, param1)");
        ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).startResolutionForResult(param0, param1);
    }
    
    public final java.lang.String toString() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.ConnectionResult) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.common.ConnectionResult) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.common.ConnectionResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.ConnectionResult) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.ConnectionResult;
    }
}