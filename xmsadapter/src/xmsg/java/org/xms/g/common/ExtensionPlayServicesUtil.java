package org.xms.g.common;

public final class ExtensionPlayServicesUtil extends org.xms.g.utils.XObject {
    
    public ExtensionPlayServicesUtil(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static java.lang.String getGMS_ERROR_DIALOG() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.GMS_ERROR_DIALOG");
        return com.google.android.gms.common.GooglePlayServicesUtil.GMS_ERROR_DIALOG;
    }
    
    public static java.lang.String getGOOGLE_PLAY_SERVICES_PACKAGE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getGOOGLE_PLAY_STORE_PACKAGE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getGOOGLE_PLAY_SERVICES_VERSION_CODE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE");
        return com.google.android.gms.common.GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }
    
    public static android.app.Dialog getErrorDialog(int param0, android.app.Activity param1, int param2) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.getErrorDialog(param0, param1, param2)");
        return com.google.android.gms.common.GooglePlayServicesUtil.getErrorDialog(param0, param1, param2);
    }
    
    public static android.app.Dialog getErrorDialog(int param0, android.app.Activity param1, int param2, android.content.DialogInterface.OnCancelListener param3) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.getErrorDialog(param0, param1, param2, param3)");
        return com.google.android.gms.common.GooglePlayServicesUtil.getErrorDialog(param0, param1, param2, param3);
    }
    
    public static android.app.PendingIntent getErrorPendingIntent(int param0, android.content.Context param1, int param2) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.getErrorPendingIntent(param0, param1, param2)");
        return com.google.android.gms.common.GooglePlayServicesUtil.getErrorPendingIntent(param0, param1, param2);
    }
    
    public static java.lang.String getErrorString(int param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.getErrorString(param0)");
        return com.google.android.gms.common.GooglePlayServicesUtil.getErrorString(param0);
    }
    
    public static final android.content.Context getRemoteContext(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.getRemoteContext(param0)");
        return com.google.android.gms.common.GooglePlayServicesUtil.getRemoteContext(param0);
    }
    
    public static final android.content.res.Resources getRemoteResource(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.getRemoteResource(param0)");
        return com.google.android.gms.common.GooglePlayServicesUtil.getRemoteResource(param0);
    }
    
    public static int isGooglePlayServicesAvailable(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(param0)");
        return com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(param0);
    }
    
    public static int isGooglePlayServicesAvailable(android.content.Context param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(param0, param1)");
        return com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(param0, param1);
    }
    
    public static boolean isUserRecoverableError(int param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.isUserRecoverableError(param0)");
        return com.google.android.gms.common.GooglePlayServicesUtil.isUserRecoverableError(param0);
    }
    
    public static boolean showErrorDialogFragment(int param0, android.app.Activity param1, int param2, android.content.DialogInterface.OnCancelListener param3) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.showErrorDialogFragment(param0, param1, param2, param3)");
        return com.google.android.gms.common.GooglePlayServicesUtil.showErrorDialogFragment(param0, param1, param2, param3);
    }
    
    public static boolean showErrorDialogFragment(int param0, android.app.Activity param1, androidx.fragment.app.Fragment param2, int param3, android.content.DialogInterface.OnCancelListener param4) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.showErrorDialogFragment(param0, param1, param2, param3, param4)");
        return com.google.android.gms.common.GooglePlayServicesUtil.showErrorDialogFragment(param0, param1, param2, param3, param4);
    }
    
    public static boolean showErrorDialogFragment(int param0, android.app.Activity param1, int param2) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.showErrorDialogFragment(param0, param1, param2)");
        return com.google.android.gms.common.GooglePlayServicesUtil.showErrorDialogFragment(param0, param1, param2);
    }
    
    public static final void showErrorNotification(int param0, android.content.Context param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.GooglePlayServicesUtil.showErrorNotification(param0, param1)");
        com.google.android.gms.common.GooglePlayServicesUtil.showErrorNotification(param0, param1);
    }
    
    public static org.xms.g.common.ExtensionPlayServicesUtil dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.ExtensionPlayServicesUtil) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.GooglePlayServicesUtil;
    }
}