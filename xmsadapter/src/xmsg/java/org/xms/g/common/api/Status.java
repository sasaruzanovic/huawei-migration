package org.xms.g.common.api;

public final class Status extends org.xms.g.utils.XObject implements org.xms.g.common.api.Result, android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {

        public org.xms.g.common.api.Status createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.common.api.Status gReturn = com.google.android.gms.common.api.Status.CREATOR.createFromParcel(param0);
            return new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn));
        }

        public org.xms.g.common.api.Status[] newArray(int param0) {
            return new org.xms.g.common.api.Status[param0];
        }
    };

    public Status(org.xms.g.utils.XBox param0) {
        super(param0);
    }

    public Status(int param0) {
        super((org.xms.g.utils.XBox) null);
        this.setGInstance(new com.google.android.gms.common.api.Status(param0));
    }

    public Status(int param0, java.lang.String param1) {
        super((org.xms.g.utils.XBox) null);
        this.setGInstance(new com.google.android.gms.common.api.Status(param0, param1));
    }

    public Status(int param0, java.lang.String param1, android.app.PendingIntent param2) {
        super((org.xms.g.utils.XBox) null);
        this.setGInstance(new com.google.android.gms.common.api.Status(param0, param1, param2));
    }

    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).equals(param0)");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).equals(param0);
    }

    public final android.app.PendingIntent getResolution() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).getResolution()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).getResolution();
    }

    public final org.xms.g.common.api.Status getStatus() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).getStatus()");
        com.google.android.gms.common.api.Status gReturn = ((com.google.android.gms.common.api.Status) this.getGInstance()).getStatus();
        return ((gReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn))));
    }

    public final int getStatusCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).getStatusCode()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).getStatusCode();
    }

    public final java.lang.String getStatusMessage() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).getStatusMessage()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).getStatusMessage();
    }

    public final boolean hasResolution() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).hasResolution()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).hasResolution();
    }

    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).hashCode()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).hashCode();
    }

    public final boolean isCanceled() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).isCanceled()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).isCanceled();
    }

    public final boolean isInterrupted() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).isInterrupted()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).isInterrupted();
    }

    public final boolean isSuccess() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).isSuccess()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).isSuccess();
    }

    public void startResolutionForResult(android.app.Activity param0, int param1) throws android.content.IntentSender.SendIntentException {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).startResolutionForResult(param0, param1)");
        ((com.google.android.gms.common.api.Status) this.getGInstance()).startResolutionForResult(param0, param1);
    }

    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).toString()");
        return ((com.google.android.gms.common.api.Status) this.getGInstance()).toString();
    }

    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Status) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.common.api.Status) this.getGInstance()).writeToParcel(param0, param1);
    }

    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }

    public static org.xms.g.common.api.Status dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.api.Status) {
            return ((org.xms.g.common.api.Status) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.common.api.Status gReturn = ((com.google.android.gms.common.api.Status) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn));
        }
        if (param0 instanceof com.google.android.gms.common.api.Status) {
            return new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(param0));
        }
        return ((org.xms.g.common.api.Status) param0);
    }

    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.Status;
    }
}