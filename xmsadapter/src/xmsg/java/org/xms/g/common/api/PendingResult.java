package org.xms.g.common.api;

public abstract class PendingResult<XR extends org.xms.g.common.api.Result> extends org.xms.g.utils.XObject {
    
    public PendingResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public abstract XR await();
    
    public abstract XR await(long param0, java.util.concurrent.TimeUnit param1);
    
    public abstract void cancel();
    
    public abstract boolean isCanceled();
    
    public abstract void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0);
    
    public abstract void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0, long param1, java.util.concurrent.TimeUnit param2);
    
    public <XS extends org.xms.g.common.api.Result> org.xms.g.common.api.TransformedResult<XS> then(org.xms.g.common.api.ResultTransform<? super XR, ? extends XS> param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).then(((com.google.android.gms.common.api.ResultTransform) ((param0) == null ? null : (param0.getGInstance()))))");
        com.google.android.gms.common.api.TransformedResult gReturn = ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).then(((com.google.android.gms.common.api.ResultTransform) ((param0) == null ? null : (param0.getGInstance()))));
        return ((gReturn) == null ? null : (new org.xms.g.common.api.TransformedResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static org.xms.g.common.api.PendingResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.PendingResult) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.PendingResult;
    }
    
    public static class XImpl<XR extends org.xms.g.common.api.Result> extends org.xms.g.common.api.PendingResult<XR> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public XR await() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).await()");
            java.lang.Object gmsObj = ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).await();
            return ((XR) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(gmsObj));
        }
        
        public XR await(long param0, java.util.concurrent.TimeUnit param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).await(param0, param1)");
            java.lang.Object gmsObj = ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).await(param0, param1);
            return ((XR) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(gmsObj));
        }
        
        public void cancel() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).cancel()");
            ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).cancel();
        }
        
        public boolean isCanceled() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).isCanceled()");
            return ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).isCanceled();
        }
        
        public void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).setResultCallback(((param0) == null ? null : (param0.getGInstanceResultCallback())))");
            ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).setResultCallback(((param0) == null ? null : (param0.getGInstanceResultCallback())));
        }
        
        public void setResultCallback(org.xms.g.common.api.ResultCallback<? super XR> param0, long param1, java.util.concurrent.TimeUnit param2) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.PendingResult) this.getGInstance()).setResultCallback(((param0) == null ? null : (param0.getGInstanceResultCallback())), param1, param2)");
            ((com.google.android.gms.common.api.PendingResult) this.getGInstance()).setResultCallback(((param0) == null ? null : (param0.getGInstanceResultCallback())), param1, param2);
        }
    }
}