package org.xms.g.common.api;

public interface ResultCallback<XR extends org.xms.g.common.api.Result> extends org.xms.g.utils.XInterface {
    
    public void onResult(XR param0);
    
    default java.lang.Object getZInstanceResultCallback() {
        return getGInstanceResultCallback();
    }
    
    default <R extends com.google.android.gms.common.api.Result> com.google.android.gms.common.api.ResultCallback<R> getGInstanceResultCallback() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.google.android.gms.common.api.ResultCallback<R>) ((org.xms.g.utils.XGettable) this).getGInstance());
        }
        return new com.google.android.gms.common.api.ResultCallback<R>() {
            
            public void onResult(R param0) {
                java.lang.Object[] params = new java.lang.Object[1];
                java.lang.Class[] types = new java.lang.Class[1];
                params[0] = param0;
                types[0] = org.xms.g.common.api.Result.class;
                org.xms.g.utils.Utils.invokeMethod(org.xms.g.common.api.ResultCallback.this, "onResult", params, types, false);
            }
        };
    }
    
    public static org.xms.g.common.api.ResultCallback dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.ResultCallback) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.ResultCallback;
        }
        return param0 instanceof org.xms.g.common.api.ResultCallback;
    }
    
    public static class XImpl<XR extends org.xms.g.common.api.Result> extends org.xms.g.utils.XObject implements org.xms.g.common.api.ResultCallback<XR> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public void onResult(XR param0) {
            com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResultCallback) this.getGInstance()).onResult(gObj0)");
            ((com.google.android.gms.common.api.ResultCallback) this.getGInstance()).onResult(gObj0);
        }
    }
}