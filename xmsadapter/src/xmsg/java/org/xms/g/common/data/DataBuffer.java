package org.xms.g.common.data;

public interface DataBuffer<XT> extends org.xms.g.utils.XInterface, org.xms.g.common.api.Releasable, java.io.Closeable, java.lang.Iterable<XT> {
    
    public void close();
    
    public XT get(int param0);
    
    public int getCount();
    
    public boolean isClosed();
    
    public java.util.Iterator<XT> iterator();
    
    public void release();
    
    public java.util.Iterator<XT> singleRefIterator();
    
    default java.lang.Object getZInstanceDataBuffer() {
        return getGInstanceDataBuffer();
    }
    
    default <T> com.google.android.gms.common.data.DataBuffer<T> getGInstanceDataBuffer() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.google.android.gms.common.data.DataBuffer<T>) ((org.xms.g.utils.XGettable) this).getGInstance());
        }
        throw new java.lang.RuntimeException("Not for inheriting");
    }
    
    public static org.xms.g.common.data.DataBuffer dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.data.DataBuffer) {
            return ((org.xms.g.common.data.DataBuffer) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.common.data.DataBuffer gReturn = ((com.google.android.gms.common.data.DataBuffer) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.common.data.DataBuffer.XImpl(new org.xms.g.utils.XBox(gReturn));
        }
        return ((org.xms.g.common.data.DataBuffer) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.data.DataBuffer;
        }
        return param0 instanceof org.xms.g.common.data.DataBuffer;
    }
    
    public static class XImpl<XT> extends org.xms.g.utils.XObject implements org.xms.g.common.data.DataBuffer<XT> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public void close() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).close()");
            ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).close();
        }
        
        public XT get(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).get(param0)");
            java.lang.Object gmsObj = ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).get(param0);
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(gmsObj));
        }
        
        public int getCount() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).getCount()");
            return ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).getCount();
        }
        
        public boolean isClosed() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).isClosed()");
            return ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).isClosed();
        }
        
        public java.util.Iterator<XT> iterator() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).iterator()");
            java.util.Iterator gReturn = ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).iterator();
            return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(gReturn, new org.xms.g.utils.Function<Object, XT>() {
                
                public XT apply(java.lang.Object param0) {
                    return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(param0));
                }
            }));
        }
        
        public void release() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).release()");
            ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).release();
        }
        
        public java.util.Iterator<XT> singleRefIterator() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).singleRefIterator()");
            java.util.Iterator gReturn = ((com.google.android.gms.common.data.DataBuffer) this.getGInstance()).singleRefIterator();
            return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(gReturn, new org.xms.g.utils.Function<Object, XT>() {
                
                public XT apply(java.lang.Object param0) {
                    return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(param0));
                }
            }));
        }
    }
}