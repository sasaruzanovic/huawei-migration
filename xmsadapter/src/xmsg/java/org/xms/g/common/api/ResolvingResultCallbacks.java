package org.xms.g.common.api;

public abstract class ResolvingResultCallbacks<XR extends org.xms.g.common.api.Result> extends org.xms.g.common.api.ResultCallbacks<XR> {
    private boolean wrapper = true;
    
    public ResolvingResultCallbacks(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    public ResolvingResultCallbacks(android.app.Activity param0, int param1) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl(param0, param1));
        wrapper = false;
    }
    
    public abstract void onSuccess(XR param0);
    
    public abstract void onUnresolvableFailure(org.xms.g.common.api.Status param0);
    
    public static org.xms.g.common.api.ResolvingResultCallbacks dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.api.ResolvingResultCallbacks) {
            return ((org.xms.g.common.api.ResolvingResultCallbacks) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.common.api.ResolvingResultCallbacks gReturn = ((com.google.android.gms.common.api.ResolvingResultCallbacks) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.common.api.ResolvingResultCallbacks.XImpl(new org.xms.g.utils.XBox(gReturn));
        }
        return ((org.xms.g.common.api.ResolvingResultCallbacks) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.ResolvingResultCallbacks;
    }
    
    private class GImpl<R extends com.google.android.gms.common.api.Result> extends com.google.android.gms.common.api.ResolvingResultCallbacks<R> {
        
        public void onSuccess(R param0) {
            java.lang.Object[] params = new java.lang.Object[1];
            java.lang.Class[] types = new java.lang.Class[1];
            params[0] = param0;
            types[0] = org.xms.g.common.api.Result.class;
            org.xms.g.utils.Utils.invokeMethod(org.xms.g.common.api.ResolvingResultCallbacks.this, "onSuccess", params, types, false);
        }
        
        public void onUnresolvableFailure(com.google.android.gms.common.api.Status param0) {
            org.xms.g.common.api.ResolvingResultCallbacks.this.onUnresolvableFailure(((param0) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(param0)))));
        }
        
        public GImpl(android.app.Activity param0, int param1) {
            super(param0, param1);
        }
    }
    
    public static class XImpl<XR extends org.xms.g.common.api.Result> extends org.xms.g.common.api.ResolvingResultCallbacks<XR> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public void onSuccess(XR param0) {
            com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onSuccess(gObj0)");
            ((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onSuccess(gObj0);
        }
        
        public void onUnresolvableFailure(org.xms.g.common.api.Status param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onUnresolvableFailure(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))))");
            ((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onUnresolvableFailure(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))));
        }
        
        public void onFailure(org.xms.g.common.api.Status param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onFailure(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))))");
            ((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onFailure(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))));
        }
        
        public void onResult(XR param0) {
            com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onResult(gObj0)");
            ((com.google.android.gms.common.api.ResolvingResultCallbacks) this.getGInstance()).onResult(gObj0);
        }
    }
}