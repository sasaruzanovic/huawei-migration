package org.xms.g.common.api;

public final class PendingResults extends org.xms.g.utils.XObject {
    
    public PendingResults(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> canceledPendingResult() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.api.PendingResults.canceledPendingResult()");
        com.google.android.gms.common.api.PendingResult gReturn = com.google.android.gms.common.api.PendingResults.canceledPendingResult();
        return ((gReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static <XR extends org.xms.g.common.api.Result> org.xms.g.common.api.PendingResult<XR> canceledPendingResult(XR param0) {
        com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.api.PendingResults.canceledPendingResult(gObj0)");
        com.google.android.gms.common.api.PendingResult gReturn = com.google.android.gms.common.api.PendingResults.canceledPendingResult(gObj0);
        return ((gReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static <XR extends org.xms.g.common.api.Result> org.xms.g.common.api.OptionalPendingResult<XR> immediatePendingResult(XR param0) {
        com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.api.PendingResults.immediatePendingResult(gObj0)");
        com.google.android.gms.common.api.OptionalPendingResult gReturn = com.google.android.gms.common.api.PendingResults.immediatePendingResult(gObj0);
        return ((gReturn) == null ? null : (new org.xms.g.common.api.OptionalPendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static org.xms.g.common.api.PendingResult<org.xms.g.common.api.Status> immediatePendingResult(org.xms.g.common.api.Status param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.common.api.PendingResults.immediatePendingResult(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))))");
        com.google.android.gms.common.api.PendingResult gReturn = com.google.android.gms.common.api.PendingResults.immediatePendingResult(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))));
        return ((gReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static org.xms.g.common.api.PendingResults dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.PendingResults) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.PendingResults;
    }
}