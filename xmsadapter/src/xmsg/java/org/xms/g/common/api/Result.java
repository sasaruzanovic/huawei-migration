package org.xms.g.common.api;

public interface Result extends org.xms.g.utils.XInterface {
    
    public org.xms.g.common.api.Status getStatus();
    
    default java.lang.Object getZInstanceResult() {
        return getGInstanceResult();
    }
    
    default com.google.android.gms.common.api.Result getGInstanceResult() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.google.android.gms.common.api.Result) ((org.xms.g.utils.XGettable) this).getGInstance());
        }
        return new com.google.android.gms.common.api.Result() {
            
            public com.google.android.gms.common.api.Status getStatus() {
                org.xms.g.common.api.Status xResult = org.xms.g.common.api.Result.this.getStatus();
                return ((com.google.android.gms.common.api.Status) ((xResult) == null ? null : (xResult.getGInstance())));
            }
        };
    }
    
    public static org.xms.g.common.api.Result dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.Result) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.Result;
        }
        return param0 instanceof org.xms.g.common.api.Result;
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.common.api.Result {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.common.api.Status getStatus() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Result) this.getGInstance()).getStatus()");
            com.google.android.gms.common.api.Status gReturn = ((com.google.android.gms.common.api.Result) this.getGInstance()).getStatus();
            return ((gReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn))));
        }
    }
}