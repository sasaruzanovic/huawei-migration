package org.xms.g.common.api;

public final class Scope extends org.xms.g.utils.XObject implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.common.api.Scope createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.common.api.Scope gReturn = com.google.android.gms.common.api.Scope.CREATOR.createFromParcel(param0);
            return new org.xms.g.common.api.Scope(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.common.api.Scope[] newArray(int param0) {
            return new org.xms.g.common.api.Scope[param0];
        }
    };
    
    public Scope(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public Scope(java.lang.String param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new com.google.android.gms.common.api.Scope(param0));
    }
    
    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Scope) this.getGInstance()).equals(param0)");
        return ((com.google.android.gms.common.api.Scope) this.getGInstance()).equals(param0);
    }
    
    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Scope) this.getGInstance()).hashCode()");
        return ((com.google.android.gms.common.api.Scope) this.getGInstance()).hashCode();
    }
    
    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Scope) this.getGInstance()).toString()");
        return ((com.google.android.gms.common.api.Scope) this.getGInstance()).toString();
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Scope) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.common.api.Scope) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.common.api.Scope dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.Scope) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.Scope;
    }
}