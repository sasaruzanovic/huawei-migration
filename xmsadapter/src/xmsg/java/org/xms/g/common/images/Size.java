package org.xms.g.common.images;

public final class Size extends org.xms.g.utils.XObject {
    
    public Size(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public Size(int param0, int param1) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new com.google.android.gms.common.images.Size(param0, param1));
    }
    
    public final boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.images.Size) this.getGInstance()).equals(param0)");
        return ((com.google.android.gms.common.images.Size) this.getGInstance()).equals(param0);
    }
    
    public final int getHeight() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.images.Size) this.getGInstance()).getHeight()");
        return ((com.google.android.gms.common.images.Size) this.getGInstance()).getHeight();
    }
    
    public final int getWidth() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.images.Size) this.getGInstance()).getWidth()");
        return ((com.google.android.gms.common.images.Size) this.getGInstance()).getWidth();
    }
    
    public final int hashCode() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final org.xms.g.common.images.Size parseSize(java.lang.String param0) throws java.lang.NumberFormatException, java.lang.NullPointerException {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.images.Size) this.getGInstance()).toString()");
        return ((com.google.android.gms.common.images.Size) this.getGInstance()).toString();
    }
    
    public static org.xms.g.common.images.Size dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.images.Size) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.images.Size;
    }
}