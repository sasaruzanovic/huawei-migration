package org.xms.g.common.data;

public abstract class AbstractDataBuffer<XT> extends org.xms.g.utils.XObject implements org.xms.g.common.data.DataBuffer<XT> {
    
    public AbstractDataBuffer(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public void close() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).close()");
        ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).close();
    }
    
    public abstract XT get(int param0);
    
    public int getCount() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).getCount()");
        return ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).getCount();
    }
    
    public boolean isClosed() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).isClosed()");
        return ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).isClosed();
    }
    
    public java.util.Iterator<XT> iterator() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).iterator()");
        java.util.Iterator gReturn = ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).iterator();
        return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(gReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(param0));
            }
        }));
    }
    
    public void release() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).release()");
        ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).release();
    }
    
    public java.util.Iterator<XT> singleRefIterator() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).singleRefIterator()");
        java.util.Iterator gReturn = ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).singleRefIterator();
        return ((java.util.Iterator) org.xms.g.utils.Utils.transformIterator(gReturn, new org.xms.g.utils.Function<Object, XT>() {
            
            public XT apply(java.lang.Object param0) {
                return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(param0));
            }
        }));
    }
    
    public static org.xms.g.common.data.AbstractDataBuffer dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.common.data.AbstractDataBuffer) {
            return ((org.xms.g.common.data.AbstractDataBuffer) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.common.data.AbstractDataBuffer gReturn = ((com.google.android.gms.common.data.AbstractDataBuffer) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.common.data.AbstractDataBuffer.XImpl(new org.xms.g.utils.XBox(gReturn));
        }
        return ((org.xms.g.common.data.AbstractDataBuffer) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.data.AbstractDataBuffer;
    }
    
    public static class XImpl<XT> extends org.xms.g.common.data.AbstractDataBuffer<XT> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public XT get(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).get(param0)");
            java.lang.Object gmsObj = ((com.google.android.gms.common.data.AbstractDataBuffer) this.getGInstance()).get(param0);
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(gmsObj));
        }
    }
}