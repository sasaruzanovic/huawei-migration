package org.xms.g.common.api;

public class Response<XT extends org.xms.g.common.api.Result> extends org.xms.g.utils.XObject {
    private boolean wrapper = true;
    
    public Response(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    public Response() {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl());
        wrapper = false;
    }
    
    protected Response(XT param0) {
        super(((org.xms.g.utils.XBox) null));
        com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
        this.setGInstance(new GImpl(gObj0));
        wrapper = false;
    }
    
    public void setResult(XT param0) {
        if (wrapper) {
            com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.Response) this.getGInstance()).setResult(gObj0)");
            ((com.google.android.gms.common.api.Response) this.getGInstance()).setResult(gObj0);
        } else {
            com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.common.api.Response) this.getGInstance())).setResultCallSuper(gObj0)");
            ((GImpl) ((com.google.android.gms.common.api.Response) this.getGInstance())).setResultCallSuper(gObj0);
        }
    }
    
    protected XT getResult() {
        if (wrapper) {
            java.lang.Object[] params = new java.lang.Object[0];
            java.lang.Class[] types = new java.lang.Class[0];
            java.lang.Object gmsObj = org.xms.g.utils.Utils.invokeProtectMethod(((com.google.android.gms.common.api.Response) this.getGInstance()), com.google.android.gms.common.api.Response.class, "getResult", types, params);
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(gmsObj));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.common.api.Response) this.getGInstance())).getResultCallSuper()");
            java.lang.Object gmsObj = ((GImpl) ((com.google.android.gms.common.api.Response) this.getGInstance())).getResultCallSuper();
            return ((XT) org.xms.g.utils.Utils.getXmsObjectWithGmsObject(gmsObj));
        }
    }
    
    public static org.xms.g.common.api.Response dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.Response) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.Response;
    }
    
    private class GImpl<T extends com.google.android.gms.common.api.Result> extends com.google.android.gms.common.api.Response<T> {
        
        public void setResult(T param0) {
            java.lang.Object[] params = new java.lang.Object[1];
            java.lang.Class[] types = new java.lang.Class[1];
            params[0] = param0;
            types[0] = org.xms.g.common.api.Result.class;
            org.xms.g.utils.Utils.invokeMethod(org.xms.g.common.api.Response.this, "setResult", params, types, false);
        }
        
        protected T getResult() {
            XT xResult = ((XT) org.xms.g.common.api.Response.this.getResult());
            return ((T) org.xms.g.utils.Utils.getInstanceInInterface(xResult, false));
        }
        
        public void setResultCallSuper(T param0) {
            super.setResult(param0);
        }
        
        protected T getResultCallSuper() {
            return super.getResult();
        }
        
        public GImpl() {
            super();
        }
        
        protected GImpl(T param0) {
            super(param0);
        }
    }
}