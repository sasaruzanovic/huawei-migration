package org.xms.g.common.api;

public abstract class ResultTransform<XR extends org.xms.g.common.api.Result, XS extends org.xms.g.common.api.Result> extends org.xms.g.utils.XObject {
    private boolean wrapper = true;
    
    public ResultTransform(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    public ResultTransform() {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl());
        wrapper = false;
    }
    
    public final org.xms.g.common.api.PendingResult<XS> createFailedResult(org.xms.g.common.api.Status param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResultTransform) this.getGInstance()).createFailedResult(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))))");
        com.google.android.gms.common.api.PendingResult gReturn = ((com.google.android.gms.common.api.ResultTransform) this.getGInstance()).createFailedResult(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))));
        return ((gReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public org.xms.g.common.api.Status onFailure(org.xms.g.common.api.Status param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResultTransform) this.getGInstance()).onFailure(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))))");
            com.google.android.gms.common.api.Status gReturn = ((com.google.android.gms.common.api.ResultTransform) this.getGInstance()).onFailure(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.common.api.ResultTransform) this.getGInstance())).onFailureCallSuper(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))))");
            com.google.android.gms.common.api.Status gReturn = ((GImpl) ((com.google.android.gms.common.api.ResultTransform) this.getGInstance())).onFailureCallSuper(((com.google.android.gms.common.api.Status) ((param0) == null ? null : (param0.getGInstance()))));
            return ((gReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn))));
        }
    }
    
    public abstract org.xms.g.common.api.PendingResult<XS> onSuccess(XR param0);
    
    public static org.xms.g.common.api.ResultTransform dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.common.api.ResultTransform) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.common.api.ResultTransform;
    }
    
    private class GImpl<R extends com.google.android.gms.common.api.Result, S extends com.google.android.gms.common.api.Result> extends com.google.android.gms.common.api.ResultTransform<R, S> {
        
        public com.google.android.gms.common.api.Status onFailure(com.google.android.gms.common.api.Status param0) {
            org.xms.g.common.api.Status xResult = org.xms.g.common.api.ResultTransform.this.onFailure(((param0) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(param0)))));
            return ((com.google.android.gms.common.api.Status) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.common.api.Status onFailureCallSuper(com.google.android.gms.common.api.Status param0) {
            return super.onFailure(param0);
        }
        
        public com.google.android.gms.common.api.PendingResult<S> onSuccess(R param0) {
            java.lang.Object[] params = new java.lang.Object[1];
            java.lang.Class[] types = new java.lang.Class[1];
            params[0] = param0;
            types[0] = org.xms.g.common.api.Result.class;
            java.lang.Object result = org.xms.g.utils.Utils.invokeMethod(org.xms.g.common.api.ResultTransform.this, "onSuccess", params, types, false);
            return ((com.google.android.gms.common.api.PendingResult) org.xms.g.utils.Utils.handleInvokeBridgeReturnValue(result, false));
        }
        
        public GImpl() {
            super();
        }
    }
    
    public static class XImpl<XR extends org.xms.g.common.api.Result, XS extends org.xms.g.common.api.Result> extends org.xms.g.common.api.ResultTransform<XR, XS> {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public org.xms.g.common.api.PendingResult<XS> onSuccess(XR param0) {
            com.google.android.gms.common.api.Result gObj0 = ((com.google.android.gms.common.api.Result) org.xms.g.utils.Utils.getInstanceInInterface(param0, false));
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.common.api.ResultTransform) this.getGInstance()).onSuccess(gObj0)");
            com.google.android.gms.common.api.PendingResult gReturn = ((com.google.android.gms.common.api.ResultTransform) this.getGInstance()).onSuccess(gObj0);
            return ((gReturn) == null ? null : (new org.xms.g.common.api.PendingResult.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
    }
}