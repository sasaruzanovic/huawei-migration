package org.xms.g.iid;

public class InstanceIDListenerService extends android.app.Service implements org.xms.g.utils.XGettable {
    public java.lang.Object gInstance;
    
    public InstanceIDListenerService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setGInstance(param0.getGInstance());
    }
    
    public InstanceIDListenerService() {
    }
    
    public void onTokenRefresh() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public android.os.IBinder onBind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setGInstance(java.lang.Object param0) {
        this.gInstance = param0;
    }
    
    public java.lang.Object getGInstance() {
        return this.gInstance;
    }
    
    public static org.xms.g.iid.InstanceIDListenerService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}