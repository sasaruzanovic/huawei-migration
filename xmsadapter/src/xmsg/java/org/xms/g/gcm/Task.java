package org.xms.g.gcm;

public class Task extends org.xms.g.utils.XObject implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.gcm.Task createFromParcel(android.os.Parcel param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.Task[] newArray(int param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    };
    
    public Task(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static int getEXTRAS_LIMIT_BYTES() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getNETWORK_STATE_ANY() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getNETWORK_STATE_CONNECTED() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getNETWORK_STATE_UNMETERED() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static long getUNINITIALIZED() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public android.os.Bundle getExtras() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public int getRequiredNetwork() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public boolean getRequiresCharging() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getServiceName() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getTag() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public boolean isPersisted() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public boolean isUpdateCurrent() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.gcm.Task dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public abstract static class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public android.os.Bundle getExtras() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public java.lang.String getGcmTaskService() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public boolean getIsPersisted() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public int getRequiredNetworkState() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public boolean getRequiresCharging() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public java.lang.String getTag() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public boolean getUpdateCurrent() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public abstract org.xms.g.gcm.Task build();
        
        public abstract org.xms.g.gcm.Task.Builder setExtras(android.os.Bundle param0);
        
        public abstract org.xms.g.gcm.Task.Builder setPersisted(boolean param0);
        
        public abstract org.xms.g.gcm.Task.Builder setRequiredNetwork(int param0);
        
        public abstract org.xms.g.gcm.Task.Builder setRequiresCharging(boolean param0);
        
        public abstract org.xms.g.gcm.Task.Builder setService(java.lang.Class<? extends org.xms.g.gcm.GcmTaskService> param0);
        
        public abstract org.xms.g.gcm.Task.Builder setTag(java.lang.String param0);
        
        public abstract org.xms.g.gcm.Task.Builder setUpdateCurrent(boolean param0);
        
        protected void checkConditions() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.gcm.Task.Builder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static class XImpl extends org.xms.g.gcm.Task.Builder {
            
            public XImpl(org.xms.g.utils.XBox param0) {
                super(param0);
            }
            
            public org.xms.g.gcm.Task build() {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setExtras(android.os.Bundle param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setPersisted(boolean param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setRequiredNetwork(int param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setRequiresCharging(boolean param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setService(java.lang.Class<? extends org.xms.g.gcm.GcmTaskService> param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setTag(java.lang.String param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
            
            public org.xms.g.gcm.Task.Builder setUpdateCurrent(boolean param0) {
                throw new java.lang.RuntimeException("Not Supported");
            }
        }
    }
}