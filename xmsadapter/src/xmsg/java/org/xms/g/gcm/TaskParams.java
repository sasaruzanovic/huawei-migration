package org.xms.g.gcm;

public class TaskParams extends org.xms.g.utils.XObject {
    
    public TaskParams(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public TaskParams(java.lang.String param0) {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public TaskParams(java.lang.String param0, android.os.Bundle param1) {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public TaskParams(java.lang.String param0, android.os.Bundle param1, java.util.List<android.net.Uri> param2) {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public android.os.Bundle getExtras() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getTag() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.gcm.TaskParams dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}