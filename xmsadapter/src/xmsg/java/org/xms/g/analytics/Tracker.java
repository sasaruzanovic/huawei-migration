package org.xms.g.analytics;

public class Tracker extends org.xms.g.utils.XObject {
    
    public Tracker(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public void enableAdvertisingIdCollection(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void enableAutoActivityTracking(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void enableExceptionReporting(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String get(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void send(java.util.Map<java.lang.String, java.lang.String> param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void set(java.lang.String param0, java.lang.String param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setAnonymizeIp(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setAppId(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setAppInstallerId(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setAppName(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setAppVersion(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setCampaignParamsOnNextHit(android.net.Uri param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setClientId(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setEncoding(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setHostname(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setLanguage(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setLocation(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setPage(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setReferrer(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setSampleRate(double param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setScreenColors(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setScreenName(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setScreenResolution(int param0, int param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setSessionTimeout(long param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setTitle(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setUseSecure(boolean param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setViewportSize(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.Tracker dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}