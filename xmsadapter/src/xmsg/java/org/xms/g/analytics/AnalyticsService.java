package org.xms.g.analytics;

public class AnalyticsService extends android.app.Service implements org.xms.g.utils.XGettable {
    public java.lang.Object gInstance;
    
    public AnalyticsService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setGInstance(param0.getGInstance());
    }
    
    public final boolean callServiceStopSelfResult(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final android.os.IBinder onBind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setGInstance(java.lang.Object param0) {
        this.gInstance = param0;
    }
    
    public java.lang.Object getGInstance() {
        return this.gInstance;
    }
    
    public static org.xms.g.analytics.AnalyticsService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}