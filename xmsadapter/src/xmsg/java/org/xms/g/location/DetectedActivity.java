package org.xms.g.location;

/**
 * The detected activity of the device with an an associated confidence. See ActivityRecognitionApi for details on how to obtain a DetectedActivity..<br/>
 * Wrapper class for com.google.android.gms.location.DetectedActivity, but only the GMS API are provided.</br>
 * com.google.android.gms.location.DetectedActivity: The detected activity of the device with an an associated confidence. See ActivityRecognitionApi for details on how to obtain a DetectedActivity.</br>
 */
public class DetectedActivity extends org.xms.g.utils.XObject implements android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.DetectedActivity.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-creatordetectedactivity-creator">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-creatordetectedactivity-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.DetectedActivity createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.DetectedActivity gReturn = com.google.android.gms.location.DetectedActivity.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.DetectedActivity(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.DetectedActivity[] newArray(int param0) {
            return new org.xms.g.location.DetectedActivity[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.DetectedActivity.DetectedActivity(org.xms.g.utils.XBox) constructor of DetectedActivity with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public DetectedActivity(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.DetectedActivity(int,int) Constructs a DetectedActivity.<br/>
     * com.google.android.gms.location.DetectedActivity(int,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-detectedactivity-int-activitytype,-int-confidence">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-detectedactivity-int-activitytype,-int-confidence</a><br/>
     * 
     * @param  param0 The activity that was detected
     * @param  param1 Value from 0 to 100 indicating how likely it is that the user is performing this activity
     */
    public DetectedActivity(int param0, int param1) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl(param0, param1));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getIN_VEHICLE() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.IN_VEHICLE: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-in_vehicle">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-in_vehicle</a><br/>
     * 
     * @return The constant value.device is in a vehicle, such as a car
     */
    public static int getIN_VEHICLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.IN_VEHICLE");
        return com.google.android.gms.location.DetectedActivity.IN_VEHICLE;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getON_BICYCLE() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.ON_BICYCLE: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-on_bicycle">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-on_bicycle</a><br/>
     * 
     * @return The constant value.The device is on a bicycle
     */
    public static int getON_BICYCLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.ON_BICYCLE");
        return com.google.android.gms.location.DetectedActivity.ON_BICYCLE;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getON_FOOT() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.ON_FOOT: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-on_foot">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-on_foot</a><br/>
     * 
     * @return The constant value.The device is on a user who is walking or running
     */
    public static int getON_FOOT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.ON_FOOT");
        return com.google.android.gms.location.DetectedActivity.ON_FOOT;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getRUNNING() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.RUNNING: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-running">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-running</a><br/>
     * 
     * @return The constant value.The device is on a user who is running. This is a sub-activity of ON_FOOT
     */
    public static int getRUNNING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.RUNNING");
        return com.google.android.gms.location.DetectedActivity.RUNNING;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getSTILL() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.STILL: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-still">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-still</a><br/>
     * 
     * @return The constant value.The device is still (not moving)
     */
    public static int getSTILL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.STILL");
        return com.google.android.gms.location.DetectedActivity.STILL;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getTILTING() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.TILTING: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-tilting">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-tilting</a><br/>
     * 
     * @return The constant value.The device angle relative to gravity changed significantly
     */
    public static int getTILTING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.TILTING");
        return com.google.android.gms.location.DetectedActivity.TILTING;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getUNKNOWN() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.UNKNOWN: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-unknown">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-unknown</a><br/>
     * 
     * @return The constant value.Unable to detect the current activity
     */
    public static int getUNKNOWN() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.UNKNOWN");
        return com.google.android.gms.location.DetectedActivity.UNKNOWN;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getWALKING() Return the constant value.<br/>
     * com.google.android.gms.location.DetectedActivity.WALKING: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-walking">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-static-final-int-walking</a><br/>
     * 
     * @return The constant value.The device is on a user who is walking
     */
    public static int getWALKING() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.DetectedActivity.WALKING");
        return com.google.android.gms.location.DetectedActivity.WALKING;
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getConfidence() Returns a value from 0 to 100 indicating the likelihood that the user is performing this activity.<br/>
     * com.google.android.gms.location.DetectedActivity.getConfidence(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-int-getconfidence">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-int-getconfidence</a><br/>
     * 
     * @return A value from 0 to 100 indicating the likelihood that the user is performing this activity
     */
    public int getConfidence() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.DetectedActivity) this.getGInstance()).getConfidence()");
            return ((com.google.android.gms.location.DetectedActivity) this.getGInstance()).getConfidence();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).getConfidenceCallSuper()");
            return ((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).getConfidenceCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.DetectedActivity.getType() Returns the type of activity that was detected.<br/>
     * com.google.android.gms.location.DetectedActivity.getType(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-int-gettype">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-int-gettype</a><br/>
     * 
     * @return Activity type
     */
    public int getType() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.DetectedActivity) this.getGInstance()).getType()");
            return ((com.google.android.gms.location.DetectedActivity) this.getGInstance()).getType();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).getTypeCallSuper()");
            return ((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).getTypeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.DetectedActivity.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.google.android.gms.location.DetectedActivity.toString(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-string-tostring">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-string-tostring</a><br/>
     * 
     * @return A character string after being converted
     */
    public java.lang.String toString() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.DetectedActivity) this.getGInstance()).toString()");
            return ((com.google.android.gms.location.DetectedActivity) this.getGInstance()).toString();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).toStringCallSuper()");
            return ((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).toStringCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.DetectedActivity.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.DetectedActivity.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-void-writetoparcel-parcel-out,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity#public-void-writetoparcel-parcel-out,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.DetectedActivity) this.getGInstance()).writeToParcel(param0, param1)");
            ((com.google.android.gms.location.DetectedActivity) this.getGInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).writeToParcelCallSuper(param0, param1)");
            ((GImpl) ((com.google.android.gms.location.DetectedActivity) this.getGInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.DetectedActivity.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.DetectedActivity.<br/>
     * 
     * @param  param0 The input object
     * @return Casted DetectedActivity object
     */
    public static org.xms.g.location.DetectedActivity dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.DetectedActivity) param0);
    }
    
    /**
     * org.xms.g.location.DetectedActivity.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.DetectedActivity;
    }
    
    private class GImpl extends com.google.android.gms.location.DetectedActivity {
        
        public int getConfidence() {
            return org.xms.g.location.DetectedActivity.this.getConfidence();
        }
        
        public int getType() {
            return org.xms.g.location.DetectedActivity.this.getType();
        }
        
        public java.lang.String toString() {
            return org.xms.g.location.DetectedActivity.this.toString();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.DetectedActivity.this.writeToParcel(param0, param1);
        }
        
        public int getConfidenceCallSuper() {
            return super.getConfidence();
        }
        
        public int getTypeCallSuper() {
            return super.getType();
        }
        
        public java.lang.String toStringCallSuper() {
            return super.toString();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public GImpl(int param0, int param1) {
            super(param0, param1);
        }
    }
}