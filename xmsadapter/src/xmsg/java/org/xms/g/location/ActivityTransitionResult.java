package org.xms.g.location;

/**
 * Represents the result of activity transitions..<br/>
 * Wrapper class for com.google.android.gms.location.ActivityTransitionResult, but only the GMS API are provided.</br>
 * com.google.android.gms.location.ActivityTransitionResult: Represents the result of activity transitions.</br>
 */
public class ActivityTransitionResult extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.ActivityTransitionResult.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-static-final-creatoractivitytransitionresult-creator">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-static-final-creatoractivitytransitionresult-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.ActivityTransitionResult createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.ActivityTransitionResult gReturn = com.google.android.gms.location.ActivityTransitionResult.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.ActivityTransitionResult(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.ActivityTransitionResult[] newArray(int param0) {
            return new org.xms.g.location.ActivityTransitionResult[param0];
        }
    };
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityTransitionResult.ActivityTransitionResult(org.xms.g.utils.XBox) Constructor of ActivityTransitionResult with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityTransitionResult(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.ActivityTransitionResult(java.util.List<org.xms.g.location.ActivityTransitionEvent>) Constructs a result by specifying a list of transition events.<br/>
     * com.google.android.gms.location.ActivityTransitionResult(java.util.List<com.google.android.gms.location.ActivityTransitionEvent>): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-activitytransitionresult-listactivitytransitionevent-transitionevents">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-activitytransitionresult-listactivitytransitionevent-transitionevents</a><br/>
     * 
     * @param  param0 The transition events
     */
    public ActivityTransitionResult(java.util.List<org.xms.g.location.ActivityTransitionEvent> param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, false))));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.google.android.gms.location.ActivityTransitionResult.equals(java.lang.Object): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-boolean-equals-object-o">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-boolean-equals-object-o</a><br/>
     * 
     * @param  param0 The other instance
     * @return True if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).equals(param0)");
            return ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).equals(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).equalsCallSuper(param0)");
            return ((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).equalsCallSuper(param0);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.extractResult(android.content.Intent) Extracts the ActivityTransitionResult from the given Intent.<br/>
     * com.google.android.gms.location.ActivityTransitionResult.extractResult(android.content.Intent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-static-activitytransitionresult-extractresult-intent-intent">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-static-activitytransitionresult-extractresult-intent-intent</a><br/>
     * 
     * @param  param0 The Intent to extract the result from
     * @return The ActivityTransitionResult included in the given intent or return null if no such result is found in the given intent
     */
    public static org.xms.g.location.ActivityTransitionResult extractResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.ActivityTransitionResult.extractResult(param0)");
        com.google.android.gms.location.ActivityTransitionResult gReturn = com.google.android.gms.location.ActivityTransitionResult.extractResult(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.ActivityTransitionResult(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.getTransitionEvents() Gets all the activity transition events in this result. The events are in ascending order of time, and may include events in the past.<br/>
     * com.google.android.gms.location.ActivityTransitionResult.getTransitionEvents(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-listactivitytransitionevent-gettransitionevents">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-listactivitytransitionevent-gettransitionevents</a><br/>
     * 
     * @return List of activity transition events
     */
    public java.util.List<org.xms.g.location.ActivityTransitionEvent> getTransitionEvents() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).getTransitionEvents()");
            java.util.List gReturn = ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).getTransitionEvents();
            return ((java.util.List) org.xms.g.utils.Utils.mapCollection(gReturn, new org.xms.g.utils.Function<com.google.android.gms.location.ActivityTransitionEvent, org.xms.g.location.ActivityTransitionEvent>() {
                
                public org.xms.g.location.ActivityTransitionEvent apply(com.google.android.gms.location.ActivityTransitionEvent param0) {
                    return new org.xms.g.location.ActivityTransitionEvent(new org.xms.g.utils.XBox(param0));
                }
            }));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).getTransitionEventsCallSuper()");
            java.util.List gReturn = ((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).getTransitionEventsCallSuper();
            return ((java.util.List) org.xms.g.utils.Utils.mapCollection(gReturn, new org.xms.g.utils.Function<com.google.android.gms.location.ActivityTransitionEvent, org.xms.g.location.ActivityTransitionEvent>() {
                
                public org.xms.g.location.ActivityTransitionEvent apply(com.google.android.gms.location.ActivityTransitionEvent param0) {
                    return new org.xms.g.location.ActivityTransitionEvent(new org.xms.g.utils.XBox(param0));
                }
            }));
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.hasResult(android.content.Intent) Checks if the intent contains an ActivityTransitionResult.<br/>
     * com.google.android.gms.location.ActivityTransitionResult.hasResult(android.content.Intent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-static-boolean-hasresult-intent-intent">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-static-boolean-hasresult-intent-intent</a><br/>
     * 
     * @param  param0 Intent instance
     * @return True if the intent contains ActivityTransitionResult; false otherwise
     */
    public static boolean hasResult(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.ActivityTransitionResult.hasResult(param0)");
        return com.google.android.gms.location.ActivityTransitionResult.hasResult(param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.google.android.gms.location.ActivityTransitionResult.hashCode(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-int-hashcode">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-int-hashcode</a><br/>
     * 
     * @return A hash code value
     */
    public int hashCode() {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).hashCode()");
            return ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).hashCode();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).hashCodeCallSuper()");
            return ((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).hashCodeCallSuper();
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.ActivityTransitionResult.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-void-writetoparcel-parcel-dest,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityTransitionResult#public-void-writetoparcel-parcel-dest,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).writeToParcel(param0, param1)");
            ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).writeToParcelCallSuper(param0, param1)");
            ((GImpl) ((com.google.android.gms.location.ActivityTransitionResult) this.getGInstance())).writeToParcelCallSuper(param0, param1);
        }
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityTransitionResult.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityTransitionResult object
     */
    public static org.xms.g.location.ActivityTransitionResult dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityTransitionResult) param0);
    }
    
    /**
     * org.xms.g.location.ActivityTransitionResult.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.ActivityTransitionResult;
    }
    
    private class GImpl extends com.google.android.gms.location.ActivityTransitionResult {
        
        public boolean equals(java.lang.Object param0) {
            return org.xms.g.location.ActivityTransitionResult.this.equals(param0);
        }
        
        public java.util.List<com.google.android.gms.location.ActivityTransitionEvent> getTransitionEvents() {
            return ((java.util.List) org.xms.g.utils.Utils.mapList2GH(org.xms.g.location.ActivityTransitionResult.this.getTransitionEvents(), false));
        }
        
        public int hashCode() {
            return org.xms.g.location.ActivityTransitionResult.this.hashCode();
        }
        
        public void writeToParcel(android.os.Parcel param0, int param1) {
            org.xms.g.location.ActivityTransitionResult.this.writeToParcel(param0, param1);
        }
        
        public boolean equalsCallSuper(java.lang.Object param0) {
            return super.equals(param0);
        }
        
        public java.util.List<com.google.android.gms.location.ActivityTransitionEvent> getTransitionEventsCallSuper() {
            return super.getTransitionEvents();
        }
        
        public int hashCodeCallSuper() {
            return super.hashCode();
        }
        
        public void writeToParcelCallSuper(android.os.Parcel param0, int param1) {
            super.writeToParcel(param0, param1);
        }
        
        public GImpl(java.util.List<com.google.android.gms.location.ActivityTransitionEvent> param0) {
            super(param0);
        }
    }
}