package org.xms.g.location;

/**
 * The main entry point for activity recognition integration..<br/>
 * Wrapper class for com.google.android.gms.location.ActivityRecognition, but only the GMS API are provided.</br>
 * com.google.android.gms.location.ActivityRecognition: The main entry point for activity recognition integration.</br>
 */
public class ActivityRecognition extends org.xms.g.utils.XObject {
    
    /**
     * org.xms.g.location.ActivityRecognition.ActivityRecognition(org.xms.g.utils.XBox) Constructor of ActivityRecognition with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityRecognition(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getCLIENT_NAME() Return the value of CLIENT_NAME.<br/>
     * com.google.android.gms.location.ActivityRecognition.CLIENT_NAME: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-final-string-client_name">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-final-string-client_name</a><br/>
     * 
     * @return Constant Value:"activity_recognition"
     */
    public static java.lang.String getCLIENT_NAME() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getAPI() Return the token that pass to addApi(Api) to enable ContextServices.<br/>
     * com.google.android.gms.location.ActivityRecognition.API: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-final-apiapi.apioptions.nooptions-api">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-final-apiapi.apioptions.nooptions-api</a><br/>
     * 
     * @return Token to pass to addApi(Api) to enable ContextServices
     */
    public static org.xms.g.common.api.Api getAPI() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getActivityRecognitionApi() Return the entry point to the activity recognition APIs.<br/>
     * com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-final-activityrecognitionapi-activityrecognitionapi">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-final-activityrecognitionapi-activityrecognitionapi</a><br/>
     * 
     * @return Entry point to the activity recognition APIs
     */
    public static org.xms.g.location.ActivityRecognitionApi getActivityRecognitionApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getClient(android.app.Activity) Create a new instance of Client for use in an Activity.<br/>
     * com.google.android.gms.location.ActivityRecognition.getClient(android.app.Activity): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-activityrecognitionclient-getclient-activity-activity">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-activityrecognitionclient-getclient-activity-activity</a><br/>
     * 
     * @param  param0 A specific Activity
     * @return ActivityRecognitionClient instance obtained based on Activity
     */
    public static org.xms.g.location.ActivityRecognitionClient getClient(android.app.Activity param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.ActivityRecognition.getClient(param0)");
        com.google.android.gms.location.ActivityRecognitionClient gReturn = com.google.android.gms.location.ActivityRecognition.getClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.ActivityRecognitionClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.getClient(android.content.Context) Create a new instance of ActivityRecognitionClient for use in a non-activity Context.<br/>
     * com.google.android.gms.location.ActivityRecognition.getClient(android.content.Context): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-activityrecognitionclient-getclient-context-context">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognition#public-static-activityrecognitionclient-getclient-context-context</a><br/>
     * 
     * @param  param0 Context instance
     * @return ActivityRecognitionClient instance obtained based on Context
     */
    public static org.xms.g.location.ActivityRecognitionClient getClient(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.ActivityRecognition.getClient(param0)");
        com.google.android.gms.location.ActivityRecognitionClient gReturn = com.google.android.gms.location.ActivityRecognition.getClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.ActivityRecognitionClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityRecognition.<br/>
     * 
     * @param  param0 The input object
     * @return casted ActivityRecognition object
     */
    public static org.xms.g.location.ActivityRecognition dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.ActivityRecognition) param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognition.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.ActivityRecognition;
    }
}