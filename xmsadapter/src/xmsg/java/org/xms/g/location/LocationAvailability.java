package org.xms.g.location;

/**
 * Status on the device location availability..<br/>
 * Wrapper class for com.google.android.gms.location.LocationAvailability, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationAvailability: Status on the availability of location data.</br>
 */
public final class LocationAvailability extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.LocationAvailability.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-static-final-creatorlocationavailability-creator">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-static-final-creatorlocationavailability-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationAvailability createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.LocationAvailability gReturn = com.google.android.gms.location.LocationAvailability.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationAvailability(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.LocationAvailability[] newArray(int param0) {
            return new org.xms.g.location.LocationAvailability[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationAvailability.LocationAvailability(org.xms.g.utils.XBox) constructor of LocationAvailability with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationAvailability(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationAvailability.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.google.android.gms.location.LocationAvailability.equals(java.lang.Object): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-boolean-equals-object-o">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-boolean-equals-object-o</a><br/>
     * 
     * @param  param0 the other instance
     * @return true if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationAvailability) this.getGInstance()).equals(param0)");
        return ((com.google.android.gms.location.LocationAvailability) this.getGInstance()).equals(param0);
    }
    
    /**
     * org.xms.g.location.LocationAvailability.extractLocationAvailability(android.content.Intent) Extracts the LocationAvailability from an Intent.<br/>
     * com.google.android.gms.location.LocationAvailability.extractLocationAvailability(android.content.Intent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-static-locationavailability-extractlocationavailability-intent-intent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-static-locationavailability-extractlocationavailability-intent-intent</a><br/>
     * 
     * @param  param0 Intent instance
     * @return a LocationAvailability, or null if the Intent doesn't contain this data
     */
    public static org.xms.g.location.LocationAvailability extractLocationAvailability(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationAvailability.extractLocationAvailability(param0)");
        com.google.android.gms.location.LocationAvailability gReturn = com.google.android.gms.location.LocationAvailability.extractLocationAvailability(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationAvailability(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationAvailability.hasLocationAvailability(android.content.Intent) Returns true if an Intent contains a LocationAvailability.This is a utility function that can be called from inside an intent receiver to make sure the received intent contains location availability data.<br/>
     * com.google.android.gms.location.LocationAvailability.hasLocationAvailability(android.content.Intent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-static-boolean-haslocationavailability-intent-intent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-static-boolean-haslocationavailability-intent-intent</a><br/>
     * 
     * @param  param0 Intent instance
     * @return true if the intent contains a LocationAvailability, false otherwise
     */
    public static boolean hasLocationAvailability(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationAvailability.hasLocationAvailability(param0)");
        return com.google.android.gms.location.LocationAvailability.hasLocationAvailability(param0);
    }
    
    /**
     * org.xms.g.location.LocationAvailability.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.google.android.gms.location.LocationAvailability.hashCode(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-int-hashcode">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-int-hashcode</a><br/>
     * 
     * @return A hash code value
     */
    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationAvailability) this.getGInstance()).hashCode()");
        return ((com.google.android.gms.location.LocationAvailability) this.getGInstance()).hashCode();
    }
    
    /**
     * org.xms.g.location.LocationAvailability.isLocationAvailable() Returns true if the device location is known and reasonably up to date within the hints requested by the active LocationRequests.<br/>
     * com.google.android.gms.location.LocationAvailability.isLocationAvailable(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-boolean-islocationavailable">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-boolean-islocationavailable</a><br/>
     * 
     * @return true if the device location is known and reasonably up to date within the hints requested by the active LocationRequests
     */
    public final boolean isLocationAvailable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationAvailability) this.getGInstance()).isLocationAvailable()");
        return ((com.google.android.gms.location.LocationAvailability) this.getGInstance()).isLocationAvailable();
    }
    
    /**
     * org.xms.g.location.LocationAvailability.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.google.android.gms.location.LocationAvailability.toString(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-string-tostring">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-string-tostring</a><br/>
     * 
     * @return A character string after being converted
     */
    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationAvailability) this.getGInstance()).toString()");
        return ((com.google.android.gms.location.LocationAvailability) this.getGInstance()).toString();
    }
    
    /**
     * org.xms.g.location.LocationAvailability.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.LocationAvailability.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-void-writetoparcel-parcel-parcel,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/LocationAvailability#public-void-writetoparcel-parcel-parcel,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationAvailability) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.location.LocationAvailability) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * org.xms.g.location.LocationAvailability.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationAvailability.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationAvailability object
     */
    public static org.xms.g.location.LocationAvailability dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationAvailability) param0);
    }
    
    /**
     * org.xms.g.location.LocationAvailability.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationAvailability;
    }
}