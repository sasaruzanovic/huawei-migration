package org.xms.g.location;

/**
 * Specifies the types of location services the client is interested in using..<br/>
 * Wrapper class for com.google.android.gms.location.LocationSettingsRequest, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationSettingsRequest: Specifies the types of location services the client is interested in using.</br>
 */
public final class LocationSettingsRequest extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.LocationSettingsRequest.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsRequest#public-static-final-creatorlocationsettingsrequest-creator">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsRequest#public-static-final-creatorlocationsettingsrequest-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationSettingsRequest createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.LocationSettingsRequest gReturn = com.google.android.gms.location.LocationSettingsRequest.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationSettingsRequest(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.LocationSettingsRequest[] newArray(int param0) {
            return new org.xms.g.location.LocationSettingsRequest[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationSettingsRequest.LocationSettingsRequest(org.xms.g.utils.XBox) constructor of LocationSettingsRequest with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsRequest(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsRequest.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.LocationSettingsRequest.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsRequest#public-void-writetoparcel-parcel-dest,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsRequest#public-void-writetoparcel-parcel-dest,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsRequest) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.location.LocationSettingsRequest) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * org.xms.g.location.LocationSettingsRequest.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsRequest.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsRequest object
     */
    public static org.xms.g.location.LocationSettingsRequest dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationSettingsRequest) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsRequest.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationSettingsRequest;
    }
    
/**
 * .<br/>
 * Wrapper class for , but only the GMS API are provided.</br>
 * : </br>
 */
    public static final class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
            this.setGInstance(new com.google.android.gms.location.LocationSettingsRequest.Builder());
        }
        
        public final org.xms.g.location.LocationSettingsRequest.Builder addAllLocationRequests(java.util.Collection<org.xms.g.location.LocationRequest> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).addAllLocationRequests(org.xms.g.utils.Utils.mapCollection2GH(param0, false))");
            com.google.android.gms.location.LocationSettingsRequest.Builder gReturn = ((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).addAllLocationRequests(org.xms.g.utils.Utils.mapCollection2GH(param0, false));
            return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.g.location.LocationSettingsRequest.Builder addLocationRequest(org.xms.g.location.LocationRequest param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).addLocationRequest(((com.google.android.gms.location.LocationRequest) ((param0) == null ? null : (param0.getGInstance()))))");
            com.google.android.gms.location.LocationSettingsRequest.Builder gReturn = ((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).addLocationRequest(((com.google.android.gms.location.LocationRequest) ((param0) == null ? null : (param0.getGInstance()))));
            return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.LocationSettingsRequest build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).build()");
            com.google.android.gms.location.LocationSettingsRequest gReturn = ((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).build();
            return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsRequest(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.LocationSettingsRequest.Builder setAlwaysShow(boolean param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).setAlwaysShow(param0)");
            com.google.android.gms.location.LocationSettingsRequest.Builder gReturn = ((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).setAlwaysShow(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.LocationSettingsRequest.Builder setNeedBle(boolean param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).setNeedBle(param0)");
            com.google.android.gms.location.LocationSettingsRequest.Builder gReturn = ((com.google.android.gms.location.LocationSettingsRequest.Builder) this.getGInstance()).setNeedBle(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public static org.xms.g.location.LocationSettingsRequest.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.location.LocationSettingsRequest.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationSettingsRequest.Builder;
        }
    }
}