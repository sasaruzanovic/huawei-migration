package org.xms.g.location;

/**
 * Specifies the list of geofences to be monitored and how the geofence notifications should be reported..<br/>
 * Wrapper class for com.google.android.gms.location.GeofencingRequest, but only the GMS API are provided.</br>
 * com.google.android.gms.location.GeofencingRequest: Specifies the list of geofences to be monitored and how the geofence notifications should be reported.</br>
 */
public class GeofencingRequest extends org.xms.g.utils.XObject {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.GeofencingRequest.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-creatorgeofencingrequest-creator">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-creatorgeofencingrequest-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.GeofencingRequest createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.GeofencingRequest gReturn = com.google.android.gms.location.GeofencingRequest.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.GeofencingRequest(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.GeofencingRequest[] newArray(int param0) {
            return new org.xms.g.location.GeofencingRequest[param0];
        }
    };
    
    /**
     * org.xms.g.location.GeofencingRequest.GeofencingRequest(org.xms.g.utils.XBox) constructor of GeofencingRequest with XBox<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public GeofencingRequest(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getINITIAL_TRIGGER_DWELL() return the constant value.<br/>
     * com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_DWELL: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-int-initial_trigger_dwell">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-int-initial_trigger_dwell</a><br/>
     * 
     * @return Constant Value.A flag indicating that geofencing service should trigger GEOFENCE_TRANSITION_DWELL notification at the moment when the geofence is added and if the device is already inside that geofence for some time.
     */
    public static int getINITIAL_TRIGGER_DWELL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_DWELL");
        return com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_DWELL;
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getINITIAL_TRIGGER_ENTER() return the constant value.<br/>
     * com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_ENTER: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-int-initial_trigger_enter">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-int-initial_trigger_enter</a><br/>
     * 
     * @return Constant Value.A flag indicating that geofencing service should trigger GEOFENCE_TRANSITION_ENTER notification at the moment when the geofence is added and if the device is already inside that geofence
     */
    public static int getINITIAL_TRIGGER_ENTER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_ENTER");
        return com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_ENTER;
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getINITIAL_TRIGGER_EXIT() return the constant value.<br/>
     * com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_EXIT: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-int-initial_trigger_exit">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-static-final-int-initial_trigger_exit</a><br/>
     * 
     * @return Constant Value.A flag indicating that geofencing service should trigger GEOFENCE_TRANSITION_EXIT notification at the moment when the geofence is added and if the device is already outside that geofence
     */
    public static int getINITIAL_TRIGGER_EXIT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_EXIT");
        return com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_EXIT;
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getGeofences() Gets the list of geofences to be monitored.<br/>
     * com.google.android.gms.location.GeofencingRequest.getGeofences(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-listgeofence-getgeofences">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-listgeofence-getgeofences</a><br/>
     * 
     * @return the list of geofences to be monitored
     */
    public java.util.List<org.xms.g.location.Geofence> getGeofences() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).getGeofences()");
        java.util.List gReturn = ((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).getGeofences();
        return ((java.util.List) org.xms.g.utils.Utils.mapCollection(gReturn, new org.xms.g.utils.Function<com.google.android.gms.location.Geofence, org.xms.g.location.Geofence>() {
            
            public org.xms.g.location.Geofence apply(com.google.android.gms.location.Geofence param0) {
                return new org.xms.g.location.Geofence.XImpl(new org.xms.g.utils.XBox(param0));
            }
        }));
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.getInitialTrigger() Gets the triggering behavior at the moment when the geofences are added.<br/>
     * com.google.android.gms.location.GeofencingRequest.getInitialTrigger(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-int-getinitialtrigger">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-int-getinitialtrigger</a><br/>
     * 
     * @return the triggering behavior at the moment when the geofences are added. Its a bit-wise of INITIAL_TRIGGER_ENTER and INITIAL_TRIGGER_EXIT
     */
    public int getInitialTrigger() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).getInitialTrigger()");
        return ((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).getInitialTrigger();
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.google.android.gms.location.GeofencingRequest.toString(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-string-tostring">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-string-tostring</a><br/>
     * 
     * @return A character string after being converted.
     */
    public java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).toString()");
        return ((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).toString();
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.GeofencingRequest.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-void-writetoparcel-parcel-dest,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingRequest#public-void-writetoparcel-parcel-dest,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.location.GeofencingRequest) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.GeofencingRequest.<br/>
     * 
     * @param  param0 the input object
     * @return casted GeofencingRequest object
     */
    public static org.xms.g.location.GeofencingRequest dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.GeofencingRequest) param0);
    }
    
    /**
     * org.xms.g.location.GeofencingRequest.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.GeofencingRequest;
    }
    
/**
 * .<br/>
 * Wrapper class for , but only the GMS API are provided.</br>
 * : </br>
 */
    public static final class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
            this.setGInstance(new com.google.android.gms.location.GeofencingRequest.Builder());
        }
        
        public org.xms.g.location.GeofencingRequest.Builder addGeofence(org.xms.g.location.Geofence param0) throws java.lang.NullPointerException, java.lang.IllegalArgumentException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).addGeofence(((param0) == null ? null : (param0.getGInstanceGeofence())))");
            com.google.android.gms.location.GeofencingRequest.Builder gReturn = ((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).addGeofence(((param0) == null ? null : (param0.getGInstanceGeofence())));
            return ((gReturn) == null ? null : (new org.xms.g.location.GeofencingRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.GeofencingRequest.Builder addGeofences(java.util.List<org.xms.g.location.Geofence> param0) throws java.lang.IllegalArgumentException {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).addGeofences(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, false)))");
            com.google.android.gms.location.GeofencingRequest.Builder gReturn = ((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).addGeofences(((java.util.List) org.xms.g.utils.Utils.mapList2GH(param0, false)));
            return ((gReturn) == null ? null : (new org.xms.g.location.GeofencingRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.GeofencingRequest build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).build()");
            com.google.android.gms.location.GeofencingRequest gReturn = ((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).build();
            return ((gReturn) == null ? null : (new org.xms.g.location.GeofencingRequest(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.GeofencingRequest.Builder setInitialTrigger(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).setInitialTrigger(param0)");
            com.google.android.gms.location.GeofencingRequest.Builder gReturn = ((com.google.android.gms.location.GeofencingRequest.Builder) this.getGInstance()).setInitialTrigger(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.GeofencingRequest.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public static org.xms.g.location.GeofencingRequest.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.location.GeofencingRequest.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.GeofencingRequest.Builder;
        }
    }
}