package org.xms.g.location;

/**
 * Status codes that can be returned to listeners to indicate the success or failure of an operation..<br/>
 * Wrapper class for com.google.android.gms.location.LocationStatusCodes, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationStatusCodes: Status codes that can be returned to listeners to indicate the success or failure of an operation.</br>
 */
public final class LocationStatusCodes extends org.xms.g.utils.XObject {
    
    /**
     * org.xms.g.location.LocationStatusCodes.LocationStatusCodes(org.xms.g.utils.XBox) constructor of LocationStatusCodes with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationStatusCodes(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getERROR() return the constant value.<br/>
     * com.google.android.gms.location.LocationStatusCodes.ERROR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-error">https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-error</a><br/>
     * 
     * @return Constant Value.An unspecified error occurred; no more specific information is available. The device logs may provide additional data
     */
    public static int getERROR() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationStatusCodes.ERROR");
        return com.google.android.gms.location.LocationStatusCodes.ERROR;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getGEOFENCE_NOT_AVAILABLE() return the constant value.<br/>
     * com.google.android.gms.location.LocationStatusCodes.GEOFENCE_NOT_AVAILABLE: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-geofence_not_available">https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-geofence_not_available</a><br/>
     * 
     * @return Constant Value.Geofence service is not available now. Typically this is because the user turned off location access in settings > location access
     */
    public static int getGEOFENCE_NOT_AVAILABLE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationStatusCodes.GEOFENCE_NOT_AVAILABLE");
        return com.google.android.gms.location.LocationStatusCodes.GEOFENCE_NOT_AVAILABLE;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getGEOFENCE_TOO_MANY_GEOFENCES() return the constant value.<br/>
     * com.google.android.gms.location.LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-geofence_too_many_geofences">https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-geofence_too_many_geofences</a><br/>
     * 
     * @return Constant Value.Your app has registered more than 100 geofences. Remove unused ones before adding new geofences
     */
    public static int getGEOFENCE_TOO_MANY_GEOFENCES() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES");
        return com.google.android.gms.location.LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getGEOFENCE_TOO_MANY_PENDING_INTENTS() return the constant value.<br/>
     * com.google.android.gms.location.LocationStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-geofence_too_many_pending_intents">https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-geofence_too_many_pending_intents</a><br/>
     * 
     * @return Constant Value.You have provided more than 5 different PendingIntents to the addGeofences(GoogleApiClient, GeofencingRequest, PendingIntent) call
     */
    public static int getGEOFENCE_TOO_MANY_PENDING_INTENTS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS");
        return com.google.android.gms.location.LocationStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.getSUCCESS() return the constant value.<br/>
     * com.google.android.gms.location.LocationStatusCodes.SUCCESS: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-success">https://developers.google.com/android/reference/com/google/android/gms/location/LocationStatusCodes#public-static-final-int-success</a><br/>
     * 
     * @return Constant Value. The operation was successful
     */
    public static int getSUCCESS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationStatusCodes.SUCCESS");
        return com.google.android.gms.location.LocationStatusCodes.SUCCESS;
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationStatusCodes.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationStatusCodes object
     */
    public static org.xms.g.location.LocationStatusCodes dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationStatusCodes) param0);
    }
    
    /**
     * org.xms.g.location.LocationStatusCodes.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationStatusCodes;
    }
}