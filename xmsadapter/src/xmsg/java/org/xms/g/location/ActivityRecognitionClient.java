package org.xms.g.location;

/**
 * The main entry point for interacting with activity recognition..<br/>
 * Wrapper class for com.google.android.gms.location.ActivityRecognitionClient, but only the GMS API are provided.</br>
 * com.google.android.gms.location.ActivityRecognitionClient: The main entry point for interacting with activity recognition.</br>
 */
public class ActivityRecognitionClient extends org.xms.g.common.api.ExtensionApi<org.xms.g.common.api.Api.ApiOptions.NoOptions> {
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.ActivityRecognitionClient(org.xms.g.utils.XBox) Constructor of ActivityRecognitionClient with XBox.<br/>
     * 
     * @param  param0 The wrapper of xms instance
     */
    public ActivityRecognitionClient(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.ActivityRecognitionClient(android.app.Activity) Wrapper constructor of ActivityIdentificationService and ActivityRecognitionClient.<br/>
     * com.google.android.gms.location.ActivityRecognitionClient(android.app.Activity): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient</a><br/>
     * 
     * @param  param0 Activity of android app
     */
    public ActivityRecognitionClient(android.app.Activity param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl(param0));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.ActivityRecognitionClient(android.content.Context) Create a new instance of Client for use in an Context.<br/>
     * com.google.android.gms.location.ActivityRecognitionClient(android.content.Context): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient</a><br/>
     * 
     * @param  param0 Context instance
     */
    public ActivityRecognitionClient(android.content.Context param0) {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl(param0));
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.removeActivityTransitionUpdates(android.app.PendingIntent) Removes activity transition updates associated with the given pendingIntent.<br/>
     * com.google.android.gms.location.ActivityRecognitionClient.removeActivityTransitionUpdates(android.app.PendingIntent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-removeactivitytransitionupdates-pendingintent-pendingintent">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-removeactivitytransitionupdates-pendingintent-pendingintent</a><br/>
     * 
     * @param  param0 The associated PendingIntent of the activity transition request which is to be removed
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> removeActivityTransitionUpdates(android.app.PendingIntent param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).removeActivityTransitionUpdates(param0)");
            com.google.android.gms.tasks.Task gReturn = ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).removeActivityTransitionUpdates(param0);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).removeActivityTransitionUpdatesCallSuper(param0)");
            com.google.android.gms.tasks.Task gReturn = ((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).removeActivityTransitionUpdatesCallSuper(param0);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.removeActivityUpdates(android.app.PendingIntent) Removes all activity updates for the specified PendingIntent.<br/>
     * com.google.android.gms.location.ActivityRecognitionClient.removeActivityUpdates(android.app.PendingIntent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-removeactivityupdates-pendingintent-callbackintent">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-removeactivityupdates-pendingintent-callbackintent</a><br/>
     * 
     * @param  param0 The PendingIntent that was used in requestActivityUpdates(long, PendingIntent) or is equal as defined by equals(Object)
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> removeActivityUpdates(android.app.PendingIntent param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).removeActivityUpdates(param0)");
            com.google.android.gms.tasks.Task gReturn = ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).removeActivityUpdates(param0);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).removeActivityUpdatesCallSuper(param0)");
            com.google.android.gms.tasks.Task gReturn = ((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).removeActivityUpdatesCallSuper(param0);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.requestActivityTransitionUpdates(org.xms.g.location.ActivityTransitionRequest,android.app.PendingIntent) Activity Recognition Transition API provides an ability for apps to subscribe to activity transitional conditions (enter, exit).<br/>
     * com.google.android.gms.location.ActivityRecognitionClient.requestActivityTransitionUpdates(com.google.android.gms.location.ActivityTransitionRequest,android.app.PendingIntent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-requestactivitytransitionupdates-activitytransitionrequest-activitytransitionrequest,-pendingintent-pendingintent">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-requestactivitytransitionupdates-activitytransitionrequest-activitytransitionrequest,-pendingintent-pendingintent</a><br/>
     * 
     * @param  param0 The interested activity transitions
     * @param  param1 The PendingIntent used to generate the callback intent when one of the interested transition has happened
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> requestActivityTransitionUpdates(org.xms.g.location.ActivityTransitionRequest param0, android.app.PendingIntent param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).requestActivityTransitionUpdates(((com.google.android.gms.location.ActivityTransitionRequest) ((param0) == null ? null : (param0.getGInstance()))), param1)");
            com.google.android.gms.tasks.Task gReturn = ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).requestActivityTransitionUpdates(((com.google.android.gms.location.ActivityTransitionRequest) ((param0) == null ? null : (param0.getGInstance()))), param1);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).requestActivityTransitionUpdatesCallSuper(((com.google.android.gms.location.ActivityTransitionRequest) ((param0) == null ? null : (param0.getGInstance()))), param1)");
            com.google.android.gms.tasks.Task gReturn = ((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).requestActivityTransitionUpdatesCallSuper(((com.google.android.gms.location.ActivityTransitionRequest) ((param0) == null ? null : (param0.getGInstance()))), param1);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.requestActivityUpdates(long,android.app.PendingIntent) Register for activity recognition updates.<br/>
     * com.google.android.gms.location.ActivityRecognitionClient.requestActivityUpdates(long,android.app.PendingIntent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-requestactivityupdates-long-detectionintervalmillis,-pendingintent-callbackintent">https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient#public-taskvoid-requestactivityupdates-long-detectionintervalmillis,-pendingintent-callbackintent</a><br/>
     * 
     * @param  param0 The desired time between activity detections. Larger values will result in fewer activity detections while improving battery life. A value of 0 will result in activity detections at the fastest possible rate
     * @param  param1 A PendingIntent to be sent for each activity detection
     * @return A Task for apps to check the status of the call. If the task fails, the status code for the failure can be found by examining getStatusCode()
     */
    public org.xms.g.tasks.Task<java.lang.Void> requestActivityUpdates(long param0, android.app.PendingIntent param1) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).requestActivityUpdates(param0, param1)");
            com.google.android.gms.tasks.Task gReturn = ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance()).requestActivityUpdates(param0, param1);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).requestActivityUpdatesCallSuper(param0, param1)");
            com.google.android.gms.tasks.Task gReturn = ((GImpl) ((com.google.android.gms.location.ActivityRecognitionClient) this.getGInstance())).requestActivityUpdatesCallSuper(param0, param1);
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
    }
    
    /**
     * XMS does not provide this api.
     */
    public java.lang.Object getApiKey() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.ActivityRecognitionClient.<br/>
     * 
     * @param  param0 The input object
     * @return Casted ActivityRecognitionClient object
     */
    public static org.xms.g.location.ActivityRecognitionClient dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.location.ActivityRecognitionClient) {
            return ((org.xms.g.location.ActivityRecognitionClient) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.location.ActivityRecognitionClient gReturn = ((com.google.android.gms.location.ActivityRecognitionClient) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.location.ActivityRecognitionClient(new org.xms.g.utils.XBox(gReturn));
        }
        return ((org.xms.g.location.ActivityRecognitionClient) param0);
    }
    
    /**
     * org.xms.g.location.ActivityRecognitionClient.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.ActivityRecognitionClient;
    }
    
    private class GImpl extends com.google.android.gms.location.ActivityRecognitionClient {
        
        public com.google.android.gms.tasks.Task<java.lang.Void> removeActivityTransitionUpdates(android.app.PendingIntent param0) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.removeActivityTransitionUpdates(param0);
            return ((com.google.android.gms.tasks.Task) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> removeActivityUpdates(android.app.PendingIntent param0) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.removeActivityUpdates(param0);
            return ((com.google.android.gms.tasks.Task) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> requestActivityTransitionUpdates(com.google.android.gms.location.ActivityTransitionRequest param0, android.app.PendingIntent param1) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.requestActivityTransitionUpdates(((param0) == null ? null : (new org.xms.g.location.ActivityTransitionRequest(new org.xms.g.utils.XBox(param0)))), param1);
            return ((com.google.android.gms.tasks.Task) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> requestActivityUpdates(long param0, android.app.PendingIntent param1) {
            org.xms.g.tasks.Task xResult = org.xms.g.location.ActivityRecognitionClient.this.requestActivityUpdates(param0, param1);
            return ((com.google.android.gms.tasks.Task) ((xResult) == null ? null : (xResult.getGInstance())));
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> removeActivityTransitionUpdatesCallSuper(android.app.PendingIntent param0) {
            return super.removeActivityTransitionUpdates(param0);
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> removeActivityUpdatesCallSuper(android.app.PendingIntent param0) {
            return super.removeActivityUpdates(param0);
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> requestActivityTransitionUpdatesCallSuper(com.google.android.gms.location.ActivityTransitionRequest param0, android.app.PendingIntent param1) {
            return super.requestActivityTransitionUpdates(param0, param1);
        }
        
        public com.google.android.gms.tasks.Task<java.lang.Void> requestActivityUpdatesCallSuper(long param0, android.app.PendingIntent param1) {
            return super.requestActivityUpdates(param0, param1);
        }
        
        public GImpl(android.app.Activity param0) {
            super(param0);
        }
        
        public GImpl(android.content.Context param0) {
            super(param0);
        }
    }
}