package org.xms.g.location;

/**
 * A data object that contains quality of service parameters for requests to the FusedLocationProviderApi..<br/>
 * Wrapper class for com.google.android.gms.location.LocationRequest, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationRequest: A data object that contains quality of service parameters for requests to the FusedLocationProviderApi.</br>
 */
public final class LocationRequest extends org.xms.g.utils.XObject implements android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.LocationRequest.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-creatorlocationrequest-creator">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-creatorlocationrequest-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationRequest createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.LocationRequest gReturn = com.google.android.gms.location.LocationRequest.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.LocationRequest[] newArray(int param0) {
            return new org.xms.g.location.LocationRequest[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationRequest.LocationRequest(org.xms.g.utils.XBox) constructor of LocationRequest with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationRequest(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationRequest.LocationRequest() constructor of LocationRequest.<br/>
     * com.google.android.gms.location.LocationRequest()
     */
    public LocationRequest() {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new com.google.android.gms.location.LocationRequest());
    }
    
    /**
     * org.xms.g.location.LocationRequest.getPRIORITY_BALANCED_POWER_ACCURACY() return the constant value.<br/>
     * com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_balanced_power_accuracy">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_balanced_power_accuracy</a><br/>
     * 
     * @return Constant Value.Block level accuracy is considered to be about 100 meter accuracy. Using a coarse accuracy such as this often consumes less power
     */
    public static int getPRIORITY_BALANCED_POWER_ACCURACY() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY");
        return com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
    }
    
    /**
     * org.xms.g.location.LocationRequest.getPRIORITY_HIGH_ACCURACY() return the constant value.<br/>
     * com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_high_accuracy">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_high_accuracy</a><br/>
     * 
     * @return Constant Value.Used with setPriority(int) to request the most accurate locations available
     */
    public static int getPRIORITY_HIGH_ACCURACY() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY");
        return com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;
    }
    
    /**
     * org.xms.g.location.LocationRequest.getPRIORITY_LOW_POWER() return the constant value.<br/>
     * com.google.android.gms.location.LocationRequest.PRIORITY_LOW_POWER: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_low_power">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_low_power</a><br/>
     * 
     * @return Constant Value.Used with setPriority(int) to request "city" level accuracy
     */
    public static int getPRIORITY_LOW_POWER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationRequest.PRIORITY_LOW_POWER");
        return com.google.android.gms.location.LocationRequest.PRIORITY_LOW_POWER;
    }
    
    /**
     * org.xms.g.location.LocationRequest.getPRIORITY_NO_POWER() return the constant value.<br/>
     * com.google.android.gms.location.LocationRequest.PRIORITY_NO_POWER: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_no_power">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-final-int-priority_no_power</a><br/>
     * 
     * @return Constant Value.Used with setPriority(int) to request the best accuracy possible with zero additional power consumption
     */
    public static int getPRIORITY_NO_POWER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationRequest.PRIORITY_NO_POWER");
        return com.google.android.gms.location.LocationRequest.PRIORITY_NO_POWER;
    }
    
    /**
     * org.xms.g.location.LocationRequest.create() Create a location request with default parameters.<br/>
     * com.google.android.gms.location.LocationRequest.create(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-locationrequest-create">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-static-locationrequest-create</a><br/>
     * 
     * @return a new location request
     */
    public static org.xms.g.location.LocationRequest create() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationRequest.create()");
        com.google.android.gms.location.LocationRequest gReturn = com.google.android.gms.location.LocationRequest.create();
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.equals(java.lang.Object) Checks whether two instances are equal.<br/>
     * com.google.android.gms.location.LocationRequest.equals(java.lang.Object): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-boolean-equals-object-object">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-boolean-equals-object-object</a><br/>
     * 
     * @param  param0 the other instance
     * @return true if two instances are equal
     */
    public boolean equals(java.lang.Object param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).equals(param0)");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).equals(param0);
    }
    
    /**
     * org.xms.g.location.LocationRequest.getExpirationTime() Get the request expiration time, in milliseconds since boot.<br/>
     * com.google.android.gms.location.LocationRequest.getExpirationTime(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getexpirationtime">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getexpirationtime</a><br/>
     * 
     * @return expiration time of request, in milliseconds since boot including suspend
     */
    public final long getExpirationTime() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getExpirationTime()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getExpirationTime();
    }
    
    /**
     * org.xms.g.location.LocationRequest.getFastestInterval() Get the fastest interval of this request, in milliseconds.<br/>
     * com.google.android.gms.location.LocationRequest.getFastestInterval(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getfastestinterval">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getfastestinterval</a><br/>
     * 
     * @return fastest interval in milliseconds, exact
     */
    public final long getFastestInterval() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getFastestInterval()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getFastestInterval();
    }
    
    /**
     * org.xms.g.location.LocationRequest.getInterval() Get the desired interval of this request, in milliseconds.<br/>
     * com.google.android.gms.location.LocationRequest.getInterval(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getinterval">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getinterval</a><br/>
     * 
     * @return desired interval in milliseconds, inexact
     */
    public final long getInterval() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getInterval()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getInterval();
    }
    
    /**
     * org.xms.g.location.LocationRequest.getMaxWaitTime() Gets the maximum wait time in milliseconds for location updates.<br/>
     * com.google.android.gms.location.LocationRequest.getMaxWaitTime(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getmaxwaittime">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-long-getmaxwaittime</a><br/>
     * 
     * @return maximum wait time in milliseconds, inexact
     */
    public final long getMaxWaitTime() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getMaxWaitTime()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getMaxWaitTime();
    }
    
    /**
     * org.xms.g.location.LocationRequest.getNumUpdates() Get the number of updates requested.<br/>
     * com.google.android.gms.location.LocationRequest.getNumUpdates(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-int-getnumupdates">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-int-getnumupdates</a><br/>
     * 
     * @return number of updates
     */
    public final int getNumUpdates() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getNumUpdates()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getNumUpdates();
    }
    
    /**
     * org.xms.g.location.LocationRequest.getPriority() Get the quality of the request.<br/>
     * com.google.android.gms.location.LocationRequest.getPriority(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-int-getpriority">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-int-getpriority</a><br/>
     * 
     * @return an accuracy constant
     */
    public final int getPriority() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getPriority()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getPriority();
    }
    
    /**
     * org.xms.g.location.LocationRequest.getSmallestDisplacement() Get the minimum displacement between location updates in meters.<br/>
     * com.google.android.gms.maps.model..getSmallestDisplacement(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-float-getsmallestdisplacement">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-float-getsmallestdisplacement</a><br/>
     * 
     * @return minimum displacement between location updates in meters
     */
    public final float getSmallestDisplacement() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).getSmallestDisplacement()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).getSmallestDisplacement();
    }
    
    /**
     * org.xms.g.location.LocationRequest.hashCode() Overrides the method of the java.lang.Object class to calculate hashCode of a object.<br/>
     * com.google.android.gms.location.LocationRequest.hashCode(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-int-hashcode">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-int-hashcode</a><br/>
     * 
     * @return a hash code value
     */
    public final int hashCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).hashCode()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).hashCode();
    }
    
    /**
     * org.xms.g.location.LocationRequest.isFastestIntervalExplicitlySet() Returns whether or not the fastest interval was explicitly specified for the location request.<br/>
     * com.google.android.gms.location.LocationRequest.isFastestIntervalExplicitlySet(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-boolean-isfastestintervalexplicitlyset">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-boolean-isfastestintervalexplicitlyset</a><br/>
     * 
     * @return True if the fastest interval was explicitly set for the location request; false otherwise
     */
    public final boolean isFastestIntervalExplicitlySet() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).isFastestIntervalExplicitlySet()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).isFastestIntervalExplicitlySet();
    }
    
    /**
     * org.xms.g.location.LocationRequest.setExpirationDuration(long) Set the duration of this request, in milliseconds.<br/>
     * com.google.android.gms.location.LocationRequest.setExpirationDuration(long): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setexpirationduration-long-millis">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setexpirationduration-long-millis</a><br/>
     * 
     * @param  param0 duration of request in milliseconds
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setExpirationDuration(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setExpirationDuration(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setExpirationDuration(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setExpirationTime(long) Set the request expiration time, in millisecond since boot.<br/>
     * com.google.android.gms.location.LocationRequest.setExpirationTime(long): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setexpirationtime-long-millis">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setexpirationtime-long-millis</a><br/>
     * 
     * @param  param0 expiration time of request, in milliseconds since boot including suspend
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setExpirationTime(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setExpirationTime(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setExpirationTime(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setFastestInterval(long) Explicitly set the fastest interval for location updates, in milliseconds.<br/>
     * com.google.android.gms.location.LocationRequest.setFastestInterval(long): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setfastestinterval-long-millis">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setfastestinterval-long-millis</a><br/>
     * 
     * @param  param0 fastest interval for updates in milliseconds, exact
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setFastestInterval(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setFastestInterval(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setFastestInterval(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setInterval(long) Set the desired interval for active location updates, in milliseconds.<br/>
     * com.google.android.gms.location.LocationRequest.setInterval(long): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setinterval-long-millis">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setinterval-long-millis</a><br/>
     * 
     * @param  param0 desired interval in millisecond, inexact
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setInterval(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setInterval(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setInterval(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setMaxWaitTime(long) Sets the maximum wait time in milliseconds for location updates.<br/>
     * com.google.android.gms.location.LocationRequest.setMaxWaitTime(long): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setmaxwaittime-long-millis">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setmaxwaittime-long-millis</a><br/>
     * 
     * @param  param0 desired maximum wait time in millisecond, inexact
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setMaxWaitTime(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setMaxWaitTime(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setMaxWaitTime(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setNumUpdates(int) Set the number of location updates.<br/>
     * com.google.android.gms.location.LocationRequest.setNumUpdates(int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setnumupdates-int-numupdates">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setnumupdates-int-numupdates</a><br/>
     * 
     * @param  param0 the number of location updates requested
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setNumUpdates(int param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setNumUpdates(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setNumUpdates(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setPriority(int) Set the priority of the request.<br/>
     * com.google.android.gms.location.LocationRequest.setPriority(int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setpriority-int-priority">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setpriority-int-priority</a><br/>
     * 
     * @param  param0 an accuracy or power constant
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setPriority(int param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setPriority(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setPriority(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.setSmallestDisplacement(float) Set the minimum displacement between location updates in meters.<br/>
     * com.google.android.gms.location.LocationRequest.setSmallestDisplacement(float): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setsmallestdisplacement-float-smallestdisplacementmeters">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-locationrequest-setsmallestdisplacement-float-smallestdisplacementmeters</a><br/>
     * 
     * @param  param0 the smallest displacement in meters the user must move between location updates
     * @return the same object, so that setters can be chained
     */
    public final org.xms.g.location.LocationRequest setSmallestDisplacement(float param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).setSmallestDisplacement(param0)");
        com.google.android.gms.location.LocationRequest gReturn = ((com.google.android.gms.location.LocationRequest) this.getGInstance()).setSmallestDisplacement(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationRequest(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationRequest.toString() Overrides the method of the java.lang.Object class to convert a value into a character string.<br/>
     * com.google.android.gms.location.LocationRequest.toString(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-string-tostring">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-string-tostring</a><br/>
     * 
     * @return A character string after being converted
     */
    public final java.lang.String toString() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).toString()");
        return ((com.google.android.gms.location.LocationRequest) this.getGInstance()).toString();
    }
    
    /**
     * org.xms.g.location.LocationRequest.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.LocationRequest.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-void-writetoparcel-parcel-parcel,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest#public-void-writetoparcel-parcel-parcel,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationRequest) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.location.LocationRequest) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationRequest.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationRequest.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationRequest object
     */
    public static org.xms.g.location.LocationRequest dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationRequest) param0);
    }
    
    /**
     * org.xms.g.location.LocationRequest.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationRequest;
    }
}