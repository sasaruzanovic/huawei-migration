package org.xms.g.location;

/**
 * Result of checking settings, indicates whether a dialog should be shown to ask the user's consent to change their settings..<br/>
 * Wrapper class for com.google.android.gms.location.LocationSettingsResult, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationSettingsResult: Result of checking settings via checkLocationSettings(GoogleApiClient, LocationSettingsRequest), indicates whether a dialog should be shown to ask the user's consent to change their settings.</br>
 */
public final class LocationSettingsResult extends org.xms.g.utils.XObject implements org.xms.g.common.api.Result, android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.LocationSettingsResult.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-static-final-creatorlocationsettingsresult-creator">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-static-final-creatorlocationsettingsresult-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationSettingsResult createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.LocationSettingsResult gReturn = com.google.android.gms.location.LocationSettingsResult.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationSettingsResult(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.LocationSettingsResult[] newArray(int param0) {
            return new org.xms.g.location.LocationSettingsResult[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationSettingsResult.LocationSettingsResult(org.xms.g.utils.XBox) constructor of LocationSettingsResult with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsResult(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.getLocationSettingsStates() Retrieves the location settings states.<br/>
     * com.google.android.gms.location.LocationSettingsResult.getLocationSettingsStates(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-locationsettingsstates-getlocationsettingsstates">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-locationsettingsstates-getlocationsettingsstates</a><br/>
     * 
     * @return the location settings states
     */
    public final org.xms.g.location.LocationSettingsStates getLocationSettingsStates() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsResult) this.getGInstance()).getLocationSettingsStates()");
        com.google.android.gms.location.LocationSettingsStates gReturn = ((com.google.android.gms.location.LocationSettingsResult) this.getGInstance()).getLocationSettingsStates();
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.getStatus() Returns the status of this result.<br/>
     * com.google.android.gms.location.LocationSettingsResult.getStatus(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-status-getstatus">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-status-getstatus</a><br/>
     * 
     * @return the status of this result
     */
    public final org.xms.g.common.api.Status getStatus() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsResult) this.getGInstance()).getStatus()");
        com.google.android.gms.common.api.Status gReturn = ((com.google.android.gms.location.LocationSettingsResult) this.getGInstance()).getStatus();
        return ((gReturn) == null ? null : (new org.xms.g.common.api.Status(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.LocationSettingsResult.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-void-writetoparcel-parcel-dest,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResult#public-void-writetoparcel-parcel-dest,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsResult) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.location.LocationSettingsResult) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsResult.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsResult object
     */
    public static org.xms.g.location.LocationSettingsResult dynamicCast(java.lang.Object param0) {
        if (param0 instanceof org.xms.g.location.LocationSettingsResult) {
            return ((org.xms.g.location.LocationSettingsResult) param0);
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            com.google.android.gms.location.LocationSettingsResult gReturn = ((com.google.android.gms.location.LocationSettingsResult) ((org.xms.g.utils.XGettable) param0).getGInstance());
            return new org.xms.g.location.LocationSettingsResult(new org.xms.g.utils.XBox(gReturn));
        }
        return ((org.xms.g.location.LocationSettingsResult) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResult.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationSettingsResult;
    }
}