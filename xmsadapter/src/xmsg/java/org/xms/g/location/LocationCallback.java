package org.xms.g.location;

/**
 * Used for receiving notifications with the location information..<br/>
 * Wrapper class for com.google.android.gms.location.LocationCallback, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationCallback: Used for receiving notifications from the FusedLocationProviderApi when the device location has changed or can no longer be determined.</br>
 */
public class LocationCallback extends org.xms.g.utils.XObject {
    private boolean wrapper = true;
    
    /**
     * org.xms.g.location.LocationCallback.LocationCallback(org.xms.g.utils.XBox) constructor of LocationCallback with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationCallback(org.xms.g.utils.XBox param0) {
        super(param0);
        wrapper = true;
    }
    
    /**
     * org.xms.g.location.LocationCallback.LocationCallback() constructor of LocationCallback.<br/>
     * com.google.android.gms.location.LocationCallback(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback#public-locationcallback">https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback#public-locationcallback</a><br/>
     * 
     */
    public LocationCallback() {
        super(((org.xms.g.utils.XBox) null));
        this.setGInstance(new GImpl());
        wrapper = false;
    }
    
    /**
     * org.xms.g.location.LocationCallback.onLocationAvailability(org.xms.g.location.LocationAvailability) Called when there is a change in the availability of location data.<br/>
     * com.google.android.gms.location.LocationCallback.onLocationAvailability(com.google.android.gms.location.LocationAvailability): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback#public-void-onlocationavailability-locationavailability-locationavailability">https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback#public-void-onlocationavailability-locationavailability-locationavailability</a><br/>
     * 
     * @param  param0 The current status of location availability
     */
    public void onLocationAvailability(org.xms.g.location.LocationAvailability param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationCallback) this.getGInstance()).onLocationAvailability(((com.google.android.gms.location.LocationAvailability) ((param0) == null ? null : (param0.getGInstance()))))");
            ((com.google.android.gms.location.LocationCallback) this.getGInstance()).onLocationAvailability(((com.google.android.gms.location.LocationAvailability) ((param0) == null ? null : (param0.getGInstance()))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.LocationCallback) this.getGInstance())).onLocationAvailabilityCallSuper(((com.google.android.gms.location.LocationAvailability) ((param0) == null ? null : (param0.getGInstance()))))");
            ((GImpl) ((com.google.android.gms.location.LocationCallback) this.getGInstance())).onLocationAvailabilityCallSuper(((com.google.android.gms.location.LocationAvailability) ((param0) == null ? null : (param0.getGInstance()))));
        }
    }
    
    /**
     * org.xms.g.location.LocationCallback.onLocationResult(org.xms.g.location.LocationResult) Called when device location information is available.<br/>
     * com.google.android.gms.location.LocationCallback.onLocationResult(com.google.android.gms.location.LocationResult): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback#public-void-onlocationresult-locationresult-result">https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback#public-void-onlocationresult-locationresult-result</a><br/>
     * 
     * @param  param0 The latest location result available
     */
    public void onLocationResult(org.xms.g.location.LocationResult param0) {
        if (wrapper) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationCallback) this.getGInstance()).onLocationResult(((com.google.android.gms.location.LocationResult) ((param0) == null ? null : (param0.getGInstance()))))");
            ((com.google.android.gms.location.LocationCallback) this.getGInstance()).onLocationResult(((com.google.android.gms.location.LocationResult) ((param0) == null ? null : (param0.getGInstance()))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((GImpl) ((com.google.android.gms.location.LocationCallback) this.getGInstance())).onLocationResultCallSuper(((com.google.android.gms.location.LocationResult) ((param0) == null ? null : (param0.getGInstance()))))");
            ((GImpl) ((com.google.android.gms.location.LocationCallback) this.getGInstance())).onLocationResultCallSuper(((com.google.android.gms.location.LocationResult) ((param0) == null ? null : (param0.getGInstance()))));
        }
    }
    
    /**
     * org.xms.g.location.LocationCallback.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationCallback.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationCallback object
     */
    public static org.xms.g.location.LocationCallback dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationCallback) param0);
    }
    
    /**
     * org.xms.g.location.LocationCallback.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationCallback;
    }
    
    private class GImpl extends com.google.android.gms.location.LocationCallback {
        
        public void onLocationAvailability(com.google.android.gms.location.LocationAvailability param0) {
            org.xms.g.location.LocationCallback.this.onLocationAvailability(((param0) == null ? null : (new org.xms.g.location.LocationAvailability(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void onLocationResult(com.google.android.gms.location.LocationResult param0) {
            org.xms.g.location.LocationCallback.this.onLocationResult(((param0) == null ? null : (new org.xms.g.location.LocationResult(new org.xms.g.utils.XBox(param0)))));
        }
        
        public void onLocationAvailabilityCallSuper(com.google.android.gms.location.LocationAvailability param0) {
            super.onLocationAvailability(param0);
        }
        
        public void onLocationResultCallSuper(com.google.android.gms.location.LocationResult param0) {
            super.onLocationResult(param0);
        }
        
        public GImpl() {
            super();
        }
    }
}