package org.xms.g.location;

/**
 * The main entry point for location services integration..<br/>
 * Wrapper class for com.google.android.gms.location.LocationServices, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationServices: The main entry point for location services integration.</br>
 */
public class LocationServices extends org.xms.g.utils.XObject {
    
    /**
     * org.xms.g.location.LocationServices.LocationServices(org.xms.g.utils.XBox) constructor of LocationServices with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationServices(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationServices.getAPI() return the API.<br/>
     * com.google.android.gms.location.LocationServices.API: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-apiapi.apioptions.nooptions-api">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-apiapi.apioptions.nooptions-api</a><br/>
     * 
     * @return Token to pass to addApi(Api ) to enable LocationServices
     */
    public static org.xms.g.common.api.Api getAPI() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationServices.getFusedLocationApi() return the API.<br/>
     * com.google.android.gms.location.FusedLocationApi: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-fusedlocationproviderapi-fusedlocationapi">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-fusedlocationproviderapi-fusedlocationapi</a><br/>
     * 
     * @return FusedLocationProviderApi.Entry point to the fused location APIs
     */
    public static org.xms.g.location.FusedLocationProviderApi getFusedLocationApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationServices.getGeofencingApi() return the API.<br/>
     * com.google.android.gms.location.GeofencingApi: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-geofencingapi-geofencingapi">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-geofencingapi-geofencingapi</a><br/>
     * 
     * @return GeofencingApi.Entry point to the geofencing APIs
     */
    public static org.xms.g.location.GeofencingApi getGeofencingApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationServices.getSettingsApi() return the API.<br/>
     * com.google.android.gms.location.SettingsApi: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-settingsapi-settingsapi">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-final-settingsapi-settingsapi</a><br/>
     * 
     * @return SettingsApi.Entry point to the location settings-enabler dialog APIs.
     */
    public static org.xms.g.location.SettingsApi getSettingsApi() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationServices.getFusedLocationProviderClient(android.app.Activity) Create a new instance of FusedLocationProviderClient for use in an Activity.<br/>
     * com.google.android.gms.location.LocationServices.getFusedLocationProviderClient(android.app.Activity): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-fusedlocationproviderclient-getfusedlocationproviderclient-activity-activity">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-fusedlocationproviderclient-getfusedlocationproviderclient-activity-activity</a><br/>
     * 
     * @param  param0 Activity in app
     * @return FusedLocationProviderClient instance
     */
    public static org.xms.g.location.FusedLocationProviderClient getFusedLocationProviderClient(android.app.Activity param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationServices.getFusedLocationProviderClient(param0)");
        com.google.android.gms.location.FusedLocationProviderClient gReturn = com.google.android.gms.location.LocationServices.getFusedLocationProviderClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.FusedLocationProviderClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationServices.getFusedLocationProviderClient(android.content.Context) Create a new instance of FusedLocationProviderClient for use in a non-activity Context.<br/>
     * com.google.android.gms.location.LocationServices.getFusedLocationProviderClient(android.content.Context): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-fusedlocationproviderclient-getfusedlocationproviderclient-context-context">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-fusedlocationproviderclient-getfusedlocationproviderclient-context-context</a><br/>
     * 
     * @param  param0 a specific Context
     * @return FusedLocationProviderClient instance
     */
    public static org.xms.g.location.FusedLocationProviderClient getFusedLocationProviderClient(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationServices.getFusedLocationProviderClient(param0)");
        com.google.android.gms.location.FusedLocationProviderClient gReturn = com.google.android.gms.location.LocationServices.getFusedLocationProviderClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.FusedLocationProviderClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationServices.getGeofencingClient(android.app.Activity) Create a new instance of GeofencingClient for use in an Activity.<br/>
     * com.google.android.gms.location.LocationServices.getGeofencingClient(android.app.Activity): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-geofencingclient-getgeofencingclient-activity-activity">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-geofencingclient-getgeofencingclient-activity-activity</a><br/>
     * 
     * @param  param0 Activity in app
     * @return GeofencingClient instance
     */
    public static org.xms.g.location.GeofencingClient getGeofencingClient(android.app.Activity param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationServices.getGeofencingClient(param0)");
        com.google.android.gms.location.GeofencingClient gReturn = com.google.android.gms.location.LocationServices.getGeofencingClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.GeofencingClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationServices.getGeofencingClient(android.content.Context) Create a new instance of GeofencingClient for use in a non-activity Context.<br/>
     * com.google.android.gms.location.LocationServices.getGeofencingClient(android.content.Context): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-geofencingclient-getgeofencingclient-context-context">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-geofencingclient-getgeofencingclient-context-context</a><br/>
     * 
     * @param  param0 a specific Context
     * @return GeofencingClient instance
     */
    public static org.xms.g.location.GeofencingClient getGeofencingClient(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationServices.getGeofencingClient(param0)");
        com.google.android.gms.location.GeofencingClient gReturn = com.google.android.gms.location.LocationServices.getGeofencingClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.GeofencingClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationServices.getSettingsClient(android.content.Context) Create a new instance of SettingsClient for use in a non-activity Context.<br/>
     * com.google.android.gms.location.LocationServices.getSettingsClient(android.content.Context): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-settingsclient-getsettingsclient-context-context">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-settingsclient-getsettingsclient-context-context</a><br/>
     * 
     * @param  param0 a specific Context
     * @return SettingsClient instance
     */
    public static org.xms.g.location.SettingsClient getSettingsClient(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationServices.getSettingsClient(param0)");
        com.google.android.gms.location.SettingsClient gReturn = com.google.android.gms.location.LocationServices.getSettingsClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.SettingsClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationServices.getSettingsClient(android.app.Activity) Create a new instance of SettingsClient for use in an Activity.<br/>
     * com.google.android.gms.location.LocationServices.getSettingsClient(android.app.Activity): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-settingsclient-getsettingsclient-activity-activity">https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices#public-static-settingsclient-getsettingsclient-activity-activity</a><br/>
     * 
     * @param  param0 Activity in app
     * @return SettingsClient instance
     */
    public static org.xms.g.location.SettingsClient getSettingsClient(android.app.Activity param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationServices.getSettingsClient(param0)");
        com.google.android.gms.location.SettingsClient gReturn = com.google.android.gms.location.LocationServices.getSettingsClient(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.SettingsClient(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationServices.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationServices.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationServices object
     */
    public static org.xms.g.location.LocationServices dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationServices) param0);
    }
    
    /**
     * org.xms.g.location.LocationServices.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationServices;
    }
}