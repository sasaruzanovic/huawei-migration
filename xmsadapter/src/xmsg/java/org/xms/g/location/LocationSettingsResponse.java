package org.xms.g.location;

/**
 * Successful response of checking settings..<br/>
 * Wrapper class for com.google.android.gms.location.LocationSettingsResponse, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationSettingsResponse: Successful response of checking settings via checkLocationSettings(GoogleApiClient, LocationSettingsRequest).</br>
 */
public class LocationSettingsResponse extends org.xms.g.common.api.Response<org.xms.g.location.LocationSettingsResult> {
    
    /**
     * org.xms.g.location.LocationSettingsResponse.LocationSettingsResponse(org.xms.g.utils.XBox) constructor of LocationSettingsResponse with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsResponse(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResponse.getLocationSettingsStates() Obtains the location setting status.<br/>
     * com.google.android.gms.location.LocationSettingsResponse.getLocationSettingsStates(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResponse#public-locationsettingsstates-getlocationsettingsstates">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsResponse#public-locationsettingsstates-getlocationsettingsstates</a><br/>
     * 
     * @return Location setting status
     */
    public org.xms.g.location.LocationSettingsStates getLocationSettingsStates() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsResponse) this.getGInstance()).getLocationSettingsStates()");
        com.google.android.gms.location.LocationSettingsStates gReturn = ((com.google.android.gms.location.LocationSettingsResponse) this.getGInstance()).getLocationSettingsStates();
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsResponse.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsResponse.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsResponse object
     */
    public static org.xms.g.location.LocationSettingsResponse dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationSettingsResponse) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsResponse.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationSettingsResponse;
    }
}