package org.xms.g.location;

/**
 * Represents a geographical region, also known as a geofence. Geofences can be monitored by geofencer service. And when the user crosses the boundary of a geofence, an alert will be generated..<br/>
 * Wrapper class for com.google.android.gms.location.Geofence, but only the GMS API are provided.</br>
 * com.google.android.gms.location.Geofence: Represents a geographical region, also known as a geofence. Geofences can be monitored by geofencer service. And when the user crosses the boundary of a geofence, an alert will be generated.</br>
 */
public interface Geofence extends org.xms.g.utils.XInterface {
    
    /**
     * org.xms.g.location.Geofence.getGEOFENCE_TRANSITION_DWELL() Return the constant value.<br/>
     * com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_DWELL: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-int-geofence_transition_dwell">https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-int-geofence_transition_dwell</a><br/>
     * 
     * @return Constant Value.The transition type indicating that the user enters and dwells in geofences for a given period of time
     */
    public static int getGEOFENCE_TRANSITION_DWELL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_DWELL");
        return com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_DWELL;
    }
    
    /**
     * org.xms.g.location.Geofence.getGEOFENCE_TRANSITION_ENTER() Return the constant value.<br/>
     * com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_ENTER: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-int-geofence_transition_enter">https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-int-geofence_transition_enter</a><br/>
     * 
     * @return Constant Value.The transition type indicating that the user enters the geofence(s)
     */
    public static int getGEOFENCE_TRANSITION_ENTER() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_ENTER");
        return com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_ENTER;
    }
    
    /**
     * org.xms.g.location.Geofence.getGEOFENCE_TRANSITION_EXIT() Return the constant value.<br/>
     * com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_EXIT: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-int-geofence_transition_exit">https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-int-geofence_transition_exit</a><br/>
     * 
     * @return Constant Value.The transition type indicating that the user exits the geofence(s)
     */
    public static int getGEOFENCE_TRANSITION_EXIT() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_EXIT");
        return com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_EXIT;
    }
    
    /**
     * org.xms.g.location.Geofence.getNEVER_EXPIRE() Return the constant value.<br/>
     * com.google.android.gms.location.Geofence.NEVER_EXPIRE: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-long-never_expire">https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-static-final-long-never_expire</a><br/>
     * 
     * @return Constant Value.Expiration value that indicates the geofence should never expire
     */
    public static long getNEVER_EXPIRE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.Geofence.NEVER_EXPIRE");
        return com.google.android.gms.location.Geofence.NEVER_EXPIRE;
    }
    
    /**
     * org.xms.g.location.Geofence.getRequestId() Returns the request ID of this geofence.<br/>
     * com.google.android.gms.location.Geofence..getRequestId(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-abstract-string-getrequestid">https://developers.google.com/android/reference/com/google/android/gms/location/Geofence#public-abstract-string-getrequestid</a><br/>
     * 
     * @return The request ID
     */
    public java.lang.String getRequestId();
    
    default java.lang.Object getZInstanceGeofence() {
        return getGInstanceGeofence();
    }
    
    default com.google.android.gms.location.Geofence getGInstanceGeofence() {
        if (this instanceof org.xms.g.utils.XGettable) {
            return ((com.google.android.gms.location.Geofence) ((org.xms.g.utils.XGettable) this).getGInstance());
        }
        return new com.google.android.gms.location.Geofence() {
            
            public java.lang.String getRequestId() {
                return org.xms.g.location.Geofence.this.getRequestId();
            }
        };
    }
    
    /**
     * org.xms.g.location.Geofence.dynamicCast(java.lang.Object) Dynamic cast the input object to org.xms.g.location.Geofence.<br/>
     * 
     * @param  param0 The input object
     * @return Casted Geofence object
     */
    public static org.xms.g.location.Geofence dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.Geofence) param0);
    }
    
    /**
     * org.xms.g.location.Geofence.isInstance(java.lang.Object) Judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 The input object
     * @return True if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XInterface)) {
            return false;
        }
        if (param0 instanceof org.xms.g.utils.XGettable) {
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.Geofence;
        }
        return param0 instanceof org.xms.g.location.Geofence;
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.location.Geofence {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.String getRequestId() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence) this.getGInstance()).getRequestId()");
            return ((com.google.android.gms.location.Geofence) this.getGInstance()).getRequestId();
        }
    }
    
/**
 * .<br/>
 * Wrapper class for , but only the GMS API are provided.</br>
 * : </br>
 */
    public static final class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
            this.setGInstance(new com.google.android.gms.location.Geofence.Builder());
        }
        
        public final org.xms.g.location.Geofence build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).build()");
            com.google.android.gms.location.Geofence gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).build();
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.XImpl(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setCircularRegion(double param0, double param1, float param2) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setCircularRegion(param0, param1, param2)");
            com.google.android.gms.location.Geofence.Builder gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setCircularRegion(param0, param1, param2);
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setExpirationDuration(long param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setExpirationDuration(param0)");
            com.google.android.gms.location.Geofence.Builder gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setExpirationDuration(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setLoiteringDelay(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setLoiteringDelay(param0)");
            com.google.android.gms.location.Geofence.Builder gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setLoiteringDelay(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setNotificationResponsiveness(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setNotificationResponsiveness(param0)");
            com.google.android.gms.location.Geofence.Builder gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setNotificationResponsiveness(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setRequestId(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setRequestId(param0)");
            com.google.android.gms.location.Geofence.Builder gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setRequestId(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public final org.xms.g.location.Geofence.Builder setTransitionTypes(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setTransitionTypes(param0)");
            com.google.android.gms.location.Geofence.Builder gReturn = ((com.google.android.gms.location.Geofence.Builder) this.getGInstance()).setTransitionTypes(param0);
            return ((gReturn) == null ? null : (new org.xms.g.location.Geofence.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public static org.xms.g.location.Geofence.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.g.location.Geofence.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.Geofence.Builder;
        }
    }
}