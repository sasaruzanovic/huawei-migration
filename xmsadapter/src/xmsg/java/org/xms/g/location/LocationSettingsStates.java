package org.xms.g.location;

/**
 * Stores the current states of all location-related settings..<br/>
 * Wrapper class for com.google.android.gms.location.LocationSettingsStates, but only the GMS API are provided.</br>
 * com.google.android.gms.location.LocationSettingsStates: Stores the current states of all location-related settings.</br>
 */
public final class LocationSettingsStates extends org.xms.g.utils.XObject implements android.os.Parcelable {
    /**
     * android.os.Parcelable.Creator.CREATOR a public CREATOR field that generates instances of your Parcelable class from a Parcel.<br/>
     * <p>
     * com.google.android.gms.location.LocationSettingsStates.CREATOR: <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-static-final-creatorlocationsettingsstates-creator">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-static-final-creatorlocationsettingsstates-creator</a><br/>
     */
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.location.LocationSettingsStates createFromParcel(android.os.Parcel param0) {
            com.google.android.gms.location.LocationSettingsStates gReturn = com.google.android.gms.location.LocationSettingsStates.CREATOR.createFromParcel(param0);
            return new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.g.location.LocationSettingsStates[] newArray(int param0) {
            return new org.xms.g.location.LocationSettingsStates[param0];
        }
    };
    
    /**
     * org.xms.g.location.LocationSettingsStates.LocationSettingsStates(org.xms.g.utils.XBox) constructor of LocationSettingsStates with XBox.<br/>
     * 
     * @param  param0 the wrapper of xms instance
     */
    public LocationSettingsStates(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.fromIntent(android.content.Intent) Retrieves the location settings states from the intent extras.<br/>
     * com.google.android.gms.location.LocationSettingsStates.fromIntent(android.content.Intent): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-static-locationsettingsstates-fromintent-intent-intent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-static-locationsettingsstates-fromintent-intent-intent</a><br/>
     * 
     * @param  param0 Intent instance
     * @return the location settings states
     */
    public static org.xms.g.location.LocationSettingsStates fromIntent(android.content.Intent param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.android.gms.location.LocationSettingsStates.fromIntent(param0)");
        com.google.android.gms.location.LocationSettingsStates gReturn = com.google.android.gms.location.LocationSettingsStates.fromIntent(param0);
        return ((gReturn) == null ? null : (new org.xms.g.location.LocationSettingsStates(new org.xms.g.utils.XBox(gReturn))));
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isBlePresent() Whether BLE is present on the device.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isBlePresent(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isblepresent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isblepresent</a><br/>
     * 
     * @return true if BLE is present on the device
     */
    public final boolean isBlePresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isBlePresent()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isBlePresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isBleUsable() Whether BLE is enabled and is usable by the app.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isBleUsable(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isbleusable">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isbleusable</a><br/>
     * 
     * @return true if BLE is enabled and is usable by the app
     */
    public final boolean isBleUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isBleUsable()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isBleUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isGpsPresent() Whether GPS provider is present on the device.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isGpsPresent(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isgpspresent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isgpspresent</a><br/>
     * 
     * @return true if GPS provider is present on the device
     */
    public final boolean isGpsPresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isGpsPresent()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isGpsPresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isGpsUsable() Whether GPS provider is enabled and is usable by the app.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isGpsUsable(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isgpsusable">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isgpsusable</a><br/>
     * 
     * @return true if GPS provider is enabled and is usable by the app
     */
    public final boolean isGpsUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isGpsUsable()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isGpsUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isLocationPresent() Whether location is present on the device.This method returns true when either GPS or network location provider is present.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isLocationPresent(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-islocationpresent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-islocationpresent</a><br/>
     * 
     * @return true if location is present on the device
     */
    public final boolean isLocationPresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isLocationPresent()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isLocationPresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isLocationUsable() Whether location is enabled and is usable by the app.This method returns true when either GPS or network location provider is usable.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isLocationUsable(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-islocationusable">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-islocationusable</a><br/>
     * 
     * @return true if location is enabled and is usable by the app
     */
    public final boolean isLocationUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isLocationUsable()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isLocationUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isNetworkLocationPresent() Whether network location provider is present on the device.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isNetworkLocationPresent(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isnetworklocationpresent">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isnetworklocationpresent</a><br/>
     * 
     * @return true if network location provider is present on the device
     */
    public final boolean isNetworkLocationPresent() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isNetworkLocationPresent()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isNetworkLocationPresent();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isNetworkLocationUsable() Whether network location provider is enabled and usable by the app.<br/>
     * com.google.android.gms.location.LocationSettingsStates.isNetworkLocationUsable(): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isnetworklocationusable">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-boolean-isnetworklocationusable</a><br/>
     * 
     * @return true if network location provider is enabled and usable by the app
     */
    public final boolean isNetworkLocationUsable() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isNetworkLocationUsable()");
        return ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).isNetworkLocationUsable();
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.writeToParcel(android.os.Parcel,int) Used in serialization and deserialization.<br/>
     * com.google.android.gms.location.LocationSettingsStates.writeToParcel(android.os.Parcel,int): <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-void-writetoparcel-parcel-dest,-int-flags">https://developers.google.com/android/reference/com/google/android/gms/location/LocationSettingsStates#public-void-writetoparcel-parcel-dest,-int-flags</a><br/>
     * 
     * @param  param0 Parcel to which this object is written
     * @param  param1 Writing mode
     */
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.android.gms.location.LocationSettingsStates) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    /**
     * XMS does not provide this api.
     */
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.dynamicCast(java.lang.Object) dynamic cast the input object to org.xms.g.location.LocationSettingsStates.<br/>
     * 
     * @param  param0 the input object
     * @return casted LocationSettingsStates object
     */
    public static org.xms.g.location.LocationSettingsStates dynamicCast(java.lang.Object param0) {
        return ((org.xms.g.location.LocationSettingsStates) param0);
    }
    
    /**
     * org.xms.g.location.LocationSettingsStates.isInstance(java.lang.Object) judge whether the Object is XMS instance or not.<br/>
     * 
     * @param  param0 the input object
     * @return true if the Object is XMS instance, otherwise false
     */
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.android.gms.location.LocationSettingsStates;
    }
}