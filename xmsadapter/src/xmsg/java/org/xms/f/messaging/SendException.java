package org.xms.f.messaging;

public final class SendException extends java.lang.Exception implements org.xms.g.utils.XGettable {
    public java.lang.Object gInstance;
    
    public SendException(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setGInstance(param0.getGInstance());
    }
    
    public static int getERROR_INVALID_PARAMETERS() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.SendException.ERROR_INVALID_PARAMETERS");
        return com.google.firebase.messaging.SendException.ERROR_INVALID_PARAMETERS;
    }
    
    public static int getERROR_SIZE() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.SendException.ERROR_SIZE");
        return com.google.firebase.messaging.SendException.ERROR_SIZE;
    }
    
    public static int getERROR_TOO_MANY_MESSAGES() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.SendException.ERROR_TOO_MANY_MESSAGES");
        return com.google.firebase.messaging.SendException.ERROR_TOO_MANY_MESSAGES;
    }
    
    public static int getERROR_TTL_EXCEEDED() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.SendException.ERROR_TTL_EXCEEDED");
        return com.google.firebase.messaging.SendException.ERROR_TTL_EXCEEDED;
    }
    
    public static int getERROR_UNKNOWN() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.SendException.ERROR_UNKNOWN");
        return com.google.firebase.messaging.SendException.ERROR_UNKNOWN;
    }
    
    public final int getErrorCode() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.SendException) this.getGInstance()).getErrorCode()");
        return ((com.google.firebase.messaging.SendException) this.getGInstance()).getErrorCode();
    }
    
    public void setGInstance(java.lang.Object param0) {
        this.gInstance = param0;
    }
    
    public java.lang.Object getGInstance() {
        return this.gInstance;
    }
    
    public static org.xms.f.messaging.SendException dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.SendException) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.SendException;
    }
}