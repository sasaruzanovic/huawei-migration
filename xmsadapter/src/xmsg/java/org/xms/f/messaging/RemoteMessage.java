package org.xms.f.messaging;

public final class RemoteMessage extends org.xms.g.utils.XObject implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.f.messaging.RemoteMessage createFromParcel(android.os.Parcel param0) {
            com.google.firebase.messaging.RemoteMessage gReturn = com.google.firebase.messaging.RemoteMessage.CREATOR.createFromParcel(param0);
            return new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(gReturn));
        }
        
        public org.xms.f.messaging.RemoteMessage[] newArray(int param0) {
            return new org.xms.f.messaging.RemoteMessage[param0];
        }
    };
    
    public RemoteMessage(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static int getPRIORITY_HIGH() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.RemoteMessage.PRIORITY_HIGH");
        return com.google.firebase.messaging.RemoteMessage.PRIORITY_HIGH;
    }
    
    public static int getPRIORITY_NORMAL() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.RemoteMessage.PRIORITY_NORMAL");
        return com.google.firebase.messaging.RemoteMessage.PRIORITY_NORMAL;
    }
    
    public static int getPRIORITY_UNKNOWN() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.RemoteMessage.PRIORITY_UNKNOWN");
        return com.google.firebase.messaging.RemoteMessage.PRIORITY_UNKNOWN;
    }
    
    public final java.lang.String getCollapseKey() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getCollapseKey()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getCollapseKey();
    }
    
    public final java.util.Map<java.lang.String, java.lang.String> getData() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getData()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getData();
    }
    
    public final java.lang.String getFrom() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getFrom()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getFrom();
    }
    
    public final java.lang.String getMessageId() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageId()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageId();
    }
    
    public final java.lang.String getMessageType() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageType()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageType();
    }
    
    public org.xms.f.messaging.RemoteMessage.Notification getNotification() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getNotification()");
        com.google.firebase.messaging.RemoteMessage.Notification gReturn = ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getNotification();
        return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Notification(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public int getOriginalPriority() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getOriginalPriority()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getOriginalPriority();
    }
    
    public int getPriority() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getPriority()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getPriority();
    }
    
    public long getSentTime() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getSentTime()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getSentTime();
    }
    
    public final java.lang.String getTo() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTo()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTo();
    }
    
    public int getTtl() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTtl()");
        return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTtl();
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).writeToParcel(param0, param1)");
        ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).writeToParcel(param0, param1);
    }
    
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.f.messaging.RemoteMessage dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.RemoteMessage) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.RemoteMessage;
    }
    
    public static class Builder extends org.xms.g.utils.XObject {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder(java.lang.String param0) {
            super(((org.xms.g.utils.XBox) null));
            this.setGInstance(new com.google.firebase.messaging.RemoteMessage.Builder(param0));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder addData(java.lang.String param0, java.lang.String param1) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).addData(param0, param1)");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).addData(param0, param1);
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage build() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).build()");
            com.google.firebase.messaging.RemoteMessage gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).build();
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder clearData() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).clearData()");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).clearData();
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setCollapseKey(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setCollapseKey(param0)");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setCollapseKey(param0);
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setData(java.util.Map<java.lang.String, java.lang.String> param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setData(param0)");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setData(param0);
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setMessageId(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageId(param0)");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageId(param0);
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setMessageType(java.lang.String param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageType(param0)");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageType(param0);
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setTtl(int param0) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setTtl(param0)");
            com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setTtl(param0);
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn))));
        }
        
        public static org.xms.f.messaging.RemoteMessage.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.messaging.RemoteMessage.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.RemoteMessage.Builder;
        }
    }
    
    public static class Notification extends org.xms.g.utils.XObject {
        
        public Notification(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.String getBody() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBody()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBody();
        }
        
        public java.lang.String[] getBodyLocalizationArgs() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationArgs()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationArgs();
        }
        
        public java.lang.String getBodyLocalizationKey() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationKey()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationKey();
        }
        
        public java.lang.String getChannelId() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getChannelId()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getChannelId();
        }
        
        public java.lang.String getClickAction() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getClickAction()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getClickAction();
        }
        
        public java.lang.String getColor() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getColor()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getColor();
        }
        
        public boolean getDefaultLightSettings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultLightSettings()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultLightSettings();
        }
        
        public boolean getDefaultSound() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultSound()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultSound();
        }
        
        public boolean getDefaultVibrateSettings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultVibrateSettings()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultVibrateSettings();
        }
        
        public java.lang.Long getEventTime() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getEventTime()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getEventTime();
        }
        
        public java.lang.String getIcon() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getIcon()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getIcon();
        }
        
        public android.net.Uri getImageUrl() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getImageUrl()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getImageUrl();
        }
        
        public int[] getLightSettings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLightSettings()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLightSettings();
        }
        
        public android.net.Uri getLink() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLink()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLink();
        }
        
        public boolean getLocalOnly() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLocalOnly()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLocalOnly();
        }
        
        public java.lang.Integer getNotificationCount() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationCount()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationCount();
        }
        
        public java.lang.Integer getNotificationPriority() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationPriority()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationPriority();
        }
        
        public java.lang.String getSound() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSound()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSound();
        }
        
        public boolean getSticky() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSticky()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSticky();
        }
        
        public java.lang.String getTag() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTag()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTag();
        }
        
        public java.lang.String getTicker() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTicker()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTicker();
        }
        
        public java.lang.String getTitle() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitle()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitle();
        }
        
        public java.lang.String[] getTitleLocalizationArgs() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationArgs()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationArgs();
        }
        
        public java.lang.String getTitleLocalizationKey() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationKey()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationKey();
        }
        
        public long[] getVibrateTimings() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVibrateTimings()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVibrateTimings();
        }
        
        public java.lang.Integer getVisibility() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVisibility()");
            return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVisibility();
        }
        
        public static org.xms.f.messaging.RemoteMessage.Notification dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.messaging.RemoteMessage.Notification) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.RemoteMessage.Notification;
        }
    }
}