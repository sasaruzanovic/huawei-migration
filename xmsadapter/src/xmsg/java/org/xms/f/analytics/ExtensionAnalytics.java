package org.xms.f.analytics;

public final class ExtensionAnalytics extends org.xms.g.utils.XObject {
    
    public ExtensionAnalytics(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public org.xms.g.tasks.Task<java.lang.String> getAppInstanceId() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).getAppInstanceId()");
        com.google.android.gms.tasks.Task gReturn = ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).getAppInstanceId();
        return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public static org.xms.f.analytics.ExtensionAnalytics getInstance(android.content.Context param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.getInstance(param0)");
        com.google.firebase.analytics.FirebaseAnalytics gReturn = com.google.firebase.analytics.FirebaseAnalytics.getInstance(param0);
        return ((gReturn) == null ? null : (new org.xms.f.analytics.ExtensionAnalytics(new org.xms.g.utils.XBox(gReturn))));
    }
    
    public final void logEvent(java.lang.String param0, android.os.Bundle param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).logEvent(param0, param1)");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).logEvent(param0, param1);
    }
    
    public final void resetAnalyticsData() {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).resetAnalyticsData()");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).resetAnalyticsData();
    }
    
    public final void setAnalyticsCollectionEnabled(boolean param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setAnalyticsCollectionEnabled(param0)");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setAnalyticsCollectionEnabled(param0);
    }
    
    public final void setCurrentScreen(android.app.Activity param0, java.lang.String param1, java.lang.String param2) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setMinimumSessionDuration(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setMinimumSessionDuration(param0)");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setMinimumSessionDuration(param0);
    }
    
    public final void setSessionTimeoutDuration(long param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setSessionTimeoutDuration(param0)");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setSessionTimeoutDuration(param0);
    }
    
    public final void setUserId(java.lang.String param0) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserId(param0)");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserId(param0);
    }
    
    public final void setUserProperty(java.lang.String param0, java.lang.String param1) {
        org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserProperty(param0, param1)");
        ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserProperty(param0, param1);
    }
    
    public static org.xms.f.analytics.ExtensionAnalytics dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.analytics.ExtensionAnalytics) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics;
    }
    
    public static class Event extends org.xms.g.utils.XObject {
        
        public Event(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected Event() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getADD_PAYMENT_INFO() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_PAYMENT_INFO");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_PAYMENT_INFO;
        }
        
        public static java.lang.String getADD_TO_CART() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_CART");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_CART;
        }
        
        public static java.lang.String getADD_TO_WISHLIST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_WISHLIST");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_WISHLIST;
        }
        
        public static java.lang.String getAPP_OPEN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.APP_OPEN");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.APP_OPEN;
        }
        
        public static java.lang.String getBEGIN_CHECKOUT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.BEGIN_CHECKOUT");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.BEGIN_CHECKOUT;
        }
        
        public static java.lang.String getCAMPAIGN_DETAILS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.CAMPAIGN_DETAILS");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.CAMPAIGN_DETAILS;
        }
        
        public static java.lang.String getCHECKOUT_PROGRESS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.CHECKOUT_PROGRESS");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.CHECKOUT_PROGRESS;
        }
        
        public static java.lang.String getEARN_VIRTUAL_CURRENCY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY;
        }
        
        public static java.lang.String getECOMMERCE_PURCHASE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ECOMMERCE_PURCHASE");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.ECOMMERCE_PURCHASE;
        }
        
        public static java.lang.String getGENERATE_LEAD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.GENERATE_LEAD");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.GENERATE_LEAD;
        }
        
        public static java.lang.String getJOIN_GROUP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.JOIN_GROUP");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.JOIN_GROUP;
        }
        
        public static java.lang.String getLEVEL_END() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_END");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_END;
        }
        
        public static java.lang.String getLEVEL_START() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_START");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_START;
        }
        
        public static java.lang.String getLEVEL_UP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_UP");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_UP;
        }
        
        public static java.lang.String getLOGIN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LOGIN");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.LOGIN;
        }
        
        public static java.lang.String getPOST_SCORE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.POST_SCORE");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.POST_SCORE;
        }
        
        public static java.lang.String getPRESENT_OFFER() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.PRESENT_OFFER");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.PRESENT_OFFER;
        }
        
        public static java.lang.String getPURCHASE_REFUND() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.PURCHASE_REFUND");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.PURCHASE_REFUND;
        }
        
        public static java.lang.String getREMOVE_FROM_CART() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.REMOVE_FROM_CART");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.REMOVE_FROM_CART;
        }
        
        public static java.lang.String getSEARCH() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SEARCH");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.SEARCH;
        }
        
        public static java.lang.String getSELECT_CONTENT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SELECT_CONTENT");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.SELECT_CONTENT;
        }
        
        public static java.lang.String getSET_CHECKOUT_OPTION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SET_CHECKOUT_OPTION");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.SET_CHECKOUT_OPTION;
        }
        
        public static java.lang.String getSHARE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SHARE");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.SHARE;
        }
        
        public static java.lang.String getSIGN_UP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SIGN_UP");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.SIGN_UP;
        }
        
        public static java.lang.String getSPEND_VIRTUAL_CURRENCY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY;
        }
        
        public static java.lang.String getTUTORIAL_BEGIN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_BEGIN");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_BEGIN;
        }
        
        public static java.lang.String getTUTORIAL_COMPLETE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_COMPLETE");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_COMPLETE;
        }
        
        public static java.lang.String getUNLOCK_ACHIEVEMENT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT;
        }
        
        public static java.lang.String getVIEW_ITEM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM;
        }
        
        public static java.lang.String getVIEW_ITEM_LIST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM_LIST");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM_LIST;
        }
        
        public static java.lang.String getVIEW_SEARCH_RESULTS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS");
            return com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS;
        }
        
        public static java.lang.String getADD_SHIPPING_INFO() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPURCHASE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getREFUND() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSELECT_ITEM() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSELECT_PROMOTION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getVIEW_CART() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getVIEW_PROMOTION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.Event dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.Event) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics.Event;
        }
    }
    
    public static class Param extends org.xms.g.utils.XObject {
        
        public Param(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected Param() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getACHIEVEMENT_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ACHIEVEMENT_ID");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ACHIEVEMENT_ID;
        }
        
        public static java.lang.String getACLID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ACLID");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ACLID;
        }
        
        public static java.lang.String getAFFILIATION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.AFFILIATION");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.AFFILIATION;
        }
        
        public static java.lang.String getCAMPAIGN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CAMPAIGN");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CAMPAIGN;
        }
        
        public static java.lang.String getCHARACTER() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CHARACTER");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CHARACTER;
        }
        
        public static java.lang.String getCHECKOUT_OPTION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_OPTION");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_OPTION;
        }
        
        public static java.lang.String getCHECKOUT_STEP() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_STEP");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_STEP;
        }
        
        public static java.lang.String getCONTENT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT;
        }
        
        public static java.lang.String getCONTENT_TYPE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE;
        }
        
        public static java.lang.String getCOUPON() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.COUPON");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.COUPON;
        }
        
        public static java.lang.String getCP1() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CP1");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CP1;
        }
        
        public static java.lang.String getCREATIVE_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_NAME");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_NAME;
        }
        
        public static java.lang.String getCREATIVE_SLOT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_SLOT");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_SLOT;
        }
        
        public static java.lang.String getCURRENCY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CURRENCY");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.CURRENCY;
        }
        
        public static java.lang.String getDESTINATION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.DESTINATION");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.DESTINATION;
        }
        
        public static java.lang.String getEND_DATE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.END_DATE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.END_DATE;
        }
        
        public static java.lang.String getEXTEND_SESSION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getFLIGHT_NUMBER() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.FLIGHT_NUMBER");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.FLIGHT_NUMBER;
        }
        
        public static java.lang.String getGROUP_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.GROUP_ID");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.GROUP_ID;
        }
        
        public static java.lang.String getINDEX() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.INDEX");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.INDEX;
        }
        
        public static java.lang.String getITEM_BRAND() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_BRAND");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_BRAND;
        }
        
        public static java.lang.String getITEM_CATEGORY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_CATEGORY");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_CATEGORY;
        }
        
        public static java.lang.String getITEM_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID;
        }
        
        public static java.lang.String getITEM_LIST() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LIST");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LIST;
        }
        
        public static java.lang.String getITEM_LOCATION_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LOCATION_ID");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LOCATION_ID;
        }
        
        public static java.lang.String getITEM_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_NAME");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_NAME;
        }
        
        public static java.lang.String getITEM_VARIANT() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_VARIANT");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_VARIANT;
        }
        
        public static java.lang.String getLEVEL() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL;
        }
        
        public static java.lang.String getLEVEL_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL_NAME");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL_NAME;
        }
        
        public static java.lang.String getLOCATION() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.LOCATION");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.LOCATION;
        }
        
        public static java.lang.String getMEDIUM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.MEDIUM");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.MEDIUM;
        }
        
        public static java.lang.String getMETHOD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.METHOD");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.METHOD;
        }
        
        public static java.lang.String getNUMBER_OF_NIGHTS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_NIGHTS");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_NIGHTS;
        }
        
        public static java.lang.String getNUMBER_OF_PASSENGERS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_PASSENGERS");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_PASSENGERS;
        }
        
        public static java.lang.String getNUMBER_OF_ROOMS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_ROOMS");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_ROOMS;
        }
        
        public static java.lang.String getORIGIN() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ORIGIN");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.ORIGIN;
        }
        
        public static java.lang.String getPRICE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE;
        }
        
        public static java.lang.String getQUANTITY() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.QUANTITY");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.QUANTITY;
        }
        
        public static java.lang.String getSCORE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SCORE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.SCORE;
        }
        
        public static java.lang.String getSEARCH_TERM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SEARCH_TERM");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.SEARCH_TERM;
        }
        
        public static java.lang.String getSHIPPING() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SHIPPING");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.SHIPPING;
        }
        
        public static java.lang.String getSIGN_UP_METHOD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SIGN_UP_METHOD");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.SIGN_UP_METHOD;
        }
        
        public static java.lang.String getSOURCE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SOURCE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.SOURCE;
        }
        
        public static java.lang.String getSTART_DATE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.START_DATE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.START_DATE;
        }
        
        public static java.lang.String getSUCCESS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SUCCESS");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.SUCCESS;
        }
        
        public static java.lang.String getTAX() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TAX");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.TAX;
        }
        
        public static java.lang.String getTERM() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TERM");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.TERM;
        }
        
        public static java.lang.String getTRANSACTION_ID() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TRANSACTION_ID");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.TRANSACTION_ID;
        }
        
        public static java.lang.String getTRAVEL_CLASS() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TRAVEL_CLASS");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.TRAVEL_CLASS;
        }
        
        public static java.lang.String getVALUE() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.VALUE");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.VALUE;
        }
        
        public static java.lang.String getVIRTUAL_CURRENCY_NAME() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME");
            return com.google.firebase.analytics.FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME;
        }
        
        public static java.lang.String getDISCOUNT() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY2() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY3() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY4() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY5() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_LIST_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_LIST_NAME() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEMS() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getLOCATION_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPAYMENT_TYPE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPROMOTION_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPROMOTION_NAME() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSHIPPING_TIER() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.Param dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.Param) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics.Param;
        }
    }
    
    public static class UserProperty extends org.xms.g.utils.XObject {
        
        public UserProperty(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected UserProperty() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getALLOW_AD_PERSONALIZATION_SIGNALS() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSIGN_UP_METHOD() {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.UserProperty.SIGN_UP_METHOD");
            return com.google.firebase.analytics.FirebaseAnalytics.UserProperty.SIGN_UP_METHOD;
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.UserProperty dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.UserProperty) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics.UserProperty;
        }
    }
}