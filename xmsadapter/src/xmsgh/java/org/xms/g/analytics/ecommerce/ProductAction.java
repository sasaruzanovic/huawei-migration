package org.xms.g.analytics.ecommerce;

public class ProductAction extends org.xms.g.utils.XObject {
    
    public ProductAction(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public ProductAction(java.lang.String param0) {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public static java.lang.String getACTION_ADD() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_CHECKOUT() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_CHECKOUT_OPTION() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_CHECKOUT_OPTIONS() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_CLICK() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_DETAIL() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_PURCHASE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_REFUND() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static java.lang.String getACTION_REMOVE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setCheckoutOptions(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setCheckoutStep(int param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setProductActionList(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setProductListSource(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setTransactionAffiliation(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setTransactionCouponCode(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setTransactionId(java.lang.String param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setTransactionRevenue(double param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setTransactionShipping(double param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public org.xms.g.analytics.ecommerce.ProductAction setTransactionTax(double param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String toString() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.ecommerce.ProductAction dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}