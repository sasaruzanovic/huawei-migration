package org.xms.g.analytics;

public class HitBuilders extends org.xms.g.utils.XObject {
    
    public HitBuilders(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public HitBuilders() {
        super(((org.xms.g.utils.XBox) null));
    }
    
    public static org.xms.g.analytics.HitBuilders dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class AppViewBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public AppViewBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public AppViewBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static org.xms.g.analytics.HitBuilders.AppViewBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class EventBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public EventBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public EventBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public EventBuilder(java.lang.String param0, java.lang.String param1) {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.analytics.HitBuilders.EventBuilder setAction(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.EventBuilder setCategory(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.EventBuilder setLabel(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.EventBuilder setValue(long param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.EventBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class ExceptionBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public ExceptionBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public ExceptionBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.analytics.HitBuilders.ExceptionBuilder setDescription(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ExceptionBuilder setFatal(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.ExceptionBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class HitBuilder<XT extends org.xms.g.analytics.HitBuilders.HitBuilder> extends org.xms.g.utils.XObject {
        
        public HitBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected HitBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public XT addImpression(org.xms.g.analytics.ecommerce.Product param0, java.lang.String param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT addProduct(org.xms.g.analytics.ecommerce.Product param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT addPromotion(org.xms.g.analytics.ecommerce.Promotion param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public java.util.Map<java.lang.String, java.lang.String> build() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public final XT set(java.lang.String param0, java.lang.String param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public final XT setAll(java.util.Map<java.lang.String, java.lang.String> param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setCampaignParamsFromUrl(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setCustomDimension(int param0, java.lang.String param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setCustomMetric(int param0, float param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setNewSession() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setNonInteraction(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setProductAction(org.xms.g.analytics.ecommerce.ProductAction param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public XT setPromotionAction(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        protected java.lang.String get(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        protected XT setHitType(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.HitBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class ItemBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public ItemBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public ItemBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setCategory(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setCurrencyCode(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setName(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setPrice(double param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setQuantity(long param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setSku(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.ItemBuilder setTransactionId(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.ItemBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class ScreenViewBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public ScreenViewBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public ScreenViewBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static org.xms.g.analytics.HitBuilders.ScreenViewBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class SocialBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public SocialBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public SocialBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.analytics.HitBuilders.SocialBuilder setAction(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.SocialBuilder setNetwork(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.SocialBuilder setTarget(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.SocialBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class TimingBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public TimingBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public TimingBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public TimingBuilder(java.lang.String param0, java.lang.String param1, long param2) {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.analytics.HitBuilders.TimingBuilder setCategory(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TimingBuilder setLabel(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TimingBuilder setValue(long param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TimingBuilder setVariable(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.TimingBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
    
    public static class TransactionBuilder extends org.xms.g.analytics.HitBuilders.HitBuilder {
        
        public TransactionBuilder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public TransactionBuilder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.analytics.HitBuilders.TransactionBuilder setAffiliation(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TransactionBuilder setCurrencyCode(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TransactionBuilder setRevenue(double param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TransactionBuilder setShipping(double param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TransactionBuilder setTax(double param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.analytics.HitBuilders.TransactionBuilder setTransactionId(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.analytics.HitBuilders.TransactionBuilder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}