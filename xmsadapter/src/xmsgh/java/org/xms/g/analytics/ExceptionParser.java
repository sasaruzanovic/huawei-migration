package org.xms.g.analytics;

public interface ExceptionParser extends org.xms.g.utils.XInterface {
    
    public java.lang.String getDescription(java.lang.String param0, java.lang.Throwable param1);
    
    default java.lang.Object getZInstanceExceptionParser() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    default com.google.android.gms.analytics.ExceptionParser getGInstanceExceptionParser() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    default java.lang.Object getHInstanceExceptionParser() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.analytics.ExceptionParser dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class XImpl extends org.xms.g.utils.XObject implements org.xms.g.analytics.ExceptionParser {
        
        public XImpl(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.String getDescription(java.lang.String param0, java.lang.Throwable param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}