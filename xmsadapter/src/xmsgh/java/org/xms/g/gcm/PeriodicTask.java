package org.xms.g.gcm;

public class PeriodicTask extends org.xms.g.gcm.Task {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.gcm.PeriodicTask createFromParcel(android.os.Parcel param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask[] newArray(int param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    };
    
    public PeriodicTask(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public long getMFlexInSeconds() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public long getMIntervalInSeconds() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public long getFlex() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public long getPeriod() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void toBundle(android.os.Bundle param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String toString() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.gcm.PeriodicTask dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class Builder extends org.xms.g.gcm.Task.Builder {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public org.xms.g.gcm.PeriodicTask build() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setExtras(android.os.Bundle param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setFlex(long param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setPeriod(long param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setPersisted(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setRequiredNetwork(int param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setRequiresCharging(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setService(java.lang.Class<? extends org.xms.g.gcm.GcmTaskService> param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setTag(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.PeriodicTask.Builder setUpdateCurrent(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        protected void checkConditions() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.gcm.PeriodicTask.Builder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}