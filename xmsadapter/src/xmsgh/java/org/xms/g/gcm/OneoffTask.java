package org.xms.g.gcm;

public class OneoffTask extends org.xms.g.gcm.Task {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.g.gcm.OneoffTask createFromParcel(android.os.Parcel param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask[] newArray(int param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    };
    
    public OneoffTask(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public long getWindowEnd() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public long getWindowStart() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void toBundle(android.os.Bundle param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String toString() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.gcm.OneoffTask dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static class Builder extends org.xms.g.gcm.Task.Builder {
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        protected void checkConditions() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask build() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setExecutionWindow(long param0, long param1) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setExtras(android.os.Bundle param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setPersisted(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setRequiredNetwork(int param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setRequiresCharging(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setService(java.lang.Class<? extends org.xms.g.gcm.GcmTaskService> param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setTag(java.lang.String param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public org.xms.g.gcm.OneoffTask.Builder setUpdateCurrent(boolean param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.g.gcm.OneoffTask.Builder dynamicCast(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            throw new java.lang.RuntimeException("Not Supported");
        }
    }
}