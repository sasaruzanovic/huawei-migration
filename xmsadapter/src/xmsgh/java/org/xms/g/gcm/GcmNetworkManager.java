package org.xms.g.gcm;

public class GcmNetworkManager extends org.xms.g.utils.XObject {
    
    public GcmNetworkManager(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static int getRESULT_FAILURE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getRESULT_RESCHEDULE() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static int getRESULT_SUCCESS() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void cancelAllTasks(java.lang.Class<? extends org.xms.g.gcm.GcmTaskService> param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void cancelTask(java.lang.String param0, java.lang.Class<? extends org.xms.g.gcm.GcmTaskService> param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.gcm.GcmNetworkManager getInstance(android.content.Context param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public synchronized void schedule(org.xms.g.gcm.Task param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.g.gcm.GcmNetworkManager dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}