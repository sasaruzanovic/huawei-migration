package org.xms.g.measurement;

public class AppMeasurementContentProvider extends android.content.ContentProvider implements org.xms.g.utils.XGettable {
    public java.lang.Object gInstance;
    public java.lang.Object hInstance;
    
    public AppMeasurementContentProvider(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setGInstance(param0.getGInstance());
        this.setHInstance(param0.getHInstance());
    }
    
    public AppMeasurementContentProvider() {
    }
    
    public void attachInfo(android.content.Context param0, android.content.pm.ProviderInfo param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public int delete(android.net.Uri param0, java.lang.String param1, java.lang.String[] param2) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public java.lang.String getType(android.net.Uri param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public android.net.Uri insert(android.net.Uri param0, android.content.ContentValues param1) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public boolean onCreate() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public android.database.Cursor query(android.net.Uri param0, java.lang.String[] param1, java.lang.String param2, java.lang.String[] param3, java.lang.String param4) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public int update(android.net.Uri param0, android.content.ContentValues param1, java.lang.String param2, java.lang.String[] param3) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setGInstance(java.lang.Object param0) {
        this.gInstance = param0;
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getGInstance() {
        return this.gInstance;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.measurement.AppMeasurementContentProvider dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}