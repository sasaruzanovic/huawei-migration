package org.xms.g.measurement;

public final class AppMeasurementService extends android.app.Service implements org.xms.g.utils.XGettable {
    public java.lang.Object gInstance;
    public java.lang.Object hInstance;
    
    public AppMeasurementService(org.xms.g.utils.XBox param0) {
        if (param0 == null) {
            return;
        }
        this.setGInstance(param0.getGInstance());
        this.setHInstance(param0.getHInstance());
    }
    
    public AppMeasurementService() {
    }
    
    public final android.os.IBinder onBind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void onCreate() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void onDestroy() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void onRebind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final int onStartCommand(android.content.Intent param0, int param1, int param2) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final boolean onUnbind(android.content.Intent param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public void setGInstance(java.lang.Object param0) {
        this.gInstance = param0;
    }
    
    public void setHInstance(java.lang.Object param0) {
        this.hInstance = param0;
    }
    
    public java.lang.Object getGInstance() {
        return this.gInstance;
    }
    
    public java.lang.Object getHInstance() {
        return this.hInstance;
    }
    
    public static org.xms.g.measurement.AppMeasurementService dynamicCast(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        throw new java.lang.RuntimeException("Not Supported");
    }
}