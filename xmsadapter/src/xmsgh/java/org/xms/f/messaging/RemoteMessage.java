package org.xms.f.messaging;

public final class RemoteMessage extends org.xms.g.utils.XObject implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() {
        
        public org.xms.f.messaging.RemoteMessage createFromParcel(android.os.Parcel param0) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                com.huawei.hms.push.RemoteMessage hReturn = com.huawei.hms.push.RemoteMessage.CREATOR.createFromParcel(param0);
                return new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(null, hReturn));
            } else {
                com.google.firebase.messaging.RemoteMessage gReturn = com.google.firebase.messaging.RemoteMessage.CREATOR.createFromParcel(param0);
                return new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(gReturn, null));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage[] newArray(int param0) {
            return new org.xms.f.messaging.RemoteMessage[param0];
        }
    };
    
    public RemoteMessage(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public static int getPRIORITY_HIGH() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.RemoteMessage.PRIORITY_HIGH");
            return com.huawei.hms.push.RemoteMessage.PRIORITY_HIGH;
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.RemoteMessage.PRIORITY_HIGH");
            return com.google.firebase.messaging.RemoteMessage.PRIORITY_HIGH;
        }
    }
    
    public static int getPRIORITY_NORMAL() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.RemoteMessage.PRIORITY_NORMAL");
            return com.huawei.hms.push.RemoteMessage.PRIORITY_NORMAL;
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.RemoteMessage.PRIORITY_NORMAL");
            return com.google.firebase.messaging.RemoteMessage.PRIORITY_NORMAL;
        }
    }
    
    public static int getPRIORITY_UNKNOWN() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.push.RemoteMessage.PRIORITY_UNKNOWN");
            return com.huawei.hms.push.RemoteMessage.PRIORITY_UNKNOWN;
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.messaging.RemoteMessage.PRIORITY_UNKNOWN");
            return com.google.firebase.messaging.RemoteMessage.PRIORITY_UNKNOWN;
        }
    }
    
    public final java.lang.String getCollapseKey() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getCollapseKey()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getCollapseKey();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getCollapseKey()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getCollapseKey();
        }
    }
    
    public final java.util.Map<java.lang.String, java.lang.String> getData() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getDataOfMap()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getDataOfMap();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getData()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getData();
        }
    }
    
    public final java.lang.String getFrom() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getFrom()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getFrom();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getFrom()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getFrom();
        }
    }
    
    public final java.lang.String getMessageId() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageId()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageId();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageId()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageId();
        }
    }
    
    public final java.lang.String getMessageType() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageType()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getMessageType();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageType()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getMessageType();
        }
    }
    
    public org.xms.f.messaging.RemoteMessage.Notification getNotification() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getNotification()");
            com.huawei.hms.push.RemoteMessage.Notification hReturn = ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getNotification();
            return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Notification(new org.xms.g.utils.XBox(null, hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getNotification()");
            com.google.firebase.messaging.RemoteMessage.Notification gReturn = ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getNotification();
            return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Notification(new org.xms.g.utils.XBox(gReturn, null))));
        }
    }
    
    public int getOriginalPriority() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getOriginalUrgency()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getOriginalUrgency();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getOriginalPriority()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getOriginalPriority();
        }
    }
    
    public int getPriority() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getUrgency()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getUrgency();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getPriority()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getPriority();
        }
    }
    
    public long getSentTime() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getSentTime()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getSentTime();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getSentTime()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getSentTime();
        }
    }
    
    public final java.lang.String getTo() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTo()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTo();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTo()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTo();
        }
    }
    
    public int getTtl() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTtl()");
            return ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).getTtl();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTtl()");
            return ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).getTtl();
        }
    }
    
    public void writeToParcel(android.os.Parcel param0, int param1) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage) this.getHInstance()).writeToParcel(param0, param1)");
            ((com.huawei.hms.push.RemoteMessage) this.getHInstance()).writeToParcel(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).writeToParcel(param0, param1)");
            ((com.google.firebase.messaging.RemoteMessage) this.getGInstance()).writeToParcel(param0, param1);
        }
    }
    
    public int describeContents() {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public static org.xms.f.messaging.RemoteMessage dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.messaging.RemoteMessage) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.RemoteMessage;
        } else {
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.RemoteMessage;
        }
    }
    
    public static class Builder extends org.xms.g.utils.XObject {
        
        
        
        public Builder(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public Builder(java.lang.String param0) {
            super(((org.xms.g.utils.XBox) null));
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                
                this.setHInstance(new com.huawei.hms.push.RemoteMessage.Builder("push.hcm.upstream"));
            } else {
                this.setGInstance(new com.google.firebase.messaging.RemoteMessage.Builder(param0));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder addData(java.lang.String param0, java.lang.String param1) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).addData(param0, param1)");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).addData(param0, param1);
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).addData(param0, param1)");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).addData(param0, param1);
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage build() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).build()");
                com.huawei.hms.push.RemoteMessage hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).build();
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).build()");
                com.google.firebase.messaging.RemoteMessage gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).build();
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder clearData() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).clearData()");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).clearData();
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).clearData()");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).clearData();
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setCollapseKey(java.lang.String param0) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setCollapseKey(param0)");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setCollapseKey(param0);
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setCollapseKey(param0)");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setCollapseKey(param0);
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setData(java.util.Map<java.lang.String, java.lang.String> param0) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setData(param0)");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setData(param0);
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setData(param0)");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setData(param0);
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setMessageId(java.lang.String param0) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageId(param0)");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageId(param0);
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageId(param0)");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageId(param0);
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setMessageType(java.lang.String param0) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageType(param0)");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setMessageType(param0);
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageType(param0)");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setMessageType(param0);
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public org.xms.f.messaging.RemoteMessage.Builder setTtl(int param0) {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setTtl(param0)");
                com.huawei.hms.push.RemoteMessage.Builder hReturn = ((com.huawei.hms.push.RemoteMessage.Builder) this.getHInstance()).setTtl(param0);
                return ((hReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(null, hReturn))));
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setTtl(param0)");
                com.google.firebase.messaging.RemoteMessage.Builder gReturn = ((com.google.firebase.messaging.RemoteMessage.Builder) this.getGInstance()).setTtl(param0);
                return ((gReturn) == null ? null : (new org.xms.f.messaging.RemoteMessage.Builder(new org.xms.g.utils.XBox(gReturn, null))));
            }
        }
        
        public static org.xms.f.messaging.RemoteMessage.Builder dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.messaging.RemoteMessage.Builder) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.RemoteMessage.Builder;
            } else {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.RemoteMessage.Builder;
            }
        }
    }
    
    public static class Notification extends org.xms.g.utils.XObject {
        
        public Notification(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        public java.lang.String getBody() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBody()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBody();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBody()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBody();
            }
        }
        
        public java.lang.String[] getBodyLocalizationArgs() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationArgs()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationArgs();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationArgs()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationArgs();
            }
        }
        
        public java.lang.String getBodyLocalizationKey() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationKey()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBodyLocalizationKey();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationKey()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getBodyLocalizationKey();
            }
        }
        
        public java.lang.String getChannelId() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getChannelId()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getChannelId();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getChannelId()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getChannelId();
            }
        }
        
        public java.lang.String getClickAction() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getClickAction()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getClickAction();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getClickAction()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getClickAction();
            }
        }
        
        public java.lang.String getColor() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getColor()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getColor();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getColor()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getColor();
            }
        }
        
        public boolean getDefaultLightSettings() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultLight()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultLight();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultLightSettings()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultLightSettings();
            }
        }
        
        public boolean getDefaultSound() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultSound()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultSound();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultSound()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultSound();
            }
        }
        
        public boolean getDefaultVibrateSettings() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultVibrate()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isDefaultVibrate();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultVibrateSettings()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getDefaultVibrateSettings();
            }
        }
        
        public java.lang.Long getEventTime() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getWhen()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getWhen();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getEventTime()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getEventTime();
            }
        }
        
        public java.lang.String getIcon() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getIcon()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getIcon();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getIcon()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getIcon();
            }
        }
        
        public android.net.Uri getImageUrl() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImageUrl()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImageUrl();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getImageUrl()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getImageUrl();
            }
        }
        
        public int[] getLightSettings() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLightSettings()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLightSettings();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLightSettings()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLightSettings();
            }
        }
        
        public android.net.Uri getLink() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLink()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getLink();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLink()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLink();
            }
        }
        
        public boolean getLocalOnly() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isLocalOnly()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isLocalOnly();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLocalOnly()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getLocalOnly();
            }
        }
        
        public java.lang.Integer getNotificationCount() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBadgeNumber()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getBadgeNumber();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationCount()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationCount();
            }
        }
        
        public java.lang.Integer getNotificationPriority() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImportance()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getImportance();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationPriority()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getNotificationPriority();
            }
        }
        
        public java.lang.String getSound() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getSound()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getSound();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSound()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSound();
            }
        }
        
        public boolean getSticky() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isAutoCancel()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).isAutoCancel();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSticky()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getSticky();
            }
        }
        
        public java.lang.String getTag() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTag()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTag();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTag()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTag();
            }
        }
        
        public java.lang.String getTicker() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTicker()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTicker();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTicker()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTicker();
            }
        }
        
        public java.lang.String getTitle() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitle()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitle();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitle()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitle();
            }
        }
        
        public java.lang.String[] getTitleLocalizationArgs() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationArgs()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationArgs();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationArgs()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationArgs();
            }
        }
        
        public java.lang.String getTitleLocalizationKey() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationKey()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getTitleLocalizationKey();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationKey()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getTitleLocalizationKey();
            }
        }
        
        public long[] getVibrateTimings() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVibrateConfig()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVibrateConfig();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVibrateTimings()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVibrateTimings();
            }
        }
        
        public java.lang.Integer getVisibility() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVisibility()");
                return ((com.huawei.hms.push.RemoteMessage.Notification) this.getHInstance()).getVisibility();
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVisibility()");
                return ((com.google.firebase.messaging.RemoteMessage.Notification) this.getGInstance()).getVisibility();
            }
        }
        
        public static org.xms.f.messaging.RemoteMessage.Notification dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.messaging.RemoteMessage.Notification) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.push.RemoteMessage.Notification;
            } else {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.messaging.RemoteMessage.Notification;
            }
        }
    }
}