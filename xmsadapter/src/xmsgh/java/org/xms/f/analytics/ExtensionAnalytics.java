package org.xms.f.analytics;




public final class ExtensionAnalytics extends org.xms.g.utils.XObject {
    
    
    
    public ExtensionAnalytics(org.xms.g.utils.XBox param0) {
        super(param0);
    }
    
    public org.xms.g.tasks.Task<java.lang.String> getAppInstanceId() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).getAAID()");
            com.huawei.hmf.tasks.Task hReturn = ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).getAAID();
            return ((hReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(null, hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).getAppInstanceId()");
            com.google.android.gms.tasks.Task gReturn = ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).getAppInstanceId();
            return ((gReturn) == null ? null : (new org.xms.g.tasks.Task.XImpl(new org.xms.g.utils.XBox(gReturn, null))));
        }
    }
    
    public static org.xms.f.analytics.ExtensionAnalytics getInstance(android.content.Context param0) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.HiAnalytics.getInstance(param0)");
            com.huawei.hms.analytics.HiAnalyticsInstance hReturn = com.huawei.hms.analytics.HiAnalytics.getInstance(param0);
            return ((hReturn) == null ? null : (new org.xms.f.analytics.ExtensionAnalytics(new org.xms.g.utils.XBox(null, hReturn))));
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.getInstance(param0)");
            com.google.firebase.analytics.FirebaseAnalytics gReturn = com.google.firebase.analytics.FirebaseAnalytics.getInstance(param0);
            return ((gReturn) == null ? null : (new org.xms.f.analytics.ExtensionAnalytics(new org.xms.g.utils.XBox(gReturn, null))));
        }
    }
    
    public final void logEvent(java.lang.String param0, android.os.Bundle param1) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).onEvent(param0, param1)");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).onEvent(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).logEvent(param0, param1)");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).logEvent(param0, param1);
        }
    }
    
    public final void resetAnalyticsData() {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).clearCachedData()");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).clearCachedData();
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).resetAnalyticsData()");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).resetAnalyticsData();
        }
    }
    
    public final void setAnalyticsCollectionEnabled(boolean param0) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setAnalyticsEnabled(param0)");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setAnalyticsEnabled(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setAnalyticsCollectionEnabled(param0)");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setAnalyticsCollectionEnabled(param0);
        }
    }
    
    public final void setCurrentScreen(android.app.Activity param0, java.lang.String param1, java.lang.String param2) {
        throw new java.lang.RuntimeException("Not Supported");
    }
    
    public final void setMinimumSessionDuration(long param0) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setMinActivitySessions(param0)");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setMinActivitySessions(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setMinimumSessionDuration(param0)");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setMinimumSessionDuration(param0);
        }
    }
    
    public final void setSessionTimeoutDuration(long param0) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setSessionDuration(param0)");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setSessionDuration(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setSessionTimeoutDuration(param0)");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setSessionTimeoutDuration(param0);
        }
    }
    
    public final void setUserId(java.lang.String param0) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserId(param0)");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserId(param0);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserId(param0)");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserId(param0);
        }
    }
    
    public final void setUserProperty(java.lang.String param0, java.lang.String param1) {
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserProfile(param0, param1)");
            ((com.huawei.hms.analytics.HiAnalyticsInstance) this.getHInstance()).setUserProfile(param0, param1);
        } else {
            org.xms.g.utils.XmsLog.d("XMSRouter", "((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserProperty(param0, param1)");
            ((com.google.firebase.analytics.FirebaseAnalytics) this.getGInstance()).setUserProperty(param0, param1);
        }
    }
    
    public static org.xms.f.analytics.ExtensionAnalytics dynamicCast(java.lang.Object param0) {
        return ((org.xms.f.analytics.ExtensionAnalytics) param0);
    }
    
    public static boolean isInstance(java.lang.Object param0) {
        if (!(param0 instanceof org.xms.g.utils.XGettable)) {
            return false;
        }
        if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
            return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.HiAnalyticsInstance;
        } else {
            return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics;
        }
    }
    
    public static class Event extends org.xms.g.utils.XObject {
        
        public Event(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected Event() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getADD_PAYMENT_INFO() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.CREATEPAYMENTINFO");
                return com.huawei.hms.analytics.type.HAEventType.CREATEPAYMENTINFO;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_PAYMENT_INFO");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_PAYMENT_INFO;
            }
        }
        
        public static java.lang.String getADD_TO_CART() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2CART");
                return com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2CART;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_CART");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_CART;
            }
        }
        
        public static java.lang.String getADD_TO_WISHLIST() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2WISHLIST");
                return com.huawei.hms.analytics.type.HAEventType.ADDPRODUCT2WISHLIST;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_WISHLIST");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.ADD_TO_WISHLIST;
            }
        }
        
        public static java.lang.String getAPP_OPEN() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTAPP");
                return com.huawei.hms.analytics.type.HAEventType.STARTAPP;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.APP_OPEN");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.APP_OPEN;
            }
        }
        
        public static java.lang.String getBEGIN_CHECKOUT() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTCHECKOUT");
                return com.huawei.hms.analytics.type.HAEventType.STARTCHECKOUT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.BEGIN_CHECKOUT");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.BEGIN_CHECKOUT;
            }
        }
        
        public static java.lang.String getCAMPAIGN_DETAILS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWCAMPAIGN");
                return com.huawei.hms.analytics.type.HAEventType.VIEWCAMPAIGN;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.CAMPAIGN_DETAILS");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.CAMPAIGN_DETAILS;
            }
        }
        
        public static java.lang.String getCHECKOUT_PROGRESS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWCHECKOUTSTEP");
                return com.huawei.hms.analytics.type.HAEventType.VIEWCHECKOUTSTEP;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.CHECKOUT_PROGRESS");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.CHECKOUT_PROGRESS;
            }
        }
        
        public static java.lang.String getEARN_VIRTUAL_CURRENCY() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.WINVIRTUALCOIN");
                return com.huawei.hms.analytics.type.HAEventType.WINVIRTUALCOIN;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY;
            }
        }
        
        public static java.lang.String getECOMMERCE_PURCHASE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.COMPLETEPURCHASE");
                return com.huawei.hms.analytics.type.HAEventType.COMPLETEPURCHASE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.ECOMMERCE_PURCHASE");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.ECOMMERCE_PURCHASE;
            }
        }
        
        public static java.lang.String getGENERATE_LEAD() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.OBTAINLEADS");
                return com.huawei.hms.analytics.type.HAEventType.OBTAINLEADS;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.GENERATE_LEAD");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.GENERATE_LEAD;
            }
        }
        
        public static java.lang.String getJOIN_GROUP() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.JOINUSERGROUP");
                return com.huawei.hms.analytics.type.HAEventType.JOINUSERGROUP;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.JOIN_GROUP");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.JOIN_GROUP;
            }
        }
        
        public static java.lang.String getLEVEL_END() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.COMPLETELEVEL");
                return com.huawei.hms.analytics.type.HAEventType.COMPLETELEVEL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_END");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_END;
            }
        }
        
        public static java.lang.String getLEVEL_START() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTLEVEL");
                return com.huawei.hms.analytics.type.HAEventType.STARTLEVEL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_START");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_START;
            }
        }
        
        public static java.lang.String getLEVEL_UP() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.UPGRADELEVEL");
                return com.huawei.hms.analytics.type.HAEventType.UPGRADELEVEL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_UP");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.LEVEL_UP;
            }
        }
        
        public static java.lang.String getLOGIN() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SIGNIN");
                return com.huawei.hms.analytics.type.HAEventType.SIGNIN;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.LOGIN");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.LOGIN;
            }
        }
        
        public static java.lang.String getPOST_SCORE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SUBMITSCORE");
                return com.huawei.hms.analytics.type.HAEventType.SUBMITSCORE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.POST_SCORE");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.POST_SCORE;
            }
        }
        
        public static java.lang.String getPRESENT_OFFER() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.CREATEORDER");
                return com.huawei.hms.analytics.type.HAEventType.CREATEORDER;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.PRESENT_OFFER");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.PRESENT_OFFER;
            }
        }
        
        public static java.lang.String getPURCHASE_REFUND() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.REFUNDORDER");
                return com.huawei.hms.analytics.type.HAEventType.REFUNDORDER;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.PURCHASE_REFUND");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.PURCHASE_REFUND;
            }
        }
        
        public static java.lang.String getREMOVE_FROM_CART() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.DELPRODUCTFROMCART");
                return com.huawei.hms.analytics.type.HAEventType.DELPRODUCTFROMCART;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.REMOVE_FROM_CART");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.REMOVE_FROM_CART;
            }
        }
        
        public static java.lang.String getSEARCH() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SEARCH");
                return com.huawei.hms.analytics.type.HAEventType.SEARCH;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SEARCH");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.SEARCH;
            }
        }
        
        public static java.lang.String getSELECT_CONTENT() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWCONTENT");
                return com.huawei.hms.analytics.type.HAEventType.VIEWCONTENT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SELECT_CONTENT");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.SELECT_CONTENT;
            }
        }
        
        public static java.lang.String getSET_CHECKOUT_OPTION() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.UPDATECHECKOUTOPTION");
                return com.huawei.hms.analytics.type.HAEventType.UPDATECHECKOUTOPTION;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SET_CHECKOUT_OPTION");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.SET_CHECKOUT_OPTION;
            }
        }
        
        public static java.lang.String getSHARE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.SHARECONTENT");
                return com.huawei.hms.analytics.type.HAEventType.SHARECONTENT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SHARE");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.SHARE;
            }
        }
        
        public static java.lang.String getSIGN_UP() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.REGISTERACCOUNT");
                return com.huawei.hms.analytics.type.HAEventType.REGISTERACCOUNT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SIGN_UP");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.SIGN_UP;
            }
        }
        
        public static java.lang.String getSPEND_VIRTUAL_CURRENCY() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.CONSUMEVIRTUALCOIN");
                return com.huawei.hms.analytics.type.HAEventType.CONSUMEVIRTUALCOIN;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY;
            }
        }
        
        public static java.lang.String getTUTORIAL_BEGIN() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.STARTTUTORIAL");
                return com.huawei.hms.analytics.type.HAEventType.STARTTUTORIAL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_BEGIN");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_BEGIN;
            }
        }
        
        public static java.lang.String getTUTORIAL_COMPLETE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.COMPLETETUTORIAL");
                return com.huawei.hms.analytics.type.HAEventType.COMPLETETUTORIAL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_COMPLETE");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.TUTORIAL_COMPLETE;
            }
        }
        
        public static java.lang.String getUNLOCK_ACHIEVEMENT() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.OBTAINACHIEVEMENT");
                return com.huawei.hms.analytics.type.HAEventType.OBTAINACHIEVEMENT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT;
            }
        }
        
        public static java.lang.String getVIEW_ITEM() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCT");
                return com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM;
            }
        }
        
        public static java.lang.String getVIEW_ITEM_LIST() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCTLIST");
                return com.huawei.hms.analytics.type.HAEventType.VIEWPRODUCTLIST;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM_LIST");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_ITEM_LIST;
            }
        }
        
        public static java.lang.String getVIEW_SEARCH_RESULTS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAEventType.VIEWSEARCHRESULT");
                return com.huawei.hms.analytics.type.HAEventType.VIEWSEARCHRESULT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS");
                return com.google.firebase.analytics.FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS;
            }
        }
        
        public static java.lang.String getADD_SHIPPING_INFO() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPURCHASE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getREFUND() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSELECT_ITEM() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSELECT_PROMOTION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getVIEW_CART() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getVIEW_PROMOTION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.Event dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.Event) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.type.HAEventType;
            } else {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics.Event;
            }
        }
    }
    
    public static class Param extends org.xms.g.utils.XObject {
        
        public Param(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected Param() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getACHIEVEMENT_ID() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ACHIEVEMENTID");
                return com.huawei.hms.analytics.type.HAParamType.ACHIEVEMENTID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ACHIEVEMENT_ID");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ACHIEVEMENT_ID;
            }
        }
        
        public static java.lang.String getACLID() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CLICKID");
                return com.huawei.hms.analytics.type.HAParamType.CLICKID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ACLID");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ACLID;
            }
        }
        
        public static java.lang.String getAFFILIATION() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.STORENAME");
                return com.huawei.hms.analytics.type.HAParamType.STORENAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.AFFILIATION");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.AFFILIATION;
            }
        }
        
        public static java.lang.String getCAMPAIGN() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PROMOTIONNAME");
                return com.huawei.hms.analytics.type.HAParamType.PROMOTIONNAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CAMPAIGN");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CAMPAIGN;
            }
        }
        
        public static java.lang.String getCHARACTER() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ROLENAME");
                return com.huawei.hms.analytics.type.HAParamType.ROLENAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CHARACTER");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CHARACTER;
            }
        }
        
        public static java.lang.String getCHECKOUT_OPTION() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.OPTION");
                return com.huawei.hms.analytics.type.HAParamType.OPTION;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_OPTION");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_OPTION;
            }
        }
        
        public static java.lang.String getCHECKOUT_STEP() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.STEP");
                return com.huawei.hms.analytics.type.HAParamType.STEP;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_STEP");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CHECKOUT_STEP;
            }
        }
        
        public static java.lang.String getCONTENT() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CONTENT");
                return com.huawei.hms.analytics.type.HAParamType.CONTENT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT;
            }
        }
        
        public static java.lang.String getCONTENT_TYPE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CONTENTTYPE");
                return com.huawei.hms.analytics.type.HAParamType.CONTENTTYPE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE;
            }
        }
        
        public static java.lang.String getCOUPON() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.VOUCHER");
                return com.huawei.hms.analytics.type.HAParamType.VOUCHER;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.COUPON");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.COUPON;
            }
        }
        
        public static java.lang.String getCP1() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.EXTENDPARAM");
                return com.huawei.hms.analytics.type.HAParamType.EXTENDPARAM;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CP1");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CP1;
            }
        }
        
        public static java.lang.String getCREATIVE_NAME() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.MATERIALNAME");
                return com.huawei.hms.analytics.type.HAParamType.MATERIALNAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_NAME");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_NAME;
            }
        }
        
        public static java.lang.String getCREATIVE_SLOT() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.MATERIALSLOT");
                return com.huawei.hms.analytics.type.HAParamType.MATERIALSLOT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_SLOT");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CREATIVE_SLOT;
            }
        }
        
        public static java.lang.String getCURRENCY() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CURRNAME");
                return com.huawei.hms.analytics.type.HAParamType.CURRNAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.CURRENCY");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.CURRENCY;
            }
        }
        
        public static java.lang.String getDESTINATION() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.DESTINATION");
                return com.huawei.hms.analytics.type.HAParamType.DESTINATION;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.DESTINATION");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.DESTINATION;
            }
        }
        
        public static java.lang.String getEND_DATE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ENDDATE");
                return com.huawei.hms.analytics.type.HAParamType.ENDDATE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.END_DATE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.END_DATE;
            }
        }
        
        public static java.lang.String getEXTEND_SESSION() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getFLIGHT_NUMBER() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.FLIGHTNO");
                return com.huawei.hms.analytics.type.HAParamType.FLIGHTNO;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.FLIGHT_NUMBER");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.FLIGHT_NUMBER;
            }
        }
        
        public static java.lang.String getGROUP_ID() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.USERGROUPID");
                return com.huawei.hms.analytics.type.HAParamType.USERGROUPID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.GROUP_ID");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.GROUP_ID;
            }
        }
        
        public static java.lang.String getINDEX() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.POSITIONID");
                return com.huawei.hms.analytics.type.HAParamType.POSITIONID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.INDEX");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.INDEX;
            }
        }
        
        public static java.lang.String getITEM_BRAND() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BRAND");
                return com.huawei.hms.analytics.type.HAParamType.BRAND;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_BRAND");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_BRAND;
            }
        }
        
        public static java.lang.String getITEM_CATEGORY() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CATEGORY");
                return com.huawei.hms.analytics.type.HAParamType.CATEGORY;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_CATEGORY");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_CATEGORY;
            }
        }
        
        public static java.lang.String getITEM_ID() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTID");
                return com.huawei.hms.analytics.type.HAParamType.PRODUCTID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID;
            }
        }
        
        public static java.lang.String getITEM_LIST() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTLIST");
                return com.huawei.hms.analytics.type.HAParamType.PRODUCTLIST;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LIST");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LIST;
            }
        }
        
        public static java.lang.String getITEM_LOCATION_ID() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PLACEID");
                return com.huawei.hms.analytics.type.HAParamType.PLACEID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LOCATION_ID");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_LOCATION_ID;
            }
        }
        
        public static java.lang.String getITEM_NAME() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTNAME");
                return com.huawei.hms.analytics.type.HAParamType.PRODUCTNAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_NAME");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_NAME;
            }
        }
        
        public static java.lang.String getITEM_VARIANT() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRODUCTFEATURE");
                return com.huawei.hms.analytics.type.HAParamType.PRODUCTFEATURE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_VARIANT");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_VARIANT;
            }
        }
        
        public static java.lang.String getLEVEL() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.LEVELID");
                return com.huawei.hms.analytics.type.HAParamType.LEVELID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL;
            }
        }
        
        public static java.lang.String getLEVEL_NAME() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.LEVELNAME");
                return com.huawei.hms.analytics.type.HAParamType.LEVELNAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL_NAME");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.LEVEL_NAME;
            }
        }
        
        public static java.lang.String getLOCATION() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PLACEID");
                return com.huawei.hms.analytics.type.HAParamType.PLACEID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.LOCATION");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.LOCATION;
            }
        }
        
        public static java.lang.String getMEDIUM() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.MEDIUM");
                return com.huawei.hms.analytics.type.HAParamType.MEDIUM;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.MEDIUM");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.MEDIUM;
            }
        }
        
        public static java.lang.String getMETHOD() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CHANNEL");
                return com.huawei.hms.analytics.type.HAParamType.CHANNEL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.METHOD");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.METHOD;
            }
        }
        
        public static java.lang.String getNUMBER_OF_NIGHTS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BOOKINGDAYS");
                return com.huawei.hms.analytics.type.HAParamType.BOOKINGDAYS;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_NIGHTS");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_NIGHTS;
            }
        }
        
        public static java.lang.String getNUMBER_OF_PASSENGERS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PASSENGERSNUMBER");
                return com.huawei.hms.analytics.type.HAParamType.PASSENGERSNUMBER;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_PASSENGERS");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_PASSENGERS;
            }
        }
        
        public static java.lang.String getNUMBER_OF_ROOMS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BOOKINGROOMS");
                return com.huawei.hms.analytics.type.HAParamType.BOOKINGROOMS;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_ROOMS");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.NUMBER_OF_ROOMS;
            }
        }
        
        public static java.lang.String getORIGIN() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.ORIGINATINGPLACE");
                return com.huawei.hms.analytics.type.HAParamType.ORIGINATINGPLACE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.ORIGIN");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.ORIGIN;
            }
        }
        
        public static java.lang.String getPRICE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.PRICE");
                return com.huawei.hms.analytics.type.HAParamType.PRICE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE;
            }
        }
        
        public static java.lang.String getQUANTITY() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.QUANTITY");
                return com.huawei.hms.analytics.type.HAParamType.QUANTITY;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.QUANTITY");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.QUANTITY;
            }
        }
        
        public static java.lang.String getSCORE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.SCORE");
                return com.huawei.hms.analytics.type.HAParamType.SCORE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SCORE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.SCORE;
            }
        }
        
        public static java.lang.String getSEARCH_TERM() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.KEYWORDS");
                return com.huawei.hms.analytics.type.HAParamType.KEYWORDS;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SEARCH_TERM");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.SEARCH_TERM;
            }
        }
        
        public static java.lang.String getSHIPPING() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.SHIPPING");
                return com.huawei.hms.analytics.type.HAParamType.SHIPPING;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SHIPPING");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.SHIPPING;
            }
        }
        
        public static java.lang.String getSIGN_UP_METHOD() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CHANNEL");
                return com.huawei.hms.analytics.type.HAParamType.CHANNEL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SIGN_UP_METHOD");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.SIGN_UP_METHOD;
            }
        }
        
        public static java.lang.String getSOURCE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.SOURCE");
                return com.huawei.hms.analytics.type.HAParamType.SOURCE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SOURCE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.SOURCE;
            }
        }
        
        public static java.lang.String getSTART_DATE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.BEGINDATE");
                return com.huawei.hms.analytics.type.HAParamType.BEGINDATE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.START_DATE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.START_DATE;
            }
        }
        
        public static java.lang.String getSUCCESS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.RESULT");
                return com.huawei.hms.analytics.type.HAParamType.RESULT;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.SUCCESS");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.SUCCESS;
            }
        }
        
        public static java.lang.String getTAX() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.TAXFEE");
                return com.huawei.hms.analytics.type.HAParamType.TAXFEE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TAX");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.TAX;
            }
        }
        
        public static java.lang.String getTERM() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.KEYWORDS");
                return com.huawei.hms.analytics.type.HAParamType.KEYWORDS;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TERM");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.TERM;
            }
        }
        
        public static java.lang.String getTRANSACTION_ID() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.TRANSACTIONID");
                return com.huawei.hms.analytics.type.HAParamType.TRANSACTIONID;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TRANSACTION_ID");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.TRANSACTION_ID;
            }
        }
        
        public static java.lang.String getTRAVEL_CLASS() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CLASS");
                return com.huawei.hms.analytics.type.HAParamType.CLASS;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.TRAVEL_CLASS");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.TRAVEL_CLASS;
            }
        }
        
        public static java.lang.String getVALUE() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.REVENUE");
                return com.huawei.hms.analytics.type.HAParamType.REVENUE;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.VALUE");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.VALUE;
            }
        }
        
        public static java.lang.String getVIRTUAL_CURRENCY_NAME() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.VIRTUALCURRNAME");
                return com.huawei.hms.analytics.type.HAParamType.VIRTUALCURRNAME;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME");
                return com.google.firebase.analytics.FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME;
            }
        }
        
        public static java.lang.String getDISCOUNT() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY2() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY3() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY4() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_CATEGORY5() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_LIST_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEM_LIST_NAME() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getITEMS() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getLOCATION_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPAYMENT_TYPE() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPROMOTION_ID() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getPROMOTION_NAME() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSHIPPING_TIER() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.Param dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.Param) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.type.HAParamType;
            } else {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics.Param;
            }
        }
    }
    
    public static class UserProperty extends org.xms.g.utils.XObject {
        
        public UserProperty(org.xms.g.utils.XBox param0) {
            super(param0);
        }
        
        protected UserProperty() {
            super(((org.xms.g.utils.XBox) null));
        }
        
        public static java.lang.String getALLOW_AD_PERSONALIZATION_SIGNALS() {
            throw new java.lang.RuntimeException("Not Supported");
        }
        
        public static java.lang.String getSIGN_UP_METHOD() {
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.huawei.hms.analytics.type.HAParamType.CHANNEL");
                return com.huawei.hms.analytics.type.HAParamType.CHANNEL;
            } else {
                org.xms.g.utils.XmsLog.d("XMSRouter", "com.google.firebase.analytics.FirebaseAnalytics.UserProperty.SIGN_UP_METHOD");
                return com.google.firebase.analytics.FirebaseAnalytics.UserProperty.SIGN_UP_METHOD;
            }
        }
        
        public static org.xms.f.analytics.ExtensionAnalytics.UserProperty dynamicCast(java.lang.Object param0) {
            return ((org.xms.f.analytics.ExtensionAnalytics.UserProperty) param0);
        }
        
        public static boolean isInstance(java.lang.Object param0) {
            if (!(param0 instanceof org.xms.g.utils.XGettable)) {
                return false;
            }
            if (org.xms.g.utils.GlobalEnvSetting.isHms()) {
                return ((org.xms.g.utils.XGettable) param0).getHInstance() instanceof com.huawei.hms.analytics.type.HAParamType;
            } else {
                return ((org.xms.g.utils.XGettable) param0).getGInstance() instanceof com.google.firebase.analytics.FirebaseAnalytics.UserProperty;
            }
        }
    }
}